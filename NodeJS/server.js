const WebSocket = require('ws');
const http = require('http');
const fs = require('fs');
const request = require('request');
const LocalStorage = require('node-localstorage').LocalStorage;
const localStorage = new LocalStorage('./localStorage');

const hostname = '192.168.0.158';
const port = 3000;

const index = fs.readFileSync('index.html', 'utf8');

const server = http.createServer((req, res) => {
	res.writeHead(200);
	res.end(index);
});

server.listen(port, hostname, () => {
	console.log(`Listen port:${port}`);
  	console.log(`Server running at http://${hostname}:${port}/`);
});

const ws = new WebSocket.Server({ server });

ws.on('connection', (connection, req) => {
	const ip = req.socket.remoteAddress;
	for (const client of ws.clients) {
		client.send(JSON.stringify({userstatusonline:1}), { binary: false });
    }

	console.log(`Connected ${ip}`);

  	connection.on('message', (message) => {
    	const dataparse = JSON.parse(message);
    	if(dataparse.message == 'connection'){
    		localStorage.setItem('user', dataparse.user);
    	}else{
	    	request.post(dataparse.url,
	    		{
	    			json:{
	    				dataparse
		    		}
		    	},
			    function (error, response, body) {
			        if (!error && response.statusCode == 200) {
			            for (const client of ws.clients) {
							client.send(JSON.stringify(body), { binary: false });
					    }
			        }
			    }
			);
	    }
	});

	connection.on('close', () => {
		for (const client of ws.clients) {
			client.send(JSON.stringify({userstatusonline:0}), { binary: false });
	    }

		console.log(localStorage.getItem('user'));
		localStorage.removeItem('user');
	});
});
