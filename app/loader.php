<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

require_once dirname( __FILE__ ) . '/config/config.php';
require_once dirname( __FILE__ ) . '/vendor/autoload.php';
require_once dirname( __FILE__ ) . '/src/env.php';

use Symfony\Component\Dotenv\Dotenv;
use app\Controllers\ErrorController;
use app\Core\Router;
use app\Models\ModulesModels;
use app\Models\TreePageModels;

class Loader
{

	public function __construct()
	{
		date_default_timezone_set('Europe/Moscow');
		/**
		 *Прверяем версию php
		 */
		if(explode('.', PHP_VERSION)['0'] < 8 ){
		    echo 'Ваша версия PHP ниже 8!';
		    exit;
		}

		/**
		 * Подключаем файл конфигурации
		 */
		if (file_exists(dirname( __FILE__ ) . '/config/.env')) {
		    (new Dotenv())->usePutenv()->load(dirname( __FILE__ ) . '/config/.env');
		}

		/**
		 * Подключаем роутинг
		 */
		$router = new Router();

		/**
		 * Подключаем контроллер главной страницы
		*/
		$router->addRoute('^/$',function($url) {
			$className = [
                'modules' => ['\app\Controllers\MainController', 'actionIndex'],
            ];
            $return = new $className["modules"][0];
            $indexfile = $className["modules"][1];
            $return->$indexfile();
		});

		/**
		 * Подключаем страницы модулей.
		 */
		$router->addRoute('^/([0-9a-zA-Z\-]+)',function($url,$slug) {
			if($slug === 'robots'){
				$className = [
	                'modules' => ['\app\Controllers\RobotsController', 'actionIndex'],
	            ];
	            $return = new $className["modules"][0];
	            $indexfile = $className["modules"][1];
	            $return->$indexfile();
			}
			if($slug === 'sitemap'){
				$className = [
	                'modules' => ['\app\Controllers\SitemapController', 'actionIndex'],
	            ];
	            $return = new $className["modules"][0];
	            $indexfile = $className["modules"][1];
	            $return->$indexfile();
			}
			/**
			 * Подключаем доступ к методам класса MainController
			 */
			if($slug === 'index'){
				if(isset(\DolinaIS::getName()->Request[1])){
					$className = [
		                'modules' => ['\app\Controllers\MainController', 'action'.ucfirst(\DolinaIS::getName()->Request[1])],
		            ];
		            if(class_exists($className["modules"][0])){
	                    $return = new $className["modules"][0];
	                    if(method_exists($return, $className["modules"][1])){
	                        $indexfile = $className["modules"][1];
	                        return $return->$indexfile();
	                    }else{
	                        return new ErrorController('Метод: <code>'.\DolinaIS::getName()->Request[1].'</code> не существует');
	                    }
	                }else{
	                    return new ErrorController();
	                }
				}else{
					return new ErrorController();
				}
			}

			/**
			 * Обращаемся к базе данных, смотрим существующие страницы (модули)
			 */
            $module = TreePageModels::get()->findOne(
            	array(
            		'page_alias' => $slug,
            		'page_status' => 1
            	)
            );

			if(!$module){
				return new ErrorController();
			}

			foreach($module as $module_id){
				if(isset($module_id["module_id"])){
					$className = [
					    'modules' => ['\app\Modules\\'.$module_id["module_id"].'\\'.ucfirst($module_id["module_id"]), 'actionIndex'],
					];
					return new $className["modules"][0];
				}else{
					return new ErrorController();
				}
			}
		});

		$router->run();
	}
}

new Loader();