-- --------------------------------------------------------
-- Хост:                         192.168.0.158
-- Версия сервера:               10.11.3-MariaDB-1 - Debian 12
-- Операционная система:         debian-linux-gnu
-- HeidiSQL Версия:              12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дамп структуры базы данных dolinaiscms
CREATE DATABASE IF NOT EXISTS `dolinaiscms` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `dolinaiscms`;

-- Дамп структуры для таблица dolinaiscms.account
CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `phone` bigint(12) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `patronymic` varchar(100) DEFAULT NULL,
  `birthday` int(11) DEFAULT 0,
  `role` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `password_hash` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT 0,
  `created_at` int(11) DEFAULT 0,
  `updated_at` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Дамп данных таблицы dolinaiscms.account: ~3 rows (приблизительно)
INSERT INTO `account` (`id`, `username`, `phone`, `email`, `first_name`, `last_name`, `patronymic`, `birthday`, `role`, `status`, `password_hash`, `auth_key`, `password_reset_token`, `code`, `created_at`, `updated_at`) VALUES
	(1, '79200880000', 79200880000, '0', NULL, NULL, NULL, 0, NULL, 0, '$2y$10$lBqT0jwBvO7gsIRRm1X67e9pgmspLNND77OBFUe3Sqz0Z2VDU28cO', '405d27af11a43cb537560ed039877bf3', NULL, 0, 1695388947, 1695388947),
	(2, '79258400000', 79258400000, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$fL33Kqf2CMtJ4JtPNRXcwOf9YSRcON6350sIkQVUq0T8mfdNjpug.', 'c3f00375d7652df2eb1b190b27f2038a', NULL, 0, 1695388984, 1695388984),
	(3, '79155000000', 79155000000, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$aWsiH5hr22sNo.j1X0ruCOf4lx5Qc2xJkGK2NIsOAjmiPD.eHAKC.', '9a4dcef22cab707d3db2715846c59227', NULL, 0, 1695389079, 1695389079);

-- Дамп структуры для таблица dolinaiscms.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Дамп данных таблицы dolinaiscms.categories: ~6 rows (приблизительно)
INSERT INTO `categories` (`id`, `name`, `description`, `created`, `modified`) VALUES
	(1, 'Fashion', 'Category for anything related to fashion.', '2014-06-01 00:35:07', '2014-05-30 14:34:33'),
	(2, 'Electronics', 'Gadgets, drones and more.', '2014-06-01 00:35:07', '2014-05-30 14:34:33'),
	(3, 'Motors', 'Motor sports and more', '2014-06-01 00:35:07', '2014-05-30 14:34:54'),
	(5, 'Movies', 'Movie products.', '2019-05-20 10:22:05', '2019-08-20 07:30:15'),
	(6, 'Books', 'Kindle books, audio books and more.', '2018-03-14 08:05:25', '2019-05-20 08:29:11'),
	(13, 'Sports', 'Drop into new winter gear.', '2016-01-09 02:24:24', '2016-01-08 22:24:24');

-- Дамп структуры для таблица dolinaiscms.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `menu_sorting` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinaiscms.menu: ~5 rows (приблизительно)
INSERT INTO `menu` (`id`, `title`, `page_id`, `slug`, `menu_sorting`, `created_by`) VALUES
	(1, 'О нас', 1, 'articles/page/about', NULL, NULL),
	(2, 'Статьи', NULL, 'articles', NULL, NULL),
	(3, 'Скачать', 4, 'articles/page/download', NULL, NULL),
	(4, 'Контакты', NULL, 'contacts', NULL, NULL),
	(5, 'API', NULL, 'api', NULL, NULL);

-- Дамп структуры для таблица dolinaiscms.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `module_size` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinaiscms.modules: ~4 rows (приблизительно)
INSERT INTO `modules` (`id`, `module_id`, `type`, `description`, `slug`, `status`, `title`, `version`, `module_size`, `created_by`, `updated_by`) VALUES
	(1, 'api', 'Module', 'Api сайта', 'api', 1, 'Api', '0.0.1', 100, NULL, NULL),
	(2, 'page', 'Module', 'Модуль страницы', 'articles', 1, 'Страницы', '0.0.1', 100, NULL, NULL),
	(3, 'admin', 'Module', 'Admin modules', 'admin', 1, 'Панель администратора', '0.0.1', 100, NULL, NULL),
	(4, 'account', 'Module', 'User modules', 'account', 1, 'ЛК', '0.0.1', 100, NULL, NULL);

-- Дамп структуры для таблица dolinaiscms.page
CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinaiscms.page: ~4 rows (приблизительно)
INSERT INTO `page` (`id`, `title`, `description`, `slug`) VALUES
	(1, 'О нас', '<p>Дизайн и разработка веб-сайта для производственной компании может быть довольно сложной задачей. Однако, когда к нам обратилась компания по производству пружин, мы были рады взяться за этот проект. Наш клиент хотел веб-сайт, который продемонстрировал бы их продукты и услуги и помог бы им привлечь больше клиентов в Интернете. Вот как мы разработали сайт для компании по производству пружин.</p>\n<p>Понимание потребностей клиента</p>\n<p>Первым шагом в этом процессе было понимание потребностей клиента. У нас была серия встреч с клиентом, чтобы обсудить его бизнес-цели, целевую аудиторию и то, чего они хотели достичь с помощью своего веб-сайта. Мы также спросили их об их конкурентах и ​​о том, что им нравится и не нравится в веб-сайтах их конкурентов.</p>\n<p>Создание концепции дизайна</p>\n<p>После того, как у нас было четкое понимание потребностей клиента, мы создали концепцию дизайна сайта. Мы хотели убедиться, что веб-сайт не только выглядит профессионально, но и удобен в навигации. Мы также хотели убедиться, что веб-сайт отражает ценности компании и ее фирменный стиль.</p>\n<p>Разработка веб-сайта</p>\n<p>После того, как концепция дизайна была одобрена клиентом, мы приступили к разработке сайта. Мы использовали WordPress в качестве системы управления контентом и разработали собственную тему для веб-сайта. Мы также позаботились о том, чтобы веб-сайт был оптимизирован для поисковых систем и был адаптивным, что означает, что он будет отлично выглядеть на любом устройстве.</p>\n<p>Создание контента</p>\n<p>Мы работали с клиентом над созданием контента для сайта. Мы позаботились о том, чтобы контент был информативным, привлекательным и легко читаемым. Мы также позаботились о том, чтобы контент был оптимизирован для поисковых систем и содержал правильные ключевые слова.</p>\n<p>Тестирование и запуск</p>\n<p>Перед запуском сайта мы протестировали его на разных устройствах и в разных браузерах, чтобы убедиться, что он работает корректно. Мы также позаботились о безопасности сайта и установке всех необходимых плагинов и обновлений. Когда сайт нас устроил, мы его запустили.</p>\n<p>Результат</p>\n<p>Сайт, который мы разработали для компании по производству пружин, удался. Веб-сайт помог компании привлечь больше клиентов в Интернете и продемонстрировать свои продукты и услуги. На сайте было легко ориентироваться, а контент был информативным и привлекательным. Веб-сайт также отражал ценности компании и ее фирменный стиль. Заказчик остался доволен результатом и получил положительные отзывы от своих клиентов.</p>', 'about'),
	(2, 'Lorem Ipsum', 'В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.', 'lorem'),
	(3, 'Хорошая новость!', '<p>Dolina IS &mdash; ведущий поставщик ИТ-услуг, который предлагает услуги веб-разработки для предприятий любого размера. С ростом присутствия в Интернете наличие хорошо разработанного веб-сайта стало необходимостью для организаций, которые хотят добиться успеха в цифровом пространстве. В этой статье мы рассмотрим основные этапы разработки веб-сайта для Dolina IS.</p>\n<p>1. Определите цель вашего сайта</p>\n<p>Первым шагом в разработке веб-сайта является четкое определение цели вашего веб-сайта. Это включает в себя определение вашей целевой аудитории, целей, которых вы хотите достичь с помощью своего веб-сайта, и типов контента, который вы хотите включить. Если вы хотите создать веб-сайт электронной коммерции, блог или сайт-портфолио, определение цели вашего веб-сайта имеет решающее значение для создания основы для успешного веб-сайта.</p>\n<p>2. Выберите правильную платформу</p>\n<p>После определения цели вашего веб-сайта следующим шагом будет выбор правильной платформы для создания вашего веб-сайта. Dolina IS предлагает различные платформы, включая WordPress, Drupal и Joomla. Каждая платформа имеет свои сильные и слабые стороны, поэтому важно выбрать ту, которая лучше всего соответствует вашим потребностям.</p>\n<p>3. Создайте свой веб-сайт</p>\n<p>Дизайн вашего веб-сайта имеет решающее значение для создания сильного присутствия в Интернете. Хорошо продуманный сайт не только привлекает посетителей, но и удерживает их внимание. Dolina IS предлагает команду опытных дизайнеров, которые могут создать индивидуальный дизайн, соответствующий эстетике вашего бренда.</p>\n<p>4. Разработайте свой сайт</p>\n<p>После того, как дизайн будет завершен, следующим шагом будет разработка вашего веб-сайта. Это включает в себя написание кода, интеграцию плагинов и добавление контента. В Dolina IS работает опытная команда разработчиков, специализирующихся на различных языках веб-разработки, включая HTML, CSS и JavaScript.</p>\n<p>5. Оптимизируйте свой сайт для SEO</p>\n<p>Поисковая оптимизация (SEO) имеет решающее значение для привлечения трафика на ваш сайт. Dolina IS предлагает услуги SEO, которые могут повысить видимость вашего сайта в поисковых системах. Наша команда может помочь оптимизировать структуру, содержание и ключевые слова вашего веб-сайта, чтобы обеспечить его более высокий рейтинг на страницах результатов поисковых систем.</p>\n<p>6. Протестируйте и запустите свой сайт</p>\n<p>Перед запуском вашего веб-сайта важно тщательно протестировать его, чтобы убедиться, что он работает правильно. Долина ИС предлагает услуги по тестированию веб-сайтов, чтобы убедиться, что ваш сайт не содержит ошибок и ошибок. После завершения этапа тестирования наша команда запустит ваш веб-сайт, обеспечив его доступность для вашей целевой аудитории.</p>\n<p>В заключение, разработка веб-сайта &mdash; это сложный процесс, требующий опыта и знаний. Dolina IS предлагает полный набор услуг веб-разработки, включая проектирование, разработку, оптимизацию и запуск веб-сайтов. С нашей командой опытных профессионалов вы можете быть уверены, что ваш сайт будет выделяться в переполненном онлайн-пространстве.</p>', 'goodnews'),
	(4, 'Скачать', 'Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'download');

-- Дамп структуры для таблица dolinaiscms.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `settings_name` varchar(255) NOT NULL,
  `settings_text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinaiscms.settings: ~0 rows (приблизительно)
INSERT INTO `settings` (`id`, `settings_name`, `settings_text`) VALUES
	(1, 'title', 'CMS');

-- Дамп структуры для таблица dolinaiscms.tree_page
CREATE TABLE IF NOT EXISTS `tree_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tree_id` int(11) DEFAULT NULL,
  `tree_id_category` varchar(255) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `page_level` int(11) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_text` text DEFAULT NULL,
  `page_type` varchar(255) DEFAULT NULL,
  `page_file` varchar(255) DEFAULT NULL,
  `page_attribute` varchar(255) DEFAULT NULL,
  `page_view_file` varchar(255) DEFAULT NULL,
  `page_position` varchar(255) DEFAULT NULL,
  `page_widgets` varchar(255) DEFAULT NULL,
  `page_status` int(11) DEFAULT NULL,
  `page_meta_title` varchar(255) DEFAULT NULL,
  `page_meta_description` varchar(255) DEFAULT NULL,
  `page_meta_keywords` varchar(255) DEFAULT NULL,
  `page_alias` varchar(255) DEFAULT NULL,
  `page_sorting` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `page_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinaiscms.tree_page: ~0 rows (приблизительно)

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
