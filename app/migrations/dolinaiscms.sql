-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 06 2023 г., 19:49
-- Версия сервера: 8.0.30
-- Версия PHP: 8.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dolinaiscms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_general_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created`, `modified`) VALUES
(1, 'Fashion', 'Category for anything related to fashion.', '2014-06-01 00:35:07', '2014-05-30 14:34:33'),
(2, 'Electronics', 'Gadgets, drones and more.', '2014-06-01 00:35:07', '2014-05-30 14:34:33'),
(3, 'Motors', 'Motor sports and more', '2014-06-01 00:35:07', '2014-05-30 14:34:54'),
(5, 'Movies', 'Movie products.', '2019-05-20 10:22:05', '2019-08-20 07:30:15'),
(6, 'Books', 'Kindle books, audio books and more.', '2018-03-14 08:05:25', '2019-05-20 08:29:11'),
(13, 'Sports', 'Drop into new winter gear.', '2016-01-09 02:24:24', '2016-01-08 22:24:24');

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_id` int DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `title`, `page_id`, `slug`, `created_by`) VALUES
(1, 'О нас', 1, 'articles/page/about', NULL),
(2, 'Статьи', NULL, 'articles', NULL),
(3, 'Реклама', NULL, 'articles/lorem', NULL),
(4, 'Контакты', NULL, 'contacts', NULL),
(5, 'API', NULL, 'api', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `modules`
--

CREATE TABLE `modules` (
  `id` int NOT NULL,
  `module_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` int DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `version` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `module_size` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `modules`
--

INSERT INTO `modules` (`id`, `module_id`, `type`, `description`, `slug`, `status`, `title`, `version`, `module_size`, `created_by`, `updated_by`) VALUES
(1, 'api', 'Module', 'Api сайта', 'api', 1, 'Api', '0.0.1', 100, NULL, NULL),
(2, 'page', 'Module', 'Модуль страницы', 'articles', 1, 'Страницы', '0.0.1', 100, NULL, NULL),
(3, 'admin', 'Module', 'Admin modules', 'admin', 1, 'Панель администратора', '0.0.1', 100, NULL, NULL),
(4, 'account', 'Module', 'User modules', 'account', 1, 'ЛК', '0.0.1', 100, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

CREATE TABLE `page` (
  `id` int NOT NULL,
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `slug` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`id`, `title`, `description`, `slug`) VALUES
(1, 'О нас', '<p>Дизайн и разработка веб-сайта для производственной компании может быть довольно сложной задачей. Однако, когда к нам обратилась компания по производству пружин, мы были рады взяться за этот проект. Наш клиент хотел веб-сайт, который продемонстрировал бы их продукты и услуги и помог бы им привлечь больше клиентов в Интернете. Вот как мы разработали сайт для компании по производству пружин.</p>\n<p>Понимание потребностей клиента</p>\n<p>Первым шагом в этом процессе было понимание потребностей клиента. У нас была серия встреч с клиентом, чтобы обсудить его бизнес-цели, целевую аудиторию и то, чего они хотели достичь с помощью своего веб-сайта. Мы также спросили их об их конкурентах и ​​о том, что им нравится и не нравится в веб-сайтах их конкурентов.</p>\n<p>Создание концепции дизайна</p>\n<p>После того, как у нас было четкое понимание потребностей клиента, мы создали концепцию дизайна сайта. Мы хотели убедиться, что веб-сайт не только выглядит профессионально, но и удобен в навигации. Мы также хотели убедиться, что веб-сайт отражает ценности компании и ее фирменный стиль.</p>\n<p>Разработка веб-сайта</p>\n<p>После того, как концепция дизайна была одобрена клиентом, мы приступили к разработке сайта. Мы использовали WordPress в качестве системы управления контентом и разработали собственную тему для веб-сайта. Мы также позаботились о том, чтобы веб-сайт был оптимизирован для поисковых систем и был адаптивным, что означает, что он будет отлично выглядеть на любом устройстве.</p>\n<p>Создание контента</p>\n<p>Мы работали с клиентом над созданием контента для сайта. Мы позаботились о том, чтобы контент был информативным, привлекательным и легко читаемым. Мы также позаботились о том, чтобы контент был оптимизирован для поисковых систем и содержал правильные ключевые слова.</p>\n<p>Тестирование и запуск</p>\n<p>Перед запуском сайта мы протестировали его на разных устройствах и в разных браузерах, чтобы убедиться, что он работает корректно. Мы также позаботились о безопасности сайта и установке всех необходимых плагинов и обновлений. Когда сайт нас устроил, мы его запустили.</p>\n<p>Результат</p>\n<p>Сайт, который мы разработали для компании по производству пружин, удался. Веб-сайт помог компании привлечь больше клиентов в Интернете и продемонстрировать свои продукты и услуги. На сайте было легко ориентироваться, а контент был информативным и привлекательным. Веб-сайт также отражал ценности компании и ее фирменный стиль. Заказчик остался доволен результатом и получил положительные отзывы от своих клиентов.</p>', 'about'),
(2, 'Lorem Ipsum', 'В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.', 'lorem'),
(3, 'Хорошая новость!', '<p>Dolina IS &mdash; ведущий поставщик ИТ-услуг, который предлагает услуги веб-разработки для предприятий любого размера. С ростом присутствия в Интернете наличие хорошо разработанного веб-сайта стало необходимостью для организаций, которые хотят добиться успеха в цифровом пространстве. В этой статье мы рассмотрим основные этапы разработки веб-сайта для Dolina IS.</p>\n<p>1. Определите цель вашего сайта</p>\n<p>Первым шагом в разработке веб-сайта является четкое определение цели вашего веб-сайта. Это включает в себя определение вашей целевой аудитории, целей, которых вы хотите достичь с помощью своего веб-сайта, и типов контента, который вы хотите включить. Если вы хотите создать веб-сайт электронной коммерции, блог или сайт-портфолио, определение цели вашего веб-сайта имеет решающее значение для создания основы для успешного веб-сайта.</p>\n<p>2. Выберите правильную платформу</p>\n<p>После определения цели вашего веб-сайта следующим шагом будет выбор правильной платформы для создания вашего веб-сайта. Dolina IS предлагает различные платформы, включая WordPress, Drupal и Joomla. Каждая платформа имеет свои сильные и слабые стороны, поэтому важно выбрать ту, которая лучше всего соответствует вашим потребностям.</p>\n<p>3. Создайте свой веб-сайт</p>\n<p>Дизайн вашего веб-сайта имеет решающее значение для создания сильного присутствия в Интернете. Хорошо продуманный сайт не только привлекает посетителей, но и удерживает их внимание. Dolina IS предлагает команду опытных дизайнеров, которые могут создать индивидуальный дизайн, соответствующий эстетике вашего бренда.</p>\n<p>4. Разработайте свой сайт</p>\n<p>После того, как дизайн будет завершен, следующим шагом будет разработка вашего веб-сайта. Это включает в себя написание кода, интеграцию плагинов и добавление контента. В Dolina IS работает опытная команда разработчиков, специализирующихся на различных языках веб-разработки, включая HTML, CSS и JavaScript.</p>\n<p>5. Оптимизируйте свой сайт для SEO</p>\n<p>Поисковая оптимизация (SEO) имеет решающее значение для привлечения трафика на ваш сайт. Dolina IS предлагает услуги SEO, которые могут повысить видимость вашего сайта в поисковых системах. Наша команда может помочь оптимизировать структуру, содержание и ключевые слова вашего веб-сайта, чтобы обеспечить его более высокий рейтинг на страницах результатов поисковых систем.</p>\n<p>6. Протестируйте и запустите свой сайт</p>\n<p>Перед запуском вашего веб-сайта важно тщательно протестировать его, чтобы убедиться, что он работает правильно. Долина ИС предлагает услуги по тестированию веб-сайтов, чтобы убедиться, что ваш сайт не содержит ошибок и ошибок. После завершения этапа тестирования наша команда запустит ваш веб-сайт, обеспечив его доступность для вашей целевой аудитории.</p>\n<p>В заключение, разработка веб-сайта &mdash; это сложный процесс, требующий опыта и знаний. Dolina IS предлагает полный набор услуг веб-разработки, включая проектирование, разработку, оптимизацию и запуск веб-сайтов. С нашей командой опытных профессионалов вы можете быть уверены, что ваш сайт будет выделяться в переполненном онлайн-пространстве.</p>', 'goodnews'),
(4, 'Скачать', 'Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'download');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_general_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_general_ci NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `category_id` int NOT NULL,
  `created` int DEFAULT NULL,
  `modified` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `category_id`, `created`, `modified`) VALUES
(2, 'test2', 'description test', '123', 1, 1693346653, NULL),
(3, 'test3', 'description test', '123', 2, 1693346516, NULL),
(4, 'test', 'description test', '123', 1, 1693346539, NULL),
(5, 'test', 'description test', '12', 1, 1693346612, NULL),
(6, 'test1', '1693767529', '13', 1, 1693767529, NULL),
(7, 'test', 'description test', '13', 1, 1693346633, NULL),
(8, 'test', 'description test', '1234', 1, 1693346650, NULL),
(9, 'test', 'description test', '1235', 1, 1693346653, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `settings_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `settings_text` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `settings_name`, `settings_text`) VALUES
(1, 'title', 'CMS');

-- --------------------------------------------------------

--
-- Структура таблицы `tree_page`
--

CREATE TABLE `tree_page` (
  `id` bigint NOT NULL,
  `tree_id` int DEFAULT NULL,
  `tree_id_category` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_id` int DEFAULT NULL,
  `page_level` int DEFAULT NULL,
  `page_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `page_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_attribute` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_view_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_widgets` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_status` int DEFAULT NULL,
  `page_meta_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_meta_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_meta_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `page_sorting` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `page_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tree_page`
--
ALTER TABLE `tree_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `page`
--
ALTER TABLE `page`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `tree_page`
--
ALTER TABLE `tree_page`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
