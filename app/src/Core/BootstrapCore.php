<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Core;

class BootstrapCore
{

    // public $position;

    function __construct($position)
    {
        $this->position($position);
    }

    private function position($position='header')
    {
        $file = array(
            'header' => array(
                // '<link rel="stylesheet" href="/resources/style/npm_bootstrap@5.3.0_dist_css_bootstrap.min.css">',
                '<link rel="stylesheet" href="/resources/style/mdb.min.css">',
                '<link rel="stylesheet" href="/resources/style/Font-Awesome-Free-6.4.0.css">',
                '<link rel="stylesheet" href="/resources/style/popap_style.css">',
                '<link rel="stylesheet" href="/resources/style/5.2.5_css_fileinput.min.css">',
                '<script type="text/javascript" src="/resources/js/jquery-3.6.0.min.js"></script>',
            ),
            'footer' => array(
                '<script type="text/javascript" src="/resources/js/mdb.min.js"></script>',
                '<script type="text/javascript" src="/resources/js/script.js"></script>',
                '<script type="text/javascript" src="/resources/js/bootstrap.bundle.min.js"></script>',
                '<script type="text/javascript" src="/resources/js/5.2.5_js_plugins_sortable.min.js"></script>',
                '<script type="text/javascript" src="/resources/js/5.2.5_js_fileinput.min.js"></script>',
            ),
        );
        foreach($file[$position] as $position)
        echo $position."\n";
    }
}