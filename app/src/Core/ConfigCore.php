<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Core;

use app\Modules\Account\Models\AccountModels;

class ConfigCore
{

    public $isGuest;
    public $isUserID;
    public $setAliasTest;
    public $widget;
    public $Request;
    public $db;

    public function __construct()
    {
        $this->isGuest = $this->isGuest();
        $this->isUserID = $this->isUserID();
        $this->setAliasTest = $this->setAliasTest();
        $this->widget = $this->setWidget();
        $this->Request = $this->setRequest();
        $this->db = 111;
    }

    static function getName()
    {
        return new ConfigCore();
    }

    public function isGuest()
    {
        if (isset($_COOKIE['PHPSESSID'])) {
            $user = AccountModels::get()
            ->findOne(
                array(
                    'auth_key' => $_COOKIE['PHPSESSID']
                )
            );
            if(!isset($user[0])){
                return null;
            }else{
                return true;
            }
        }
        return null;
    }

    public function isUserID()
    {
        if (isset($_COOKIE['PHPSESSID'])) {
            $user = AccountModels::get()
            ->findOne(
                array(
                    'auth_key' => $_COOKIE['PHPSESSID']
                )
            );
            if(!isset($user[0])){
                return null;
            }else{
                return $user[0]['id'];
            }
        }
        return null;
    }

    public static function setAlias($setAlias)
    {
        switch ($setAlias) {
            case '@app':
                return dirname($_SERVER['DOCUMENT_ROOT']) . '/app/src';
                break;
            case '@config':
                return dirname($_SERVER['DOCUMENT_ROOT']) . '/app/config';
                break;
            case '@widget':
                return dirname($_SERVER['DOCUMENT_ROOT']) . '/app/src/Wigets';
                break;
            case '@templates':
                return dirname($_SERVER['DOCUMENT_ROOT']) . '/app/src/templates';
                break;
            case '@resources':
                return dirname($_SERVER['DOCUMENT_ROOT']) . '/WWW/resources';
                break;
            default:
               return dirname($_SERVER['DOCUMENT_ROOT']) . '/app/src';
        }
    }

    public function setAliasTest()
    {
        return json_decode(json_encode([
            'webroot' => dirname(__DIR__, 2) . '/WWW',
            'config' => dirname(__DIR__) . '/config'
        ]));
    }

    public function setRequest()
    {
        if($setRequest = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'))){
            return $setRequest;
        }
    }

    public static function setTemplates($files, $params = [])
    {

        if(isset($params['data'])){
            extract([
                'data' => $params["data"],
                'metadata' => $params["data"][0] ?? null
            ]);
        }

        if (file_exists($files.'.php')) {
            require $files.'.php';
        } else {
            echo "Файл $files не существует";
        }
    }

    public function setAssets($position)
    {
        $obj = new \app\Core\BootstrapCore($position);
        return $obj;
    }

    public function setWidget()
    {
        new \app\Widgets\IndexWiget();
    }
}


// var_dump (\DolinaIS::getName()->isGuest);
// var_dump (\DolinaIS::setAlias('@config'));