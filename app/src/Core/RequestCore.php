<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Core;

class RequestCore
{

    private $storage;

    public function __construct()
    {
        $this->storage = $this->cleanInput($_REQUEST);
    }

    public function __get($name)
    {
        if (isset($this->storage[$name])) return $this->storage[$name];
    }

    private function cleanInput($data)
    {
        if (is_array($data)) {
            $cleaned = [];
            foreach ($data as $key => $value) {
                $cleaned[$key] = $this->cleanInput($value);
            }
            return $cleaned;
        }
        return trim(htmlspecialchars($data, ENT_QUOTES));
    }

    public function getRequestEntries()
    {
        return $this ->storage;
    }
}
