<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Core;

class LogCore
{
	public function __construct($fileName, $data)
    {
        $fh = fopen($fileName, 'a');
        fwrite($fh, date('d-m-Y H:i:s') . " - " . $data . "\n");
        fclose($fh);
  	}

    public static function Log($fileName, $data){
        file_put_contents(APPLOG . '/'.$fileName.'.txt', date('Y-m-d H:i:s') . ' ' . print_r($data, true) . PHP_EOL, FILE_APPEND);
    }
}
