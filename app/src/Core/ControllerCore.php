<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Core;

use app\Core\RenderCore;

class ControllerCore
{

    public $name;
    public $title;
    public $description;
    public $keywords;

    public function rendercontroller($get_class_name, $__view, $data, $template){
        $obj = new RenderCore($get_class_name, $__view, $data, $template);
        return $obj;
    }
}
