<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

declare(strict_types=1);

namespace app\Core;

class PdoCore extends \PDO
{
    public static function initialize()
    {
        try {
            $con = new \PDO(
                "mysql:host=". env('DB_HOST')
                . ";dbname=". env('DB_NAME'),
                env('DB_USERNAME'), env('DB_PASSWORD')
            );
            $con->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $con;
        } catch (\PDOException $e) {
            die("Could not connect to the database: $e");
        }
    }
}