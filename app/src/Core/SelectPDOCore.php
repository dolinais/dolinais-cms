<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

declare(strict_types=1);

namespace app\Core;

use app\Core\PdoCore;

class SelectPDOCore extends PdoCore
{
	private static $app;
    public static $sth;
    public static $table;

    public $getInsert;
    public $getUpdate;
    public $findAll;
    public $findOne;
    public $fields;

    public $select;
    public $from;
    public $join;
    public $on;
    public $where;
    public $like;
    public $groupBy;
    public $orderBy;
    public $limit;

    public function __construct($fields = '*')
    {
        $this->fields = "SELECT $fields";
    }

    public static function get(string $fields = '*')
	{
		self::$table = static::tableName();
        self::$app = new SelectPDOCore($fields ?? '*');
		return self::$app;
	}

    public function select($conditions)
    {
        $this->fields = $this->clause('SELECT', $conditions);
        return $this;
    }

    /**
     * Создание таблицы.
     */

    public static function createTable($table= "company", $array = array()){
        try {
            $db = self::initialize();
            $db->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
            $sql ="CREATE table $table(".implode(",", array_filter(array_map(function($v){
                return implode(", ", array_filter($v));
            }, $array))).");" ;
            $db->exec($sql);
            return "Created $table Table.\n";
        } catch(\PDOException $e) {
            echo $e->getMessage();
        }

        // $array = array(
        //     array("ID INT( 11 ) AUTO_INCREMENT PRIMARY KEY"),
        //     array("Prename VARCHAR( 50 ) NOT NULL"),
        //     array("Name VARCHAR( 250 ) NOT NULL")
        //  );
        // self::create('company', $array);
    }


    /**
     * Получение всех записей.
     */

    public function findAll($param = array())
    {
        self::$sth = self::initialize()->prepare("SELECT * FROM ".self::$table." ");
        self::$sth->execute((array) $param);
        return self::$sth->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findOne($param = array())
    {
        $query = "SELECT * FROM ".self::$table." WHERE";
		$values = array();

		foreach ($param as $name => $value) {
		    $query .= ' '.$name.' = :'.$name.' AND ';
		    $values[':'.$name] = $value;
		}

		$query = substr($query, 0, -4).' LIMIT 50;';
		$sth = self::initialize()->prepare($query);
		$sth->execute($values);
		return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getInsert($insert)
    {
        $db = self::initialize();
        $query = "INSERT INTO ".self::$table." SET";
		$values = array();

		foreach ($insert as $name => $value) {
		    $query .= ' '.$name.' = :'.$name.',';
		    $values[':'.$name] = $value;
		}

		$query = substr($query, 0, -1).';';
		$sth = $db->prepare($query);
		$sth->execute($values);
        return $db->lastInsertId();
    }

    public function getUpdate($update, $id)
    {

        $query = "UPDATE ".self::$table." SET";
		$values = array();

		foreach ($update as $name => $value) {
		    $query .= ' '.$name.' = :'.$name.',';
		    $values[':'.$name] = $value;
		}

		$query = substr($query, 0, -1).' WHERE `id` = '.$id.';';

		$sth = self::initialize()->prepare($query);
		$sth->execute($values);
    }

    public static function getDelete($param = array())
    {
    	$query = "DELETE FROM ".self::$table." WHERE";
		$values = array();

		foreach ($param as $name => $value) {
		    $query .= ' '.$name.' = :'.$name.',';
		    $values[':'.$name] = $value;
		}

		$query = substr($query, 0, -1).';';

		$sth = self::initialize()->prepare($query);
		$sth->execute($values);
    }

    public function from(string $table='')
    {
        $this->from = "FROM ".self::$table." $table";
        return $this;
    }

    public function join($conditions)
    {
        $this->join = $this->clause('JOIN', $conditions);
        return $this;
    }

    public function on($conditions)
    {
        $this->on = $this->clause('ON', $conditions);
        return $this;
    }

    public function where($conditions)
    {
        $this->where = $this->clause('WHERE', $conditions);
        return $this;
    }


    public function like($data = '', $conditions = '')
    {
        $this->like = $this->clause('WHERE '.$data.'', ['LIKE "%'.$conditions.'%"']);
        return $this;
    }

    public function clause(string $prefix, $conditions) : string
    {
        $array[] = $prefix;
        foreach ($conditions as $condition) {
            $array[] = is_array($condition) ?
                implode(' ', $condition) :
                $condition;
        }
        return implode(' ', $array);
    }

    public function groupBy(string $field = '', string $group = 'ASC')
    {
        if (empty($field)) return $this;
        $this->groupBy = "GROUP BY $field $group";
        return $this;
    }

    public function orderBy(string $field = '', string $order = 'ASC')
    {
        if (empty($field)) return $this;
        $this->orderBy = "ORDER BY $field $order";
        return $this;
    }

    public function limit($LIMIT = 100)
    {
        $this->limit = "LIMIT {$LIMIT}";
        return $this;
    }

    public function fetch() {
        $sql = implode(' ', (array)$this);
        // var_dump($sql);
        // exit;
        $db = self::initialize();
        $db->beginTransaction();
        $query = $db->prepare($sql);
        $db->commit();
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_OBJ);
    }
}