<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Core;

class RenderCore
{

	public function __construct($get_class_name, $__view, $data=[], $template='default')
    {
        $this->FilesExit(str_replace("\\", "/", $get_class_name), $__view);
        if(!$template){
            require TEMPLATES . '/'.str_replace("\\", "/", $get_class_name).'/'.$__view.'.php';
        } else{
            $this->render(str_replace("\\", "/", $get_class_name), $__view, [
                'content' => str_replace("\\", "/", $get_class_name).'/'.$__view,
                'data' => $data,
            ]);
        }
  	}
	function render($get_class_name, $__view, $params = [])
    {
        if (!empty($params)) extract($params);

        if (file_exists(TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php')) {
            ob_start();
                require \DolinaIS::setAlias('@templates').'/layouts/main.php';
                // require TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php';
            ob_get_flush();
        }
    }
    function FilesExit($get_class_name, $__view)
    {
        if (!file_exists(TEMPLATES . '/'.$get_class_name)){
            mkdir(TEMPLATES . '/'.$get_class_name, 0755, true);
        }
        if (!file_exists(TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php')) {
            file_put_contents(TEMPLATES . '/'.$get_class_name.'/'.$__view.'.php', '<div>HTML code</div>');
        }
    }
}
