<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

declare(strict_types=1);

namespace app\Core;

class Router
{

    private $routes;

    public function addRoute($pattern, $function)
    {
        $this->routes['{'.$pattern.'}'] = $function;
    }

    public function run()
    {
        foreach( $this->routes as $pattern => $function) {
            if( preg_match($pattern, $_SERVER['REQUEST_URI'], $params) ) {
                array_shift($params);
                array_unshift($params, $_SERVER['REQUEST_URI']);
                return call_user_func_array($function, array_values($params));
            }
        }
        return false;
    }

}