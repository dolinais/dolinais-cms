<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

/**
 * Redirect to another URL
 *
 * @param string $url
 * @return void
 */

namespace app\Core;

class RedirectCore
{

    public function __construct(string $url)
    {
        header('Location:' . $url);
        exit;
    }

    public static function to(string $url): void
    {
        header('Location:' . $url);
        exit;
    }
}
