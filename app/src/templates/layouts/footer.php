<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

?>
<?php if(\DolinaIS::getName()->Request[0] != 'messenger'): ?>
<?//=\DolinaIS::setTemplates(\DolinaIS::setAlias('@widget').'/views/messager', array('data'=> $data)); ?>
<?php endif; ?>
<footer class="container py-5">
    <div class="row">
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted"><?=\DolinaIS::$copyright; ?> <?=date('Y'); ?></small>
        </div>
        <div class="col-6 col-md">
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="/document-api">API</a></li>
                <li><a class="text-muted" href="/razrabotchikam">Разработчикам</a></li>
                <li><a class="text-muted" href="/help">Документация</a></li>
                <li><a class="text-muted" href="/download">Скачать</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="/account">Личный кабинет</a></li>
                <li><a class="text-muted" href="/about">О нас</a></li>
                <li><a class="text-muted" href="/contacts">Контакты</a></li>
                <li><a class="text-muted" href="/license">Лицензия</a></li>
            </ul>
        </div>
    </div>
</footer>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.11.1/font/bootstrap-icons.min.css" crossorigin="anonymous">
<?php \DolinaIS::getName()->setAssets('footer'); ?>
<!-- <script type="text/javascript" src="/resources/js/nodeServer.js" ></script> -->
<script src="https://cdn.tiny.cloud/1/fzv601e7vu03lyqo5mi7yogg7171bdx5yl137c9zhvb38ii9/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<!-- <script src="/resources/js/tinymce.min.js" referrerpolicy="origin"></script> -->

<script>
    tinymce.init({
      selector: '#textarea-tinymce',
      plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
    });
</script>
<?=\DolinaIS::setTemplates(\DolinaIS::setAlias('@widget').'/views/auntification', array('data'=> $data)); ?>
</body>
</html>