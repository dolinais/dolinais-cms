<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */
?>
<?=\DolinaIS::setTemplates(\DolinaIS::setAlias('@templates').'/layouts/header', array('data'=> $data)); ?>
<?=\DolinaIS::setTemplates(\DolinaIS::setAlias('@templates').'/'.$content, array('data'=> $data)); ?>
<?=\DolinaIS::setTemplates(\DolinaIS::setAlias('@templates').'/layouts/footer', array('data'=> $data)); ?>