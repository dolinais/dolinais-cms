<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */
?>
<!doctype html>
<html lang="ru">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<title><?=$metadata->title; ?></title>
<meta name="keywords" content="<?=$metadata->keywords ?? null; ?>">
<meta name="description" content="<?=$metadata->description ?? null; ?>">
<?php \DolinaIS::getName()->setAssets('header'); ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();
   for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
   k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(97067768, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/97067768" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body>
<header>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid justify-content-between">
            <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent"
            aria-controls="navbarToggleExternalContent1"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <i class="fas fa-bars text-dark"></i>
        </button>
            <!-- Left elements -->
            <div class="d-flex">
                <!-- Brand -->
                <a class="navbar-brand me-2 mb-1 d-flex align-items-center" href="/">
                    <img src="/resources/img/logo.png" height="20" alt="" loading="lazy"
                    style="margin-top: 2px;"/>
                </a>
                <!-- Left links -->
                <ul class="navbar-nav flex-row d-none d-md-flex">
                    <? foreach(app\models\MenuModels::get()->findAll() as $menu): ?>
                        <li class="nav-item">
                          <a class="nav-link" href="/<?=$menu['slug']; ?>"><?=$menu['title']; ?></a>
                      </li>
                  <? endforeach; ?>
                </ul>
                <!-- Left links -->
            </div>
            <!-- Left elements -->

            <!-- Center elements -->
            <? if(\DolinaIS::getName()->isGuest): ?>
                    <ul class="navbar-nav flex-row d-none d-md-flex">
                        <li class="nav-item me-3 me-lg-1 active">
                            <a class="nav-link" href="/account">
                                <span><i class="fas fa-cog fa-lg"></i></span>
                            </a>
                        </li>
                        <li class="nav-item me-3 me-lg-1">
                            <a class="nav-link" href="#">
                                <span><i class="fas fa-flag fa-lg"></i></span>
                            </a>
                        </li>
                        <li class="nav-item me-3 me-lg-1">
                            <a class="nav-link" href="#">
                                <span><i class="fas fa-video fa-lg"></i></span>
                                <span class="badge rounded-pill badge-notification bg-danger">100</span>
                            </a>
                        </li>
                        <li class="nav-item me-3 me-lg-1">
                            <a class="nav-link" href="#">
                              <span><i class="fas fa-shopping-bag fa-lg"></i></span>
                              <span class="badge rounded-pill badge-notification bg-danger">10</span>
                          </a>
                        </li>
                        <li class="nav-item me-3 me-lg-1">
                            <a class="nav-link" href="#">
                              <span><i class="fas fa-users fa-lg"></i></span>
                              <span class="badge rounded-pill badge-notification bg-danger">2</span>
                          </a>
                        </li>
                    </ul>
                <? endif; ?>
            <!-- Center elements -->
            <!-- Right elements -->
            <ul class="navbar-nav flex-row">
                <? if(\DolinaIS::getName()->isGuest): ?>
                <li class="nav-item me-3 me-lg-1">
                    <a class="nav-link" href="/page/create">
                      <span><i class="fas fa-plus-circle fa-lg"></i></span>
                    </a>
                </li>
                <li class="nav-item me-3 me-lg-1">
                    <a class="nav-link" href="/messenger?sel=2">
                        <span>
                            <i class="fas fa-comments fa-lg"></i>
                             <span class="badge rounded-pill badge-notification bg-danger" id="notificationsApiController">6</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item dropdown me-3 me-lg-1">
                    <a class="nav-link dropdown-toggle hidden-arrow" href="#" id="navbarDropdownMenuLink" role="button" data-mdb-toggle="dropdown" aria-expanded="false">
                        <i class="fas fa-bell fa-lg"></i>
                        <span class="badge rounded-pill badge-notification bg-danger">12</span>
                    </a>
                </li>
                <li class="nav-item dropdown me-3 me-lg-1">
                    <a class="nav-link dropdown-toggle hidden-arrow" href="#" id="navbarDropdownMenuLink" role="button" data-mdb-toggle="dropdown" aria-expanded="false">
                        <i class="fas fa-chevron-circle-down fa-lg"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownMenuLink">
                        <li>
                            <a class="dropdown-item" href="#">Статьи</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">Скачать</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">Контакты</a>
                        </li>
                    </ul>
                </li>
                <? endif; ?>
                <li class="nav-item me-3 me-lg-1">
                    <? if(\DolinaIS::getName()->isGuest()): ?>
                    <form action="/account/auth" method="post">
                    <a class="nav-link d-sm-flex align-items-sm-center" href="javascript:;" onclick="parentNode.submit();">
                        <input type="hidden" name="logout" value="">
                        <img src="/resources/img/sign-out-icon-vector-illustration.png" class="rounded-circle" height="22" alt="Black and White Portrait of a Man" loading="lazy"/>
                        <strong class="d-none d-sm-block ms-1">Выйти</strong>
                    </a>
                    </form>
                <? else: ?>
                    <a class="nav-link d-sm-flex align-items-sm-center" href="#" data-mdb-toggle="modal" data-mdb-target="#exampleModalReg">
                        <img src="/resources/img/sign-in-icon-vector-illustration.png" class="rounded-circle" height="22" alt="Black and White Portrait of a Man" loading="lazy"/>
                        <strong class="d-none d-sm-block ms-1">Войти</strong>
                    </a>
                <? endif; ?>
                </li>
            </ul>
            <!-- Right elements -->
        </div>
        <ul class="navbar-nav collapse" id="navbarToggleExternalContent">
            <table class="table">
              <tbody>
                <? foreach(app\models\MenuModels::get()->findAll() as $menu): ?>
                <tr>
                  <th scope="row"><a class="nav-link" href="/<?=$menu['slug']; ?>"><?=$menu['title']; ?></a></th>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <? endforeach; ?>
              </tbody>
            </table>
        </ul>
    </nav>
    <!-- Navbar -->
</header>
