<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */
?>

<content>
    <hr>
    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$metadata->description; ?> Not Found</li>
            </ol>
        </nav>
    </div>
    <div class="container">
        <h1><?=$metadata->description; ?></h1>
        <div class="p-3 mb-4 bg-body-tertiary rounded-3">
            <div class="container-fluid py-5">
                <p class="display-5 fw-bold">Ops.. нет такой страницы :(</p>
            </div>
        </div>
    </div>
    <hr>
</content>
