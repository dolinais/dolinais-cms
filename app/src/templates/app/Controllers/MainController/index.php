<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */
?>
<div class="p-5 mb-4 bg-body-tertiary rounded-3">
      <div class="container-fluid py-5">
        <h1 class="display-5 fw-bold">[[blocks.title.4]]</h1>
        <p class="col-md-8 fs-4">[[blocks.description.4]]</p>
        <a href="/download" class="btn btn-primary btn-lg" type="button">Скачать</a>
      </div>
</div>
<content>
    <hr>
    <div class="container">
        <h1>[[blocks.title.3]]</h1>
        <div class="row">
            <div class="col">
                [[blocks.text.3]]
            </div>
        </div>
    </div>
    <hr>
</content>
