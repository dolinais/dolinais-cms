<?php



?>
<div class="p-5 mb-4 bg-body-tertiary rounded-3">
	<div class="container-fluid py-5">
		<h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
		<p class="col-md-8 fs-4"><?=$data["description"]; ?></p>
		<button class="btn btn-primary btn-lg" type="button">Узнать подробно</button>
	</div>
</div>
<content>
	<hr>
	<div class="container">
		<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
			</ol>
		</nav>
	</div>
	<div class="container">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
  			<li class="nav-item" role="presentation">
    			<button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Главная</button>
  			</li>
  			<li class="nav-item" role="presentation">
    			<button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Профиль</button>
  			</li>
  			<li class="nav-item" role="presentation">
    			<button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Контакт</button>
  			</li>
		</ul>
		<div class="tab-content" id="myTabContent">
  			<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
				Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.
  			</div>
		  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
		  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
		</div>
	</div>
	<hr>
</content>
