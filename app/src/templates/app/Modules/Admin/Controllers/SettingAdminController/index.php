<div class="p-5 mb-4 bg-body-tertiary rounded-3">
	<div class="container-fluid py-5">
		<h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
		<p class="col-md-8 fs-4"><?=$data["description"]; ?></p>
		<button class="btn btn-primary btn-lg" type="button">Узнать подробно</button>
	</div>
</div>
<content>
	<hr>
	<div class="container">
		<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
			</ol>
		</nav>
	</div>
	<div class="container">
		<h1><?=$data["title"]; ?></h1>
		<div class="row">
			<div class="col">
				Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.
			</div>
		</div>
	</div>
	<hr>
	<div class="container mt-4 mb-4">
		<form class="row g-3">
			<div class="col-md-6">
				<label for="inputEmail4" class="form-label">Эл. адрес</label>
				<input type="email" class="form-control" id="inputEmail4">
			</div>
			<div class="col-md-6">
				<label for="inputPassword4" class="form-label">Пароль</label>
				<input type="password" class="form-control" id="inputPassword4">
			</div>
			<div class="col-12">
				<label for="inputAddress" class="form-label">Адрес</label>
				<input type="text" class="form-control" id="inputAddress" placeholder="Проспект Ленина">
			</div>
			<div class="col-12">
				<label for="inputAddress2" class="form-label">Адрес 2</label>
				<input type="text" class="form-control" id="inputAddress2" placeholder="Квартира">
			</div>
			<div class="col-md-6">
				<label for="inputCity" class="form-label">Город</label>
				<input type="text" class="form-control" id="inputCity" placeholder="Брянск">
			</div>
			<div class="col-md-4">
				<label for="inputState" class="form-label">Область</label>
				<select id="inputState" class="form-select">
					<option selected>Выберите...</option>
					<option>...</option>
				</select>
			</div>
			<div class="col-md-2">
				<label for="inputZip" class="form-label">Индекс</label>
				<input type="text" class="form-control" id="inputZip">
			</div>
			<div class="col-12">
				<div class="form-check">
					<input class="form-check-input" type="checkbox" id="gridCheck">
					<label class="form-check-label" for="gridCheck">
						Проверить меня
					</label>
				</div>
			</div>
			<div class="col-12">
				<div class="row justify-content-md-center">
					<div class="col-md-12 col-lg-8">
						<h1 class="h2 mb-4">Добавь новую статью</h1>
						<label>Старайся писать красиво!</label>
						<div class="form-group">
							<textarea></textarea>
						</div><br>
						<button type="submit" class="btn btn-primary">Опубликовать</button>
					</div>
				</div>
			</div>
		</form>
		<!--Bootstrap classes arrange web page components into columns and rows in a grid -->
	</div>
</content>
