<div class="p-5 mb-4 bg-body-tertiary rounded-3">
	<div class="container-fluid py-5">
		<h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
		<p class="col-md-8 fs-4"><?=$data["description"]; ?></p>
		<!-- <button onclick="window.location = '/<?=$data["post"][0]["slug"]; ?>';" class="btn btn-primary btn-lg" type="button">Узнать подробно</button> -->
	</div>
</div>
<content>
	<div class="container">
		<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
			</ol>
		</nav>
	</div>
	<div class="container">
		<div class="row <? if(\app\Core\MobileCore::IsMobile()){ ?>row-cols-1<?}else{?>row-cols-2<?}?>">
			<? foreach(app\Modules\Page\Models\PageModels::get()->from()->where(['articles_status', '=', 1])->orderBy('created_at', 'DESC')->limit(20)->fetch() as $post): ?>
				<? if(!app\Models\FilesModels::get()->findOne(
	                array(
	                    'file_page_id' => $post->id
	                )
	            )): ?>
				<div class="col">
					<? if(strlen($post->title) > 20): ?>
						<a href="/<?=$post->slug; ?>"><h1><?=mb_substr($post->title, 0, 22); ?>...</h1></a>
					<? else: ?>
						<a href="/<?=$post->slug; ?>"><h1><?=mb_substr($post->title, 0, 20); ?></h1></a>
					<? endif; ?>
					<p><?=mb_substr($post->description, 0, 149); ?>...</p>
					<nav class="navbar navbar-expand-lg navbar-light bg-light">
						<div class="container-fluid justify-content-between">
							<ul class="navbar-nav flex-row">
								<li class="nav-item dropdown me-3 me-lg-1">
									<a class="nav-link dropdown-toggle hidden-arrow" href="#">
				                    	<i class="fas fa-commenting fa-lg"></i>
				                    	<span class="badge rounded-pill badge-notification bg-danger"><?=$post->id; ?>6</span>
				                    </a>
				                </li>
				                <li class="nav-item dropdown me-3 me-lg-1">
				                    <a class="nav-link dropdown-toggle hidden-arrow" href="#">
				                    	<i class="fas fa-heart fa-lg"></i>
				                    	<span class="badge rounded-pill badge-notification bg-danger"><?=$post->id; ?>10</span>
				                    </a>
				                </li>
				                <!-- <li class="nav-item dropdown me-3 me-lg-1">
				                    <a class="nav-link dropdown-toggle hidden-arrow" href="#">
				                    	<i class="fas fa-share-alt fa-lg"></i>
				                    </a>
				                </li> -->
				            </ul>
				        </div>
				    </nav>
					<hr>
				</div>
				<? endif; ?>
			<? endforeach; ?>
		</div>
		<nav aria-label="Пример навигации по страницам">
			<ul class="pagination">
				<li class="page-item"><a class="page-link" href="#">Предыдущая</a></li>
				<li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item"><a class="page-link" href="#">Следующая</a></li>
			</ul>
		</nav>
	</div>

	<div class="container">
		<div class="row <? if(\app\Core\MobileCore::IsMobile()){ ?>row-cols-1<?}else{?>row-cols-3<?}?>">
			<? foreach($data["post"] as $post): ?>
				<? if($img = app\Models\FilesModels::get()->findOne(
	                array(
	                    'file_page_id' => $post['id'],
	                    'file_status' => 1
	                )
	            )): ?>
				<div class="col">
					<div class="card" style="margin: 10px;">
						<img src="/uploads/<?=$img[0]['file_name']; ?>" class="card-img-top" alt="<?=$post['title']; ?>" width="auto" height="220">
						<div class="card-body">
							<? if(strlen($post['title']) > 20): ?>
								<h5 class="card-title"><?=mb_substr($post['title'], 0, 13); ?>...</h5>
							<?php else: ?>
								<h5 class="card-title"><?=mb_substr($post['title'], 0, 18); ?></h5>
							<?php endif; ?>
							<p class="card-text"><?=mb_substr(strip_tags($post['description']), 0, 38); ?>...</p>
							<a href="/<?=$post['slug']; ?>" class="btn btn-primary">Читать</a>
						</div>
					</div>
				</div>
				<? endif; ?>
			<? endforeach; ?>
		</div>
	</div>
	<hr>
</content>
