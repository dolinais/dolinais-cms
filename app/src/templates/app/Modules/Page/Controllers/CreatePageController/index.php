<div class="p-2 mb-4 bg-body-tertiary rounded-3">
	<div class="container-fluid py-5">
		<h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
	</div>
</div>

<content>
	<div class="container">
		<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
			</ol>
		</nav>
	</div>

	<div class="container">
		<div class="tree ">
		    <ul>
		    	<div>Чтобы создать новый раздел, модуль или страницу: выберите пункт раздела и нажмите на <i class="fa fa-plus" aria-hidden="true"></i></div>
		    	<?php foreach($data["TreePageModels"] as $category): ?>
		        <li> <span><i class="fa fa-folder-open"></i><?=$category["page_title"]; ?></span>
		        	<i class="fa fa-plus" aria-hidden="true" onclick="location.href='/page/create?id=<?=$category["id"]; ?>';"></i>
		        	<i class="fa fa-cog" aria-hidden="true" onclick="location.href='/page/update?tree_id=<?=$category["id"]; ?>';"></i>
		            <ul>
		            	<?php if(isset($category["children"])): ?>
		        			<?php foreach($category["children"] as $child): ?>
		        				<?php if(isset($_GET["id"]) && $_GET["id"] == $child["id"]): ?>
		        					<li><span style="background:#eee; border:1px solid #94a0b4; color:#000"><i class="fa fa-minus-square"></i><?=$child["page_title"]; ?></span>
								<?php else: ?>
		                			<li><span ><i class="fa fa-minus-square"></i><?=$child["page_title"]; ?></span>
	                			<?php endif; ?>
	                			<i class="fa fa-plus" aria-hidden="true" onclick="location.href='/page/create?id=<?=$child["id"]; ?>';"></i>
	                			<i class="fa fa-cog" aria-hidden="true" onclick="location.href='/page/update?tree_id=<?=$child["id"]; ?>';"></i>
	                			<i class="fa fa-trash" aria-hidden="true"></i>
			                    	<?php if(isset($child["children"])): ?>
			                    		<ul>
			                    		<?php foreach($child["children"] as $children): ?>
			                        		<?php if(isset($_GET["id"]) && $_GET["id"] == $children["id"]): ?>
					        					<li><span style="background:#eee; border:1px solid #94a0b4; color:#000"><i class="fa fa-minus-square"></i><?=$children["page_title"]; ?></span>
											<?php else: ?>
					                			<li><span ></i><?=$children["page_title"]; ?></span>
				                			<?php endif; ?>
			                        			<?if($children["page_type"] === 'page_category'): ?>
			                        				<i class="fa fa-plus" aria-hidden="true" onclick="location.href='/page/create?id=<?=$children["id"]; ?>';"></i>
			                        			<? endif; ?>
			                        			<i class="fa fa-cog" aria-hidden="true" onclick="location.href='/page/update?tree_id=<?=$children["id"]; ?>';"></i>
			                        			<i class="fa fa-trash" aria-hidden="true"></i>
				                        		<?php if(isset($children["children"])): ?>
				                        			<ul>
					                        		<?php foreach($children["children"] as $children3): ?>
					                        			<?php if(isset($_GET["id"]) && $_GET["id"] == $children3["id"]): ?>
								        					<li><span style="background:#eee; border:1px solid #94a0b4; color:#000"><i class="fa fa-minus-square"></i><?=$children3["page_title"]; ?></span>
														<?php else: ?>
								                			<li><span ><?=$children3["page_title"]; ?></span>
							                			<?php endif; ?>
					                        		<?php endforeach; ?>
					                        		</ul>
				                        		<? endif; ?>
			                        		</li>
			                        	<?php endforeach; ?>
			                        	</ul>
		                        	<? endif; ?>
	                			</li>
		            		<?php endforeach; ?>
		                <? endif; ?>
		            </ul>
		        </li>
		    	<?php endforeach; ?>
		    </ul>
		</div>
	</div>
	<?php if(isset($_GET["id"])): ?>
	<div class="container">
		<div class="row row-cols-2">
			<form enctype="multipart/form-data" method="POST">
				<fieldset>
					<?=$data["error"]; ?>
    				<!-- <legend>Создать новую запись</legend> -->
    				<div class="mb-3">
						<input type="text" name="TitlePage" class="form-control" placeholder="Заголовок страницы" value="">
						<div class="form-text" id="basic-addon4">Заголовок страницы</div>
					</div>
					<div class="mb-3">
					    <div class="form-floating">
							<select name="TypePageModels"  class="form-select" id="floatingSelect" aria-label="Floating label select example">
								<?php foreach($data["TypePageModels"] as $TypePageModels): ?>
				      				<option value="<?=$TypePageModels->type_code; ?>"><?=$TypePageModels->type_title; ?></option>
				    			<?php endforeach; ?>
							</select>
							<label for="floatingSelect">Тип страницы</label>
						</div>
					</div>
				    <div class="mb-3">
				      <select name="ModulesModels" class="form-select">
				      	<option value="new_modules">Новый модуль</option>
				      	<?php foreach($data["ModulesModels"] as $ModulesModels): ?>
				        <option value="<?=$ModulesModels['module_id']; ?>"><?=$ModulesModels['title']; ?></option>
				    	<?php endforeach; ?>
				      </select>
				      <label for="floatingSelect">Модуль</label>
				    </div>
				    <div class="form-floating mb-3">
					 	<textarea name="TextPost" class="form-control" placeholder="Основной текст" id="floatingTextarea2Disabled" style="height: 100px"></textarea>
					</div>
					<div class="mb-3">
						<input name="UrlPage" type="text" id="disabledTextInput" class="form-control" placeholder="URL страницы" value="">
						<div class="form-text" id="basic-addon4">URL страницы заполняется автоматически, можно не заполнять</div>
					</div>
					<!-- <div class="mb-3">
						<label for="formFileLg" class="form-label">Главное изображение</label>
						<input name="filePost" class="form-control form-control-lg" id="formFileLg" type="file" />
					</div> -->
    				<div class="mb-3">
      					<div class="form-check">
					        <input name="StatusPage" class="form-check-input" type="checkbox" id="disabledFieldsetCheck">
					        <label class="form-check-label" for="disabledFieldsetCheck">
					          Включить
					        </label>
      					</div>
    				</div>
    				<button name="SubmitPost" type="submit" class="btn btn-primary">Создать</button>
  				</fieldset>
			</form>
		</div>
	</div>
	<?php endif; ?>
<hr class="border border-primary border-3 opacity-75">
</content>

<script>
 	$(function () {
		$('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
		$('.tree li.parent_li > span').on('click', function (e) {
          	var children = $(this).parent('li.parent_li').find(' > ul > li');
          	if (children.is(":visible")) {
      			children.hide('fast');
      			$(this).attr('title', 'Expand this branch').find(' > i').addClass('fa-plus-square').removeClass('fa-minus-square');
          	} else {
          		children.show('fast');
          		$(this).attr('title', 'Collapse this branch').find(' > i').addClass('fa-minus-square').removeClass('fa-plus-square');
          	}
          e.stopPropagation();
      });
  });

</script>
<style>
.fa:hover {
  color: #00B3FF;
  cursor: pointer;
}
.tree {
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
}
.tree li {
    list-style-type:none;
    margin:0;
    padding:10px 5px 0 5px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-20px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:50px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:20px;
    top:30px;
    width:25px
}
.tree li span {
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:3px 8px;
    text-decoration:none
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:30px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}
</style>
