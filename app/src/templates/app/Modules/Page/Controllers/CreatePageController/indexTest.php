<div class="p-2 mb-4 bg-body-tertiary rounded-3">
	<div class="container-fluid py-5">
		<h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
	</div>
</div>
<content>
	<div class="container">
		<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
			</ol>
		</nav>
	</div>
	<ul class="tree-padding tree-vertical-lines tree-horizontal-lines tree-summaries tree-markers tree-buttons">
<li>
    <details open>
    	<?php foreach($data["TreePageModels"] as $category): ?>
			<summary><?=$category["page_title"]; ?><span style="font-size: 12px; color: blue; position: absolute; margin-top: 5px;"><a href="/page/create?id=<?=$category["id"]; ?>">&nbsp;cозд.</a></span></summary>
			<?php if(isset($category["children"])): ?>
				<ul>
					<?php foreach($category["children"] as $child): ?>
					    <li>
					      <details>
					        <summary><?=$child["page_text"]; ?><span style="font-size: 12px; color: blue; position: absolute; margin-top: 5px;"><a href="/page/create?id=<?=$child["id"]; ?>">&nbsp;cозд.</a></span></summary>
					        <?php if(isset($child["children"])): ?>
						        <ul>
						        	<?php foreach($child["children"] as $children): ?>
					          			<li><?=$children["page_title"]; ?></li>
					      			<?php endforeach; ?>
						        </ul>
					        <? endif; ?>
					      </details>
					    </li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		<?php endforeach; ?>
	</details>
</li>
</ul>
	<div class="container">
		<div class="row row-cols-2">
			<form enctype="multipart/form-data" method="POST">
				<fieldset>
					<?=$data["error"]; ?>
    				<!-- <legend>Создать новую запись</legend> -->
    				<div class="mb-3">
						<input type="text" name="TitlePage" class="form-control" placeholder="Заголовок страницы" value="">
						<div class="form-text" id="basic-addon4">Заголовок страницы</div>
					</div>
					<div class="mb-3">
					    <div class="form-floating">
							<select name="TypePageModels"  class="form-select" id="floatingSelect" aria-label="Floating label select example">
								<?php foreach($data["TypePageModels"] as $TypePageModels): ?>
				      				<option value="<?=$TypePageModels->id; ?>"><?=$TypePageModels->type_title; ?></option>
				    			<?php endforeach; ?>
							</select>
							<label for="floatingSelect">Тип страницы</label>
						</div>
					</div>
				    <!-- <div class="mb-3">
				      <label for="disabledSelect" class="form-label">Выбрать категорию</label>
				      <select name="CategoryPost" class="form-select">
				      	<?php foreach($data["СategoriesPageModels"] as $СategoriesPageModels): ?>
				        <option value="<?=$СategoriesPageModels['id']; ?>"><?=$СategoriesPageModels['name']; ?></option>
				    	<?php endforeach; ?>
				      </select>
				    </div> -->
				    <!-- <div class="form-floating mb-3">
					 	<textarea name="TextPost" class="form-control" placeholder="Основной текст" id="floatingTextarea2Disabled" style="height: 100px"></textarea>
					</div> -->
					<div class="mb-3">
						<input name="UrlPage" type="text" id="disabledTextInput" class="form-control" placeholder="URL страницы" value="">
						<div class="form-text" id="basic-addon4">URL страницы заполняется автоматически, можно не заполнять</div>
					</div>
					<!-- <div class="mb-3">
						<label for="formFileLg" class="form-label">Главное изображение</label>
						<input name="filePost" class="form-control form-control-lg" id="formFileLg" type="file" />
					</div> -->
    				<div class="mb-3">
      					<div class="form-check">
					        <input name="StatusPage" class="form-check-input" type="checkbox" id="disabledFieldsetCheck">
					        <label class="form-check-label" for="disabledFieldsetCheck">
					          Включить
					        </label>
      					</div>
    				</div>
    				<button name="SubmitPost" type="submit" class="btn btn-primary">Создать</button>
  				</fieldset>
			</form>
		</div>
	</div>
<hr class="border border-primary border-3 opacity-75">
</content>
<style>
:root{
  --width      : 36;
  --rounding   : 4px;
  --accent     : #696;
  --dark-grey  : #ddd;
  --grey       : #eee;
  --light-grey : #f8f8f8;
}
*, ::before, ::after {
    box-sizing: border-box;
    margin: 0;
}
.tree-padding{
  --spacing    : 1.5rem;
  --radius     : 10px;
  padding-left : 1rem;
}

.tree-padding li{
  display      : block;
  position     : relative;
  padding-left : calc(2 * var(--spacing) - var(--radius) - 2px);
}

.tree-padding ul{
  margin-left  : calc(var(--radius) - var(--spacing));
  padding-left : 0;
}

.tree-vertical-lines ul li{
  border-left : 2px solid var(--dark-grey);
}

.tree-vertical-lines ul li:last-child{
  border-color : transparent;
}

.tree-horizontal-lines ul li::before{
  content      : '';
  display      : block;
  position     : absolute;
  top          : calc(var(--spacing) / -2);
  left         : -2px;
  width        : calc(var(--spacing) + 2px);
  height       : calc(var(--spacing) + 1px);
  border       : solid var(--dark-grey);
  border-width : 0 0 2px 2px;
}

.tree-summaries summary{
  display : block;
  cursor  : pointer;
}

.tree-summaries summary::marker,
.tree-summaries summary::-webkit-details-marker{
  display : none;
}

.tree-summaries summary:focus{
  outline : none;
}

.tree-summaries summary:focus-visible{
  outline : 1px dotted #000;
}

.tree-markers li::after,
.tree-markers summary::before{
  content       : '';
  display       : block;
  position      : absolute;
  top           : calc(var(--spacing) / 2 - var(--radius));
  left          : calc(var(--spacing) - var(--radius) - 1px);
  width         : calc(2 * var(--radius));
  height        : calc(2 * var(--radius));
  border-radius : 50%;
  background    : var(--dark-grey);
}

.tree-buttons summary::before{
  content     : '+';
  z-index     : 1;
  background  : var(--accent);
  color       : #fff;
  font-weight : 400;
  line-height : calc(2 * var(--radius) - 2px);
  text-align  : center;
}

.tree-buttons details[open] > summary::before{
  content : '−';
}
</style>