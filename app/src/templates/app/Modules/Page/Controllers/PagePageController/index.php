<div class="p-5 mb-4 bg-body-tertiary rounded-3">
      <div class="container-fluid py-5">
        <h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
        <p class="col-md-8 fs-4"><?=mb_substr($data["description"], 0, 150); ?>...</p>
        <a href="https://gitlab.com/dolinais/dolinais-cms" target="_blank" class="btn btn-primary btn-lg" type="button">Скачать dolinais cms</a>
      </div>
</div>
<content>
    <hr>
    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
            </ol>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <? if($img = app\Models\FilesModels::get()->findOne(
                array(
                    'file_page_id' => $data['id'],
                    'file_status' => 1
                )
            )): ?>
            <div class="col">
                <img src="/uploads/<?=$img[0]['file_name']; ?>" class="img-fluid" alt="<?=$data["title"]; ?>">
            </div>
            <? endif; ?>
        <? if(\app\Core\MobileCore::IsMobile()): ?>
        </div>
        <div class="row" style="margin: 10px">
        <? endif; ?>
            <div class="col">
                <?=$data["description"]; ?>
                <hr>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid justify-content-between">
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item dropdown me-3 me-lg-1">
                                <a class="nav-link dropdown-toggle hidden-arrow" href="#showComments">
                                    <i class="fas fa-commenting fa-lg"></i>
                                    <span class="badge rounded-pill badge-notification bg-danger"><?=count($data["ReviewsModels"]); ?></span>
                                </a>
                            </li>
                            <li class="nav-item dropdown me-3 me-lg-1">
                                <a class="nav-link dropdown-toggle hidden-arrow" href="#">
                                    <i class="fas fa-heart fa-lg"></i>
                                    <span class="badge rounded-pill badge-notification bg-danger"><?=$data["articles_like"]; ?></span>
                                </a>
                            </li>
                            <li class="nav-item dropdown me-3 me-lg-1">
                                <a class="nav-link dropdown-toggle hidden-arrow" href="#">
                                    <i class="fas fa-share-alt fa-lg"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <hr>
                <? if(\DolinaIS::getName()->isGuest): ?>
                <a href="/page/update?id=<?=$data["id"]; ?>">Редактировать</a>
                <? endif; ?>
            </div>
        </div>
    </div>
    <div class="container">
        <hr>
        <!-- <nav aria-label="Пример навигации по страницам">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">Предыдущая</a></li>
                <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">Следующая</a></li>
            </ul> -->
        </nav>
    </div>
    <section style="background-color: #fff;">
        <div class="container my-5 py-5 text-body">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12 col-lg-10 col-xl-8" id="showComments">
                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <h4 class="text-body mb-0">Комментарии</h4>
                    </div>
                    <?php foreach($data["ReviewsModels"] as $ReviewsModels): ?>
                    <?php
                        if($ReviewsModels->reply_id){
                            $ReviewsPX = 30;
                        }else{
                            $ReviewsPX = 0;
                        }
                        ?>
                    <div class="card mb-3" style="margin-left: <?=$ReviewsPX ;?>px;">
                        <div class="card-body">
                            <div class="d-flex flex-start">
                                <img class="rounded-circle shadow-1-strong me-3"
                                    src="/uploads/avatarg.jpg" alt="avatar" width="60"
                                    height="60" />
                                <div>
                                    <h6 class="fw-bold mb-1">Гость</h6>
                                    <div class="d-flex align-items-center mb-3">
                                        <p class="mb-0" style="font-size: 12px;">
                                            <?= app\Components\DateTimeComponents::T($ReviewsModels->created_at, 5); ?>
                                        </p>
                                        <a href="#!<?=$ReviewsModels->id; ?>" class="link-muted"><i class="fas fa-pencil-alt ms-2"></i></a>
                                        <a href="#!<?=$ReviewsModels->id; ?>" class="link-muted"><i class="fas fa-redo-alt ms-2"></i></a>
                                        <a href="#!<?=$ReviewsModels->id; ?>" class="link-danger"><i class="fas fa-heart ms-2"></i> (<?=$ReviewsModels->reviews_like; ?>)</a>
                                    </div>
                                    <p class="mb-0">
                                        <?=$ReviewsModels->message; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
        <div class="container my-5 py-5 text-body">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12 col-lg-10 col-xl-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <form method="POST" id="commentForm">
                                <div class="row d-flex justify-content-center">
                                    <div class="">
                                        <div class="d-flex flex-start w-100">
                                          <img class="rounded-circle shadow-1-strong me-3"
                                            src="/uploads/avatarg.jpg" alt="avatar" width="40"
                                            height="40" />
                                            <input type="text" name="page_id" id="page_id" value="<?=\DolinaIS::getName()->Request[0]; ?>" style="display: none;">
                                            <div class="form-outline ">
                                                <textarea class="form-control" name="comment" id="comment" rows="5"
                                              style="background-color: #f0f0f0"></textarea>
                                                <label class="form-label" for="textAreaExample">Оставить комментарий</label>
                                            </div>
                                        </div>
                                        <div class="float-end mt-2 pt-1"><br>
                                            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Комментировать" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</content>
<script>
$(document).ready(function(){
    $('#commentForm').on('submit', function(event){
        event.preventDefault();
        var formData = $(this).serialize();
        $.ajax({
            url: "/api/comment",
            method: "POST",
            data: formData,
            dataType: "JSON",
            success:function(response) {
                if(!response.error) {
                    $('#commentForm')[0].reset();
                    $('#commentId').val('0');
                    $('#message').html(response.message);
                    showComments();
                } else if(response.error){
                    $('#message').html(response.message);
                }
            }
        })
    });
});

function showComments() {
    $.ajax({
        url:"/<?=\DolinaIS::getName()->Request[0]; ?>",
        method:"POST",
        success:function(response) {
            $('#showComments').html(response);
        }
    })
}
</script>