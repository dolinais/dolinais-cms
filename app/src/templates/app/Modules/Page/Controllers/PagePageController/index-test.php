<div class="p-5 mb-4 bg-body-tertiary rounded-3">
      <div class="container-fluid py-5">
        <h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
        <p class="col-md-8 fs-4"><?=mb_substr($data["description"], 0, 200); ?>...</p>
        <button class="btn btn-primary btn-lg" type="button">Анонс</button>
      </div>
</div>
<content>
    <hr>
    <div class="container">
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
            </ol>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <?php foreach(app\Models\FilesModels::get()->findOne(
                  array(
                      'file_page_id' => $data['id'],
                      'file_status' => 1
                  )
              ) as $files):
            ?>
            <div class="col">
                <img src="/uploads/<?=$files['file_name']; ?>" class="img-fluid" alt="...">
            </div>
            <?php endforeach; ?>
            <div class="col">
                <?=$data["description"]; ?>
                <hr>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid justify-content-between">
                        <ul class="navbar-nav flex-row">
                            <li class="nav-item dropdown me-3 me-lg-1">
                                <a class="nav-link dropdown-toggle hidden-arrow" href="#">
                                    <i class="fas fa-commenting fa-lg"></i>
                                    <span class="badge rounded-pill badge-notification bg-danger">6</span>
                                </a>
                            </li>
                            <li class="nav-item dropdown me-3 me-lg-1">
                                <a class="nav-link dropdown-toggle hidden-arrow" href="#">
                                    <i class="fas fa-heart fa-lg"></i>
                                    <span class="badge rounded-pill badge-notification bg-danger"><?=$data["articles_like"]; ?></span>
                                </a>
                            </li>
                            <li class="nav-item dropdown me-3 me-lg-1">
                                <a class="nav-link dropdown-toggle hidden-arrow" href="#">
                                    <i class="fas fa-share-alt fa-lg"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <hr>
                <a href="/page/update?id=<?=$data["id"]; ?>">Редактировать</a>
            </div>
        </div>
    </div>
    <div class="container">
        <hr>
        <nav aria-label="Пример навигации по страницам">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">Предыдущая</a></li>
                <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">Следующая</a></li>
            </ul>
        </nav>
    </div>
    <hr>
    <section style="background-color: #FFF;">
      <div class="container my-5 py-5">
        <div class="row d-flex justify-content-center">
          <div class="col-md-12 col-lg-10">
            <div class="card text-dark">
                <?php foreach($data["ReviewsModels"] as $ReviewsModels): ?>
                    <div class="card-body p-4">
                        <!-- <h4 class="mb-0">Комментарии</h4>
                        <p class="fw-light mb-4 pb-2"><?=$data["title"]; ?></p> -->
                        <div class="d-flex flex-start">
                          <img class="rounded-circle shadow-1-strong me-3"
                            src="https://klike.net/uploads/posts/2023-01/1673594987_3-24.jpg" alt="avatar" width="60"
                            height="60" />
                          <div>
                            <h6 class="fw-bold mb-1">Ольга Орлова</h6>
                            <div class="d-flex align-items-center mb-3">
                              <p class="mb-0">
                                час назад
                              </p>
                              <a href="#!" class="link-muted"><i class="fas fa-pencil-alt ms-2"></i></a>
                              <a href="#!" class="link-muted"><i class="fas fa-redo-alt ms-2"></i></a>
                              <a href="#!" class="link-danger"><i class="fas fa-heart ms-2"></i> (<?=$ReviewsModels->reviews_like; ?>)</a>
                            </div>
                            <p class="mb-0">
                              <?=$ReviewsModels->message; ?>
                            </p>
                          </div>
                        </div>
                    </div>
                    <hr class="my-0" />
                <? endforeach; ?>


              <div class="card-body p-4">
                <div class="d-flex flex-start">
                  <img class="rounded-circle shadow-1-strong me-3"
                    src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(19).webp" alt="avatar" width="60"
                    height="60" />
                  <div>
                    <h6 class="fw-bold mb-1">Мария Швец</h6>
                    <div class="d-flex align-items-center mb-3">
                      <p class="mb-0">
                        5 часов назад
                        <span class="badge bg-success">Высший рейтинг</span>
                      </p>
                      <a href="#!" class="link-muted"><i class="fas fa-pencil-alt ms-2"></i></a>
                      <a href="#!" class="text-success"><i class="fas fa-redo-alt ms-2"></i></a>
                      <a href="#!" class="link-danger"><i class="fas fa-heart ms-2"></i> (200)</a>
                    </div>
                    <p class="mb-0">
                      Самая лучшая на мой взгляд это Dolina IS CMS. Рекомендую!
                    </p>
                  </div>
                </div>
              </div>

              <hr class="my-0" style="height: 1px;" />

              <div class="card-body p-4">
                <div class="d-flex flex-start">
                  <img class="rounded-circle shadow-1-strong me-3"
                    src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(19).webp" alt="avatar" width="60"
                    height="60" />
                  <div>
                    <h6 class="fw-bold mb-1">Александра Соловьёва</h6>
                    <div class="d-flex align-items-center mb-3">
                      <p class="mb-0">
                        3 дня назад
                        <span class="badge bg-danger">Маленький рейтинг</span>
                      </p>
                      <a href="#!" class="link-muted"><i class="fas fa-pencil-alt ms-2"></i></a>
                      <a href="#!" class="link-muted"><i class="fas fa-redo-alt ms-2"></i></a>
                      <a href="#!" class="link-muted"><i class="fas fa-heart ms-2"></i></a>
                    </div>
                    <p class="mb-0">
                      Мое знакомство с сайтостроением началось с дипломной работы - для наглядности я решила проиллюстрировать ее сайтом. Мне посоветовали воспользоваться бесплатной системой управления содержимым (CMS) Joomla. И я, абсолютный гуманитарий не только сделала дипломный проект, но и множество других сайтов - для себя и на заказ.
                    </p>
                  </div>
                </div>
              </div>

              <hr class="my-0" />

              <div class="card-body p-4">
                <div class="d-flex flex-start">
                  <img class="rounded-circle shadow-1-strong me-3"
                    src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(19).webp" alt="avatar" width="60"
                    height="60" />
                  <div>
                    <h6 class="fw-bold mb-1">Ольга Смирнова</h6>
                    <div class="d-flex align-items-center mb-3">
                      <p class="mb-0">
                        5 дней назад
                        <span class="badge bg-primary">Средний рейтинг</span>
                      </p>
                      <a href="#!" class="link-muted"><i class="fas fa-pencil-alt ms-2"></i></a>
                      <a href="#!" class="link-muted"><i class="fas fa-redo-alt ms-2"></i></a>
                      <a href="#!" class="link-muted"><i class="fas fa-heart ms-2"></i></a>
                    </div>
                    <p class="mb-0">
                      Наверно все, кто ранее занимались сайтами или чем то подобным, сейчас боготворят людей создавших CMS, в частности WordPress (на самом деле их много, но данная самая удобная и дружелюбная). Захотел блог, обзорный сайт, галерею или сайт визитку, без проблем ВордПресс всегда поможет.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <section style="background-color: #FFF;">
                <div class="container my-5 py-5">
                    <div class="card-footer py-3 border-0 w-100" style="background-color: #f8f9fa;">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-12 col-lg-10">
                                <div class="d-flex flex-start w-100">
                                  <img class="rounded-circle shadow-1-strong me-3"
                                    src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(19).webp" alt="avatar" width="40"
                                    height="40" />
                                  <div class="form-outline w-100">
                                    <textarea class="form-control" id="textAreaExample" rows="4"
                                      style="background: #fff;"></textarea>
                                    <label class="form-label" for="textAreaExample">Комментарий</label>
                                  </div>
                                </div>
                                <div class="float-end mt-2 pt-1"><br>
                                    <button type="button" class="btn btn-primary btn-sm">Отправить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
          </div>
        </div>
      </div>
    </section>
    <hr>
</content>