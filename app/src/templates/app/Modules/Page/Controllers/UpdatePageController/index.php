<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */
$Checked = '';
if($data["value"]["articles_status"] == 1){
	$Checked = 'checked';
}
?>
<div class="p-2 mb-4 bg-body-tertiary rounded-3">
	<div class="container-fluid py-5">
		<h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
	</div>
</div>
<content>
	<div class="container">
		<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
			</ol>
		</nav>
	</div>
	<div class="container">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
  			<li class="nav-item" role="presentation">
    			<button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Основное</button>
  			</li>
  			<li class="nav-item" role="presentation">
    			<button class="nav-link" id="description-tab" data-bs-toggle="tab" data-bs-target="#description" type="button" role="tab" aria-controls="description" aria-selected="true">Описание</button>
  			</li>
  			<li class="nav-item" role="presentation">
    			<button class="nav-link" id="image-tab" data-bs-toggle="tab" data-bs-target="#image" type="button" role="tab" aria-controls="image" aria-selected="false">Изображение</button>
  			</li>
  			<li class="nav-item" role="presentation">
    			<button class="nav-link" id="additionally-tab" data-bs-toggle="tab" data-bs-target="#additionally" type="button" role="tab" aria-controls="additionally" aria-selected="false">Дополнительно</button>
  			</li>
		</ul>
		<form enctype="multipart/form-data" method="POST">
			<div class="tab-content" id="myTabContent">
				<hr>
	  			<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
					<!-- As a link -->
					<div class="container">
						<div class="row row-cols-2">
							<fieldset>
								<?=$data["error"]; ?>
			    				<!-- <legend><?=$data["value"]["title"]; ?></legend> -->
								<div class="mb-3">
									<input type="text" name="TitlePost" class="form-control" placeholder="Заголовок страницы" value="<?=$data["value"]["title"]; ?>">
									<div class="form-text" id="basic-addon4">Заголовок страницы</div>
								</div>
							    <div class="mb-3">
						      		<select name="CategoryPost" class="form-select">
						      			<?php foreach($data["СategoriesPageModels"] as $СategoriesPageModels): ?>
						      				<?php if($СategoriesPageModels['id'] == $data["value"]["categories_id"]): ?>
						        			<option selected="selected" value="<?=$СategoriesPageModels['id']; ?>"><?=$СategoriesPageModels['name']; ?></option>
						        			<?php else: ?>
						        			<option value="<?=$СategoriesPageModels['id']; ?>"><?=$СategoriesPageModels['name']; ?></option>
						        			<?php endif; ?>
						    			<?php endforeach; ?>
						      		</select>
						      		<div class="form-text" id="basic-addon4">Выбрать категорию</div>
							    </div>
							    <div class="mb-3">
								    <div class="form-floating">
										<select name="TypePageModels"  class="form-select" id="floatingSelect" aria-label="Floating label select example">
											<?php foreach($data["TypePageModels"] as $TypePageModels): ?>
							      				<?php if($TypePageModels['id'] == 6): ?>
							        			<option selected="selected" value="<?=$TypePageModels['id']; ?>"><?=$TypePageModels['type_title']; ?></option>
							        			<?php else: ?>
							        			<option value="<?=$TypePageModels['id']; ?>"><?=$TypePageModels['type_title']; ?></option>
							        			<?php endif; ?>
							    			<?php endforeach; ?>
										</select>
										<label for="floatingSelect">Тип страницы</label>
									</div>
								</div>
			  				</fieldset>
						</div>
					</div>
	  			</div>
	  			<div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description-tab">
				  	<!-- As a link -->
				  	<div class="container">
						<div class="row row-cols-2">
							<fieldset>
								<div class="form-floating mb-3">
								 	<textarea name="TextPost" id="textarea-tinymce" class="form-control" style="height: 100px"><?=$data["value"]["description"]; ?></textarea>
								 	<div class="form-text" id="basic-addon4">Описание</div>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			  <div class="tab-pane fade" id="image" role="tabpanel" aria-labelledby="image-tab">
			  	<!-- As a link -->
					<div class="container">
						<div class="row row-cols-2">
							<fieldset>
								<div class="mb-3">
									<label for="formFileLg" class="form-label">Главное изображение</label>
									<input name="filePost[]" id="main_slider" type="file" accept="image/*" multiple="multiple">
								</div>
								<script>
									const urlParams = new URLSearchParams(window.location.search);
								    function getValue(){
									   	var value= $.ajax({
											url: '/page/views?id='+ urlParams.get('id'),
											async: false
									   	}).responseJSON;
									   	return value;
									}

								    $(document).ready(function(){
									    $("#main_slider").fileinput({
									    	uploadUrl: '/page/files?id='+ urlParams.get('id'),
									    	uploadAsync: true,
									        showUpload: true,
									        showRemove: true,
									        enableResumableUpload: true,
									        maxFileSize:5000,
									        initialPreviewShowDelete: true,
									        allowedFileExtensions: ["jpg", "png", "gif", "webp"],
									        initialPreviewAsData: true,
									        overwriteInitial: false,
									        initialPreview: getValue().initialPreview,
									        uploadExtraData: {
									            'uploadToken': 'SOME-TOKEN',
									        },
									        initialPreviewConfig: getValue().initialPreviewConfig,
									    });
									});
								</script>
							</fieldset>
						</div>
					</div>
			  </div>
			  <div class="tab-pane fade" id="additionally" role="tabpanel" aria-labelledby="additionally-tab">
			  	<!-- As a link -->
			  	<div class="container">
					<div class="row row-cols-2">
						<fieldset>
							<div class="mb-3">
		      					<div class="form-check">
							        <input <?=$Checked; ?> name="StatusPost" class="form-check-input" type="checkbox" id="disabledFieldsetCheck">
							        <label class="form-check-label" for="disabledFieldsetCheck">
							          Активно
							        </label>
		      					</div>
		    				</div>
		    				<div class="mb-3">
								<input name="UrlPage" type="text" id="disabledTextInput" class="form-control" placeholder="URL страницы" value="<?=$data["value"]["slug"]; ?>">
								<div class="form-text" id="basic-addon4">URL страницы</div>
							</div>
							<div class="mb-3">
								<input name="template-name" type="text" id="disabledTextInput" class="form-control" placeholder="@app/views/template-name || template-name" value="">
								<div class="form-text" id="basic-addon4">@app/views/template-name || template-name</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<button name="SubmitPost" type="submit" class="btn btn-primary">Обновить</button>
		</form>
	</div>
<hr class="border border-primary border-3 opacity-75">
</content>