<link href="/resources/style/messenger.css" rel="stylesheet" />
<content>
    <hr>
    <div class="container">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card chat-app">
                    <div id="plist" class="people-list">
                        <div class="input-group">
                            <!-- <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-search"></i></span>
                            </div> -->
                            <input type="text" class="form-control" placeholder="Найти Пётр...">
                        </div>
                        <ul class="list-unstyled chat-list mt-2 mb-0">
                            <a href="">
                            <li class="clearfix">
                                <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="avatar">
                                <div class="about">
                                    <div class="name">Пётр Первый</div>
                                    <div class="status"> <i class="fa fa-circle offline"></i> был 7 минут назад </div>
                                </div>
                            </li>
                        </a>
                            <li class="clearfix">
                                <img src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="avatar">
                                <div class="about">
                                    <div class="name">Максим фролов</div>
                                    <div class="status"> <i class="fa fa-circle online"></i> online </div>
                                </div>
                            </li>
                            <li class="clearfix">
                                <img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="avatar">
                                <div class="about">
                                    <div class="name">Екатерина Морозова</div>
                                    <div class="status"> <i class="fa fa-circle offline"></i> offline </div>
                                </div>
                            </li>
                            <?php if(isset($_GET["sel"]) && $_GET["sel"] == 1): ?>
                                <?php $userName1 = 'active'; ?>
                            <?php endif; ?>
                            <li class="clearfix <?=$userName1 ?? null; ?>">
                                <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar">
                                <div class="about">
                                    <div class="name">Игорь Меденцев</div>
                                    <div class="status"> <i class="fa fa-circle online"></i> online </div>
                                </div>
                            </li>
    <!--                         <li class="clearfix">
                                <img src="https://bootdey.com/img/Content/avatar/avatar8.png" alt="avatar">
                                <div class="about">
                                    <div class="name">Дарья Фролова</div>
                                    <div class="status"> <i class="fa fa-circle online"></i> online </div>
                                </div>
                            </li> -->
                            <?php if(isset($_GET["sel"]) && $_GET["sel"] == 2): ?>
                                <?php $userName2 = 'active'; ?>
                            <?php endif; ?>
                            <li class="clearfix <?=$userName2 ?? null; ?>" onclick=" window.open('/messenger?sel=2')">
                                <img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="avatar">
                                <div class="about">
                                    <div class="name">Дарья Бухгалтер</div>
                                    <div class="im-activity" id="im-activity-d">
                                        <div id="fast"></div>
                                        <div id="regular"></div>
                                        <div id="slow"></div>
                                        <div id="typeT"></div>
                                    </div>
                                    <div class="status" id="status-d"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="chat">
                        <div class="chat-header clearfix">
                            <div class="row">
                                <div class="col-lg-6">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#view_info">
                                        <img src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="avatar">
                                    </a>
                                    <div class="chat-about">
                                        <h6 class="m-b-0" id="userName"><?=$data["userName"]; ?></h6>
                                        <div class="im-activity" id="im-activity">
                                            <div id="fast"></div>
                                            <div id="regular"></div>
                                            <div id="slow"></div>
                                            <div id="typeT"><span id="typeTuserName"></span></div>
                                        </div>
                                        <small><div class="status" id="div_status"><i class="fa fa-circle online"></i>online</div></small>
                                    </div>
                                </div>
                                <div class="col-lg-6 hidden-sm text-right">
                                    <a href="javascript:void(0);" class="btn btn-outline-secondary"><i class="fa fa-camera"></i></a>
                                    <a href="javascript:void(0);" class="btn btn-outline-primary"><i class="fa fa-image"></i></a>
                                    <a href="javascript:void(0);" class="btn btn-outline-info"><i class="fa fa-cogs"></i></a>
                                    <a href="javascript:void(0);" class="btn btn-outline-warning"><i class="fa fa-question"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="chat-history">
                            <ul class="m-b-0" id="messages">
                                <div class="chat-date">10 ноября</div>
                                <?php foreach(app\Models\MessengerModels::get()->findAll() as $MessengerModels ): ?>
                                <li class="clearfix">
                                    <div class="message-data">
                                        <span class="message-data-time"><span><a href=""><?=app\Modules\Account\Models\AccountModels::isUserPerson($MessengerModels['user_id'])['first_name']; ?></a> </span><?=date('d.m.Y H:i', $MessengerModels['created_at']); ?></span>
                                    </div>
                                    <div class="message my-message">
                                        <p><?=$MessengerModels['message']; ?></p>
                                        <?php if($MessengerModels['avatar']): ?>
                                            <img src="<?=$MessengerModels['avatar']; ?>" alt="avatar" width="300" height="auto" class="chat-img">
                                        <?php endif; ?>
                                    </div>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="chat-message clearfix">
                            <span id="length"></span>
                            <div class="form-floating mb-3">
                                <input type="text" id="messageBox" class="form-control" placeholder="Напишите что нибудь...">
                                <div class="input-group-prepend" id="send">
                                    <span class="input-group-text"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            <hr>
                            <img src="https://laborcolor.ru/wp-content/uploads/f/a/d/fadda6d67d084b4c92424b65d084d144.jpeg" alt="avatar" width="75" height="auto">
                            <img src="https://laborcolor.ru/wp-content/uploads/f/a/d/fadda6d67d084b4c92424b65d084d144.jpeg" alt="avatar" width="75" height="auto">
                            <img src="https://excel-power.ru/wp-content/uploads/2015/08/excel-1200x1200.png" width="75" height="auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</content>
<hr>
</body>
<script type="text/javascript" src="/resources/js/messenger.js" ></script>
<script type="text/javascript" src="/resources/js/main.js" ></script>