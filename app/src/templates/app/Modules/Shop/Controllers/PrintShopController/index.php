<?php
use app\Components\BarcodeComponents;

?>
<?php header('Content-Type: text/html; charset=utf-8'); ?>
<!DOCTYPE html>
<html>
  <head>
    <title><?=$data["title"]; ?></title>
    <style>
       body {
         padding: 0;
       }
       /*
         Наклейка
       */
       .b-sticker {
         width: 45%;
         padding: 3mm;
         float: left;
         font-family: Arial;
         border: 1px dashed black;
         page-break-inside: avoid;
       }
       .b-sticker table {
         width: 99%;
         border-collapse: collapse;
       }
       .b-sticker tr td {
         padding: 2mm;
       }
       .b-sticker tr:first-child td {
         border-bottom: 1mm solid #000;
       }
       .b-sticker tr td p {
         margin: 0;
         padding-bottom: 1mm;
       }

       /* Штрихкод код PHP*/
       .barcode {
         width: 95%;
       }

       /* Размеры шрифтов */
       .b-sticker .seller {
         font-size: 4mm;
       }
       .b-sticker .number {
         font-size: 6mm;
       }
       .b-sticker .customer-info {
         font-size: 3.8mm;
       }
       .b-sticker .customer-info .date,
       .b-sticker .customer-info .price,
       .b-sticker .customer-info .name,
       .b-sticker .customer-info .phone {
       }
       .b-sticker .punkt-info {
         font-size: 3.5mm;
       }
     </style>
  </head>

  <body>

<?php
foreach($data['post'] as $data){
    $pkgs[] = array('shop' => 'Сантехника 57"', 'title' => $data->title, 'sku' => 1234567891234, 'price' => 1000, 'buyer_fio' => '', 'buyer_phone' => '');
}
?>


<?php foreach ($pkgs as $item): ?>
      <div class="b-sticker">
        <table>
          <colgroup>
            <col width="1%">
            <col width="10%">
            <col width="60%">
          </colgroup>
          <tr>
            <td class="customer-info" colspan="2">
             <!--  <div class="seller"><?php echo $item['shop'] ?></div>
              <div class="number" style="font-size: 12px;"><?php echo $item['title'] ?></div>
              <div class="date"></div>
              <div class="price"><?php echo $item['price'] ?></div>
              <div class="name"><?php echo $item['buyer_fio'] ?></div>
              <div class="phone"><?php echo $item['buyer_phone'] ?></div> -->
            </td>
            <td>
              <div class="barcode" ><?php echo BarcodeComponents::code39($item['sku']); ?></div>
            </td>
          </tr>
          <tr>
            <td><img src="logo.png" alt=""></td>
            <td class="punkt-info" colspan="2">
              <p>
                <?php echo $item['title'] ?>
              </p>
              <p>
                <!-- ул. Михалицына, 5, Орёл. (24 павильон)<shy/>+7 (910) 263 9971 -->
          </p>
          </td>
          </tr>
        </table>
      </div>

<?php endforeach; ?>

  </body>
</html>