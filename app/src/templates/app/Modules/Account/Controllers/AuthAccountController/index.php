<div class="p-2 mb-4 bg-body-tertiary rounded-3">
    <div class="container-fluid py-5">
        <h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
    </div>
</div>
<div class="container">
    <div class="row align-items-center">
        <div class="col-sm-5">
            <form action="/account/auth" method="post">
                <?=$data["error"]; ?>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Номер телефона</label>
                    <input type="text" value="+7" name="username" class="form-control tel" id="exampleInputEmail1" aria-describedby="emailHelp" autocomplete="off">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Пароль</label>
                    <input type="password" name="password"  class="form-control" id="exampleInputPassword1" autocomplete="off">
                </div>
                <div class="mb-3 form-check">
                    <input type="checkbox" name="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Запомнить меня на этом компьютере</label>
                </div>
                <button type="submit" class="btn btn-primary">Авторизоваться</button>
                <button type="button" class="btn btn-link btn-floating mx-1">
                  <i class="fab fa-telegram" onclick="location.href='tg://resolve?domain=dev_dolinaisbot&start=123';"></i>
                </button>

                <button type="button" class="btn btn-link btn-floating mx-1">
                  <i class="fab fa-vk"></i>
                </button>

                <button type="button" class="btn btn-link btn-floating mx-1">
                  <i class="fab fa-odnoklassniki"></i>
                </button>
            </form>
        </div>
    </div>
</div>
<script>
window.addEventListener("DOMContentLoaded", function() {
  [].forEach.call( document.querySelectorAll('.tel'), function(input) {
    var keyCode;
    function mask(event) {
      event.keyCode && (keyCode = event.keyCode);
      var pos = this.selectionStart;
      if (pos < 3) event.preventDefault();
      var matrix = "+7 (___) ___ ____",
          i = 0,
          def = matrix.replace(/\D/g, ""),
          val = this.value.replace(/\D/g, ""),
          new_value = matrix.replace(/[_\d]/g, function(a) {
              return i < val.length ? val.charAt(i++) : a
          });
      i = new_value.indexOf("_");
      if (i != -1) {
          i < 5 && (i = 3);
          new_value = new_value.slice(0, i)
      }
      var reg = matrix.substr(0, this.value.length).replace(/_+/g,
          function(a) {
              return "\\d{1," + a.length + "}"
          }).replace(/[+()]/g, "\\$&");
      reg = new RegExp("^" + reg + "$");
      if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) {
        this.value = new_value;
      }
      if (event.type == "blur" && this.value.length < 5) {
        this.value = "";
      }
    }

    input.addEventListener("input", mask, false);
    input.addEventListener("focus", mask, false);
    input.addEventListener("blur", mask, false);
    input.addEventListener("keydown", mask, false);

  });
});
</script>