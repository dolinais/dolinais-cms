<div class="p-1 mb-4 bg-body-tertiary rounded-3">
	<div class="container-fluid py-5">
		<h1 class="display-5 fw-bold"><?=$data["title"]; ?></h1>
	</div>
</div>
<content>
	<div class="container">
		<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?=$data["title"]; ?></li>
			</ol>
		</nav>
	</div>
	<div class="container">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
  			<li class="nav-item" role="presentation">
    			<button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Профиль</button>
  			</li>
  			<li class="nav-item" role="presentation">
    			<button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Настройки</button>
  			</li>
  			<li class="nav-item" role="presentation">
    			<button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">API</button>
  			</li>
		</ul>
		<div class="tab-content" id="myTabContent">
			<hr>
  			<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
				<!-- As a link -->
				<div class="container">
			  		<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Личные данные</a>
				    	</div>
				  	</nav>
				  	<br>
				  	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Изменить пароль</a>
				    	</div>
				  	</nav>
				  	<br>
				  	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Публичность</a>
				    	</div>
				  	</nav>
				</div>
  			</div>
		  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
		  	<!-- As a link -->
				<div class="container">
			  		<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Настроить вывод статей</a>
				    	</div>
				  	</nav>
				  	<br>
				  	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Настроить алиасы</a>
				    	</div>
				  	</nav>
				  	<br>
				  	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Настроить API</a>
				    	</div>
				  	</nav>
				</div>
		  </div>
		  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
		  	<!-- As a link -->
				<div class="container">
			  		<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Ключи доступа</a>
				    	</div>
				  	</nav>
				  	<br>
				  	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Доступ к контенту</a>
				    	</div>
				  	</nav>
				  	<br>
				  	<nav class="navbar navbar-expand-lg navbar-light bg-light">
				    	<div class="container-fluid">
				      		<a class="navbar-brand" href="#">Публичность</a>
				    	</div>
				  	</nav>
				</div>
		  </div>
		</div>
	</div>
	<hr>
</content>
