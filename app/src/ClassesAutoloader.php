<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

class ClassesAutoloader
{
    public $dirArray = [];

    public function __construct($app)
    {
        $this->getDirs($app);
    }

    public function getClass($className)
    {
        foreach ($this->dirArray as $path) {
            $filename = "{$path}/".basename(str_replace("\\", "/", $className)).".php";
            if (file_exists($filename)) {
                require_once $filename;
                break;
            }
        }
    }

    public function getDirs($dir)
    {
        $arr = glob("{$dir}/*", GLOB_ONLYDIR);
        if (count($arr)) {
            foreach ($arr as $dirname) {
                if (preg_match_all('/\b[A-Z][a-z]+/u', $dirname)) {
                    $this->dirArray[] = $dirname;
                    $this->getDirs($dirname);
                }
            }
        }
    }
}

spl_autoload_register([new ClassesAutoloader(APP), 'getClass']);