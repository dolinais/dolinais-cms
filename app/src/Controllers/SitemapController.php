<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Controllers;

use app\Core\ControllerCore;

/*SitemapController*/
class SitemapController extends ControllerCore
{

	public function actionIndex()
    {
        $out = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $out .= '
        <url>
            <loc>https://'.$_SERVER['HTTP_HOST'].'/articles/1.html</loc>
            <lastmod>' . date('Y-m-d') . '</lastmod>
            <priority>0.5</priority>
        </url>';
        $out .= '</urlset>';

        header('Content-Type: text/xml; charset=utf-8');

        echo $out;

        exit();
    }
}