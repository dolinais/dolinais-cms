<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Controllers;

use app\Core\ControllerCore;

class MainController extends ControllerCore
{
	public function actionIndex()
	{
		\DolinaIS::getName()->widget;

		$this->title = \DolinaIS::$copyright;
        $this->description = 'Рады представить вам новую версию нашей системы управления сайтом Dolina IS CMS';
        $this->keywords = 'Dolina IS CMS';

		$data = array(
			$this
		);

		$this->rendercontroller(get_class($this), 'index', $data, 'assets');
  	}

  	public function actionInstall()
  	{
		exit('Installing');
    }
}