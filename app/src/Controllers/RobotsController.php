<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Controllers;

use app\Core\ControllerCore;

/*RobotsController*/
class RobotsController extends ControllerCore
{

	public function actionIndex()
    {
        $out  = "User-agent: *" . PHP_EOL;
        $out .= "Disallow: /" . PHP_EOL;
        $out .= "User-agent: Yandex" . PHP_EOL;
        $out .= "Host: https://".$_SERVER['HTTP_HOST'].PHP_EOL;
        $out .= "Sitemap: https://".$_SERVER['HTTP_HOST']."/sitemap.xml";

        header('Content-Type: text/plain; charset=utf-8');
        echo $out;

        exit();
    }
}