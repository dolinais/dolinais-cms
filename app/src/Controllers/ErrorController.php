<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Controllers;

use app\Core\ControllerCore;

/*PageController 404*/
class ErrorController extends ControllerCore
{

	public function __construct($error=404, $code=404)
	{
		http_response_code($code);
		$this->title = $error;
        $this->description = $error;
        $this->keywords = $error;

		$data = array(
			'title' => $error,
			$this
		);

		$this->rendercontroller(get_class($this), 'index', $data, 'assets');
  	}
}