<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Components;

class DateTimeComponents
{

    public static function T($unix_time, $minutes){
        $minutesout = date("d.m.Y H:i", strtotime('-'.$minutes.' minutes',$unix_time));

        $diff = round(time() - strtotime($minutesout) / 60, 1);  // 10,5 часов

        // return $minutesout;
        switch(true)
        {
            case $diff < 1:
                return $diff." мин. назад";
                break;
            case $diff < 5:
                return $diff." мин. назад";
                break;
            case $diff < 10:
                return $diff." мин. назад";
                break;
            default:
                return date('d.m.Y H:i', $unix_time);
                break;
        }
    }
}
