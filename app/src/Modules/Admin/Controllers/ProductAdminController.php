<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Admin\Controllers;

use app\Modules\Admin\Models\ProductAdminModels;

class ProductAdminController
{

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");

        if(true){
            http_response_code(200);

            // ProductAdminModels::get()->getInsert(array(
            //     'name' => htmlspecialchars(strip_tags('test')),
            //     'price' => 123,
            //     'description' => 'description test',
            //     'category_id' => 1,
            //     'created' => time()
            // ));


            // $app = ProductAdminModels::get();
            // $app->bootstrap();

            // ProductAdminModels::get()->getUpdate(array(
            //     'name' => htmlspecialchars(strip_tags('test1')),
            //     'price' => 13,
            //     'description' => time(),
            //     'category_id' => 1,
            //     'created' => time()
            // ),6);

            // echo json_encode(ProductAdminModels::get()->getDelete(
            //     array(
            //         'id' => 1
            //     )
            // ));

            // echo json_encode(ProductAdminModels::get()->findAll());

            echo json_encode(ProductAdminModels::get()->findOne(
                    array(
                        'price' => 123,
                        'category_id' => 2
                    )
                )
            );

            // echo json_encode(ProductAdminModels::get('products.id, products.name AS title, products.description, products.price, categories.name')
            //     ->from('LEFT JOIN categories')
            //     ->on(['products.category_id','=','categories.id'])
            //     ->where(['products.price', '=', '"13"'],'AND',['products.id','=','7'])
            //     ->limit(20)
            //     ->fetch()
            // );

        }else {
            http_response_code(404);

            echo json_encode(array("message" => "Товары не найдены."), JSON_UNESCAPED_UNICODE);
        }
    }
}