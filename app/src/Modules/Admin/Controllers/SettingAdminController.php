<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Admin\Controllers;

use app\Core\ControllerCore;
use app\Modules\Page\Models\PageModels;
use app\Controllers\ErrorController;

class SettingAdminController extends ControllerCore
{

    public function actionIndex()
    {

        $this->title = 'Панель администратора';
        $this->description = '';
        $this->keywords = '';

        if(!\DolinaIS::getName()->isGuest){
            RedirectCore::to('/account/auth');
        }else{
            $cabinet = 'Здесь будут храиться Панель администратора';
        }

        $data = array(
            'title' => 'Панель администратора',
            'description' => $cabinet,
            $this
        );

        $this->rendercontroller(get_class($this), 'index', $data, 'assets');
    }
}