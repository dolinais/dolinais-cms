<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Admin\Models;

use app\Core\SelectPDOCore;

class ProductAdminModels extends SelectPDOCore
{

    public static function tableName()
    {
        return 'products';
    }
}