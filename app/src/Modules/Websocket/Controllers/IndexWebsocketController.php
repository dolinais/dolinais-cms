<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Websocket\Controllers;

class IndexWebsocketController
{
    public function actionIndex()
    {
        echo 200;
        exit();
    }
}
