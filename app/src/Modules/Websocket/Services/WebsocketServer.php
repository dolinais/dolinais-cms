<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Websocket\Services;

use app\Modules\Websocket\Services\WebsocketMaster;
use app\Modules\Websocket\Services\WebsocketHandler;

class WebsocketServer
{
    private $config;
    public function __construct($config) {
        $this->config = $config;
    }

    public function start() {
        $server = stream_socket_server("tcp://{$this->config['host']}:{$this->config['port']}", $errorNumber, $errorString);

        if (!$server) {
            die("error: stream_socket_server: $errorString ($errorNumber)\r\n");
        }

        list($pid, $master, $workers) = $this->spawnWorkers();

        if ($pid) {
            fclose($server);
            $WebsocketMaster = new WebsocketMaster($workers);
            $WebsocketMaster->start();
        } else {
            $WebsocketHandler = new WebsocketHandler($server, $master);
            $WebsocketHandler->start();
        }
    }

    protected function spawnWorkers() {
        $master = null;
        $workers = array();
        $i = 0;
        while ($i < $this->config['workers']) {
            $i++;
            $pair = stream_socket_pair(STREAM_PF_UNIX, STREAM_SOCK_STREAM, STREAM_IPPROTO_IP);

            $pid = pcntl_fork();
            if ($pid == -1) {
                die("error: pcntl_fork\r\n");
            } elseif ($pid) {
                fclose($pair[0]);
                $workers[$pid] = $pair[1];
            } else {
                fclose($pair[1]);
                $master = $pair[0];
                break;
            }
        }

        return array($pid, $master, $workers);
    }
}