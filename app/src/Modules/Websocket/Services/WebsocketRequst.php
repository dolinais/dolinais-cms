<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Websocket\Services;

class WebsocketRequst
{
	// для хранения массива данных
    public $data;

    /**
     * Bot constructor.
     */
    public function __construct()
    {
        // записываем в свойство данные
        $this->data = json_decode(file_get_contents('php://input'));
    }

    /**
     * @return getDataAll
     */
    public function getDataAll()
    {
        return $this->data;
    }
    /**
     * @return getUserID
     */
    public function getUserID()
    {
        return 1;
    }
    /**
     * @return getClientID
     */
    public function getClientID()
    {
        if (isset($this->data->client_id)) {
            return $this->data->client_id;
        }
        exit('client_id not installed ');
    }
    /**
     * @return getMessage
     */
    public function getMessage()
    {
        if (isset($this->data->message->text)) {
            return $this->data->message->text;
        }
        exit('message->text not installed ');
    }
    /**
     * @return getFromAdress
     */
    public function getFromAdress()
    {
        if (isset($this->data->from_the_adress)) {
            return trim($this->getFromRegion() . ", " . $this->getFromSity() . ", " . $this->getFromStreet() . " " . $this->getFromHome() . "" . $this->getFromCorps());
        }
        exit('404');
    }
    /**
     * @return getFromСoordinates
     */
    public function getFromСoordinates()
    {
        return $this->data->from_the_adress;
    }
    /**
     * @return getFromRegion
     */
    public function getFromRegion()
    {
        if (isset($this->data->from_the_adress->region)) {
            return $this->data->from_the_adress->region;
        }
        exit('from_the_adress->region not installed ');
    }
    /**
     * @return getFromSity
     */
    public function getFromSity()
    {
        if (isset($this->data->from_the_adress->sity)) {
            return $this->data->from_the_adress->sity;
        }
        exit('from_the_adress->sity not installed ');
    }
    /**
     * @return getFromStreet
     */
    public function getFromStreet()
    {
        if (isset($this->data->from_the_adress->street)) {
            return $this->data->from_the_adress->street;
        }
        exit('from_the_adress->street not installed ');
    }
    /**
     * @return getFromHome
     */
    public function getFromHome()
    {
        if (isset($this->data->from_the_adress->home)) {
            return $this->data->from_the_adress->home;
        }
        exit('from_the_adress->home not installed ');
    }
    /**
     * @return getFromCorps
     */
    public function getFromCorps()
    {
        if (isset($this->data->from_the_adress->corps)) {
            return $this->data->from_the_adress->corps;
        }
        return;
    }
     /**
     * @return getFromEntrance
     */
    public function getFromEntrance()
    {
        if (isset($this->data->from_the_adress->entrance)) {
            return $this->data->from_the_adress->entrance;
        }
        return;
    }

     /**
     * @return getToTheAdress
     */
    public function getToTheAdress()
    {
        if (isset($this->data->to_the_adress)) {
            return trim($this->getToTheRegion() . ", " . $this->getToTheSity() . ", " . $this->getToTheStreet() . " " . $this->getToTheHome() . "" . $this->getToTheCorps());
        }
        exit('404');
    }
    /**
     * @return getToTheRegion
     */
    public function getToTheRegion()
    {
        if (isset($this->data->to_the_adress->region)) {
            return $this->data->to_the_adress->region;
        }
        exit('to_the_adress->region not installed ');
    }
    /**
     * @return getToTheSity
     */
    public function getToTheSity()
    {
        if (isset($this->data->to_the_adress->sity)) {
            return $this->data->to_the_adress->sity;
        }
        exit('to_the_adress->sity not installed ');
    }
    /**
     * @return getToTheStreet
     */
    public function getToTheStreet()
    {
        if (isset($this->data->to_the_adress->street)) {
            return $this->data->to_the_adress->street;
        }
        exit('to_the_adress->street not installed ');
    }
    /**
     * @return getToTheHome
     */
    public function getToTheHome()
    {
        if (isset($this->data->to_the_adress->home)) {
            return $this->data->to_the_adress->home;
        }
        exit('to_the_adress->home not installed ');
    }
    /**
     * @return getToTheCorps
     */
    public function getToTheCorps()
    {
        if (isset($this->data->to_the_adress->corps)) {
            return $this->data->to_the_adress->corps;
        }
        return;
    }
    /**
     * @return getToTheEntrance
     */
    public function getToTheEntrance()
    {
        if (isset($this->data->to_the_adress->entrance)) {
            return $this->data->to_the_adress->entrance;
        }
        return;
    }
    /**
     * @return getOrderID
     */
    public function getOrderID()
    {
        if (isset($this->data->order_id)) {
            return $this->data->order_id;
        }
        return 'client_order_id not installed ';
    }
    /**
     * @return getLimit
     */
    public function getLimit()
    {
        if (isset($this->data->limit)) {
            return $this->data->limit;
        }
        return 1;
    }
    /**
     * @return getClientBalance
     */
    public function getClientBalance()
    {
        if (isset($this->data->client_amount)) {
            return $this->data->client_amount;
        }
        exit('client_amount not installed ');
    }
    /**
     * @return getClientPayComment
     */
    public function getClientPayComment()
    {
        if (isset($this->data->pay_comment)) {
            return $this->data->pay_comment;
        }
        exit('pay_comment not installed ');
    }
    /**
     * @return getClientPayBillStatus
     */
    public function getClientPayBillStatus()
    {
        if (isset($this->data->params->bill_status)) {
            return $this->data->params->bill_status;
        }
        return false;
    }
    /**
     * @return getClientPayBillID
     */
    public function getClientPayBillID()
    {
        if (isset($this->data->params->billId)) {
            return $this->data->params->billId;
        }
        exit('billId not installed ');
    }
    /**
     * @return getType
     */
    public function getType()
    {
        if (isset($this->data->params->type->driver)) {
            if ($this->data->params->type->driver === 'taxi') {
                return $this->data->params->type->driver;
            }elseif($this->data->params->type->driver === 'travelers'){
                return $this->data->params->type->driver;
            }elseif($this->data->params->type->driver === 'passenger'){
                return $this->data->params->type->driver;
            }elseif($this->data->params->type->driver === 'driver'){
                return $this->data->params->type->driver;
            }elseif($this->data->params->type->driver === 'balance'){
                return $this->data->params->type->driver;
            }elseif($this->data->params->type->driver === 'coordinates'){
                return $this->data->params->type->driver;
            }elseif($this->data->params->type->driver === 'alladress'){
                return $this->data->params->type->driver;
            }elseif($this->data->params->type->driver === 'history'){
                return $this->data->params->type->driver;
            }
        }
    }
}