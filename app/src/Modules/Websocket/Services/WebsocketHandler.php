<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Websocket\Services;

use app\Modules\Websocket\Services\WebsocketWorker;

class WebsocketHandler extends WebsocketWorker
{
    protected function onOpen($client, $info) {
        if(!isset($_SESSION))
        {
            session_start();
        }
        $_SESSION['pid'] = $this->pid;
    }

    protected function onClose($client) {
        if(!isset($_SESSION))
        {
            session_start();
        }
        if(isset($_SESSION['sessionID']) && isset($_SESSION['pid'])) {
            $postData = $this->api(array(
                'pid' => $_SESSION['pid'],
                'postData' => json_encode(array(
                    'url' => env('API_SOCKET'),
                    'method' => 'onClose',
                    'sessionID' => $_SESSION['sessionID'],
                    'pid' => $_SESSION['pid']
                ))
            ));
            $this->send($postData);
            $this->sendHelper($postData);
        }
    }

    protected function onMessage($client, $data) {
        if(!isset($_SESSION))
        {
            session_start();
        }
        $data = $this->decode($data);

        if (!$data['payload']) {
            return;
        }

        if (!mb_check_encoding($data['payload'], 'utf-8')) {
            return;
        }
        if(isset($_SESSION['pid'])){
            $postData = $this->api(array(
                'pid' => $_SESSION['pid'],
                'postData' => $data['payload']
            ));

            if(isset(json_decode($postData)->sessionID)) {
                $_SESSION['sessionID'] = json_decode($postData)->sessionID;
            }
            if($postData){
                $this->send($postData);
                $this->sendHelper($postData);
            }
        }
    }

    protected function onSend($data) {
        $this->sendHelper($data);
    }

    protected function send($message) {
        @fwrite($this->master, $message);
    }

    private function sendHelper($data) {
        $data = $this->encode($data);
        if(!empty($this->clients)){
            $write = $this->clients;
            if (stream_select($read, $write, $except, 0)) {
                foreach ($write as $client) {
                    @fwrite($client, $data);
                }
            }
        }
    }
    private function api($data) {
        if(isset($data['postData'])){
            if(isset(json_decode($data['postData'])->url)){
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, json_decode($data['postData'])->url);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($curl, CURLOPT_HTTPHEADER, [
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'Authorization: Bearer '.env('API_SOCKET_KEY')
                ]);
                curl_setopt($curl, CURLOPT_HEADER, 0);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $out = json_decode(curl_exec($curl));
                curl_close($curl);
                return json_encode($out);
            }
        }
    }
}