<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Websocket\Commands;

use app\Modules\Websocket\Services\WebsocketServer;

class IndexWebsocketCommands
{
    public function __construct()
    {
        $config = array(
            'host' => env('HOST_SOCKET'),
            'port' => env('PORT_SOCKET'),
            'workers' => 1,
        );

        $WebsocketServer = new WebsocketServer($config);
        $WebsocketServer->start();
    }
}
