<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Tg\Controllers;

use app\Modules\Tg\Services\TgRequstService;
use app\Modules\Tg\Services\ServiceMessageTg;
use app\Modules\Tg\Components\TgAPI;
use app\Modules\Tg\Models\TgUserModels;
use app\Modules\Tg\Models\TgModels;
use app\Modules\Tg\Services\KeyboardServiceTg;
use app\Modules\Tg\Services\CommandsServiceTg;
use app\Core\LogCore;
use app\Core\RedirectCore;
use app\Modules\Account\Models\AccountModels;

class IndexTgController
{
    public function actionIndex()
    {
        if($_GET["token"] == "3ab553e7abf98460ce9831bc7c226345"){

            // var_dump(json_decode(json_encode(array(
            //     'params' => 'menu',
            //     'services' => 'search'
            // )))->services);
            // exit;

            $output = new TgRequstService();
            LogCore::Log('log', $output->getText());

            if($output->getText() && $output->getChatId()){
                ServiceMessageTg::sendChatAction($output->getChatId(), 'typing');
                if(!TgUserModels::get()->from()->where(['chat_member_id', '=', $output->getChatId()])->fetch()){
                    TgUserModels::Insert(array(
                        'chat_member_id' => $output->getChatId()
                    ));
                    ServiceMessageTg::SendMassage($output->getChatId(), 'Регистрация, отправьте код полученый в личном кабинете' );
                    exit("ok");
                }

                if($tgmodel = TgUserModels::get()->from()->where(['chat_member_id', '=', $output->getChatId()])->fetch()){
                    if($tgmodel[0]->status != 1){
                        if (filter_var($output->getText(), FILTER_VALIDATE_INT) === false ) {
                            ServiceMessageTg::SendMassage($tgmodel[0]->chat_member_id, 'Регистрация, отправьте код полученый в личном кабинете' );
                            exit("ok");
                        }
                        if($usermodel = AccountModels::get()->from()->where(['code', '=', $output->getText()])->fetch()){
                            TgUserModels::get()->getUpdate(
                                array(
                                    'user_id' => $usermodel[0]->id,
                                    'site_id' => $usermodel[0]->site_id,
                                    'status' => 1,
                                    'updated_at' => time()
                                ),
                                $tgmodel[0]->id
                            );
                            AccountModels::get()->getUpdate(
                                array(
                                    'code' => NULL,
                                    'updated_at' => time()
                                ),
                                $usermodel[0]->id
                            );
                            ServiceMessageTg::SendMassage($tgmodel[0]->chat_member_id, 'Подписка оформлена!' );
                            exit("ok");
                        }
                        ServiceMessageTg::SendMassage($tgmodel[0]->chat_member_id, 'Регистрация, отправьте код полученый в личном кабинете' );
                        exit("ok");
                    }

                    CommandsServiceTg::startMessage($output);
                    exit("ok");
                }
            }
            exit("ok");
        }
        if($_GET["token"] != "3ab553e7abf98460ce9831bc7c226344"){
            RedirectCore::to('/account');
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $output = new TgRequstService();

            LogCore::Log('log', $output->getAll());

            if(!TgUserModels::get()->from()->where(['chat_member_id', '=', $output->getBusinessFromId()])->orderBy('id', 'ASC')->limit(1)->fetch()){
                TgUserModels::Insert(array(
                    'chat_member_id' => $output->getBusinessFromId()
                ));
                TgAPI::sendTelegram('sendMessage',
                    array(
                        'business_connection_id' =>$output->getBusiness(),
                        'chat_id' => $output->getBusinessFromId(),
                        'text' => self::Chat('Сообщение от: '.$output->getBusinessFromFirstName().'. '.$output->getBusinessFromText()),
                        'parse_mode' => 'HTML'
                    )
                );
            }else{
                TgAPI::sendTelegram('sendMessage',
                    array(
                        'business_connection_id' =>$output->getBusiness(),
                        'chat_id' => $output->getBusinessFromId(),
                        'text' => self::Chat('Сообщение от: '.$output->getBusinessFromFirstName().'. '.$output->getBusinessFromText()),
                        'parse_mode' => 'HTML'
                    )
                );
            }
            exit("ok");
        }else{
            exit("ok");
        }

        $headers = getallheaders();

        if (isset($headers['Authorization']) && substr($headers['Authorization'], 0, 7) !== 'Bearer ') {
            echo json_encode(["error" => "Bearer keyword is missing"]);
            return;
        }

        if (isset($headers['Authorization'])){
            $token = trim(substr($headers['Authorization'], 6));
            if($token != 123){
                echo json_encode([
                    "code" => 401,
                    "error" => "Bearer keyword is missing"]);
                return;
            }
        }

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return "ok";
        }
    }

    static function Chat($message = 'здравствуйте'){
        $answer = "";
        $url = "https://gigachat.devices.sberbank.ru/api/v1/chat/completions";
        $headers = [
            'Authorization: Bearer ' .TgModels::tokenGiga()[0]["token"],
            'Content-Type: application/json'
        ];
        $messages = [];
        $messages[] = [
            "role" => "system",
            "content"=> "Ты виртуальный помощник Игорь, компании Долина ИС. Контакты Долина ИС mail@dolinais.ru, сайт dolinais.ru, номер телефона +79200887766. Прайс, цены на услуги: Сайт визитка от 60 000р, корпоративный сайт 150 000р, интернет магазин 250 000р, обслуживание сайтов от 15 000р."
       ];
        $messages[] = [
            "role" => "user",
            "content"=> $message
        ];
        $data = [
          "model" => "GigaChat:latest",
          "temperature" => 0.7,
          "messages" => $messages
        ];
        $result = self::get($url, $headers, json_encode($data));
        if(isset($result["status"]) && $result["status"] == 401){
            $url = "https://ngw.devices.sberbank.ru:9443/api/v2/oauth";
            $headers = [
             'Authorization: Bearer '.env('GigaChatAPI'),
             'RqUID: '.self::guidv4(),
             'Content-Type: application/x-www-form-urlencoded'
            ];
            $data = [
             'scope'=>'GIGACHAT_API_PERS'
            ];
            $result = self::get($url, $headers, http_build_query($data));
            TgModels::tokenGiga(true, $result["access_token"]);
            return 'Повторите пожалуйста запрос.';
        }
        $answer = $result["choices"][0]["message"]["content"];
        if(!empty($answer)){
            $messages[] = [
                "role" => "assistant",
                "content"=> $answer
            ];
           $messages2 = $messages;
        }
        return $answer ;
    }

    static function get($url, $headers, $data){
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => 1,
        ]);
        if(!empty($data)){
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        $result = curl_exec($curl);
        return json_decode($result,true);
    }

    static function guidv4($data = null) {
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}