<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Tg\Controllers;

use app\Modules\Tg\Services\TgRequstService;
use app\Modules\Tg\Components\TgAPI;
use app\Modules\Tg\Models\TgUserModels;
use app\Modules\Tg\Models\TgModels;
use app\Core\LogCore;
use app\Core\RedirectCore;

class STgController
{
    public function actionIndex()
    {
        if(isset($_GET["code"])){
            if(false){
                $url = "https://ngw.devices.sberbank.ru:9443/api/v2/oauth";
                $headers = [
                 'Authorization: Bearer OTQ1ZjlhYjYtODdmNy00NmVkLTkwNTgtYjhkZTZiMjRhZDBlOmM2NjBiM2EwLTQwYjgtNDlmMC1iNjhlLTdmOGQ1MTQ4ZWRmYg==',
                 'RqUID: '.self::guidv4(),
                 'Content-Type: application/x-www-form-urlencoded'
                ];
                $data = [
                 'scope'=>'SALUTE_SPEECH_PERS'
                ];
                $result = self::get($url, $headers, http_build_query($data));
                var_dump($result);
                return;
            }
            $url = "https://smartspeech.sber.ru/rest/v1/text:synthesize?format=wav16&voice=Bys_24000";
            $headers = [
                'Authorization: Bearer eyJjdHkiOiJqd3QiLCJlbmMiOiJBMjU2Q0JDLUhTNTEyIiwiYWxnIjoiUlNBLU9BRVAtMjU2In0.WRsd7Hgr-_yhTdmI_7s0pA1Db4iT--Dd3jAvK4lv9VaFyNExEOZe1aHx6rJPUYO31ZdhmOca4EarwiYlD-DgH53JNl-5CEbcznzVeleetBAOsKOVgxukLPMJBYtSvDjA2ADFlj2Z3ORIfyNSFPzCnmBbdTU50vIbLQaM6qmozss_V9ftfdBS7OVCo-iBHUJIg7-fTUUefOFObPJhU7F74Mn-hEQ4V2pYc6H_qFIArAArUsavA0PguCHI0XRM43hQ40tXJ-DtktY2MDg5soJTkyvB_GSDZXgw7GYGgfbqt3cemULseIa-uV0Zckob0pH0U-MKNb-CiV8APATRCVzxhg.9MTKZPAUGSPgOVxRuQ70jw.gLFH1A-DPNcMm36jqB4MIhf84PNVP7fn3zKDqKqbud1XdHEsKxGlooq92J0LhLNBgcnNe_HW7dBY1nuTb0HsuIg8TKC0WhBQ3EQ4mThYiPNEuGfRrvMd2NQW52dHMBmL5lMx6PrB6sLsUXKq86RykxEbdRchscJFEYMklmQIAmCzvSOcx8fFD1hbDSOtM8ysjmrB-SpUzcGyV1FqdRPM7V-zUbiZX89DFS3DiMBp3K3GbuVcTBQRwCXMxIN9DEqi5oGo_emNx-nIam7bNFNCehnE12Gk45k1onceAF19dODvIbG1BA6oM2GVm53jDsbTs3Rh6CXCky7eRJplAGC4SW-tmbGMtdLkP9KELw9KBVN66GC6DMpAq4Zoq03m6FX4PZx_mQ9GphvdP4zmcSoccQvrjMP00C95d5Vt5jAoT1pMVbU1a7TXaeOZNnemqK3yBHfWXs7u7ULmFsvA_IZij6BLlw5y2kOQldED9w_cuHYCEjqWB7S-Ru9YD9cULhq0D90ctmuMTUU7_H_gvaPkmpMCTho1d3RIgXX52v66Li9w0eDG_sI5kRgMmnksE2x5h66zdA8Rfd04p_dd3d9yP-OL5I0f1-KGi0hQ2YVWNMulexiNyP5oG2UihtwQ9OWVzcNshMyvmlvUMB_V3E1S3iL4ZvPR7JNSEQOvk5IPKcHlcXrhllrbhz5x1tGqBzmanM8GcY9iH1JmMZkpoHK3BGnHsv4tF9qqZyir7TzgXss.GtPsOR4X6CPagofHNVkZu5vqcQUqSAb8mAimeALEWAA',
                'Content-Type: application/ssml'
            ];
            $data = 'Привет, меня зовут, Игорь! Сегодня я хотел бы, презентовать новый продукт.';

            $result = self::getAudio($url, $headers, $data);
            header("Content-Type: audio/ogg;codecs=opus");
            echo $result;
            return;
        }
        echo json_encode([
            "code" => 401,
            "error" => "Bearer keyword is missing"]);
        exit;
    }

    static function getAudio($url, $headers, $data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTP_VERSION, '1.0');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt($ch, CURLOPT_HTTPHEADER,  $headers);

        $result = curl_exec($ch);
        return $result;
    }

    static function get($url, $headers, $data){
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => 1,
        ]);
        if(!empty($data)){
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        $result = curl_exec($curl);
        return json_decode($result,true);
    }


    static function guidv4($data = null) {
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}