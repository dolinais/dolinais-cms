<?php

declare(strict_types=1);

namespace app\Modules\Tg\Services;

use app\Modules\Tg\Components\TgAPI;
use app\Modules\Tg\Services\KeyboardServiceTg;

define ('dbname', 'kassa');

class ServiceMessageTg
{

	public static function SendMassageQuery($method, $data){
		return TgAPI::sendTelegram($method,	$data,
			dbname
		);
	}
	public static function SendMassage($user_telegram_id, $message){
		return TgAPI::sendTelegram('sendMessage',
			array(
				'chat_id' => $user_telegram_id,
				'text' => $message,
				'parse_mode' => 'HTML'
			),
			dbname
		);
	}
	public static function editMessageText($user_telegram_id, $message_id, $message){
		return TgAPI::sendTelegram('editMessageText',
			array(
				'message_id' => $message_id,
				'chat_id' => $user_telegram_id,
				'text' => $message,
				'parse_mode' => 'HTML'
			),
			dbname
		);
	}
	public static function deleteMessage($user_telegram_id, $message_id){
		return TgAPI::sendTelegram('deleteMessage',
			array(
				'chat_id' => $user_telegram_id,
        		'message_id' =>$message_id,
			),
			dbname
		);
	}
	public static function getChat($user_telegram_id){
		return TgAPI::sendTelegram('getChatMember',
			array(
				'chat_id' => $user_telegram_id,
				'user_id' => $user_telegram_id,
			),
			dbname
		);
	}
	public static function sendChatAction($user_telegram_id, $message){
		return TgAPI::sendTelegram('sendChatAction',
			array(
				'chat_id' => $user_telegram_id,
				'action' => $message
			),
			dbname
		);
	}
	public static function SendContact($user_telegram_id, $phone_number, $message){
		return TgAPI::sendTelegram('sendContact',
			array(
				'chat_id' => $user_telegram_id,
				'phone_number' => $phone_number,
				'first_name' => $message
			),
			dbname
		);
	}
	public static function SendforwardMessage($user_telegram_id, $from_chat_id, $message_id){
		return TgAPI::sendTelegram('forwardMessage',
			array(
				'chat_id' => $user_telegram_id,
				'from_chat_id' => $from_chat_id,
				'message_id' => $message_id
			),
			dbname
		);
	}
	public static function SendUserKeyboard($user_telegram_id=null, $message=null,  $keyboard_id=null, $params=null){
		if($keyboard_id === null){
			$keyboard_id = 0;
		}
		return TgAPI::sendTelegram('sendMessage',
			array(
				'chat_id'		=> $user_telegram_id,
				'text'			=> $message,
				'parse_mode' 	=> 'HTML',
				'reply_markup'	=> json_encode(
					array('inline_keyboard' => array(KeyboardServiceTg::keyParamsMessage($keyboard_id, $params, $user_telegram_id)))
				)
			),
			dbname
		);
	}
	public static function SendURLKeyboard($user_telegram_id, $keyboard_id, $message, $params=null){
		return TgAPI::sendTelegram('sendMessage',
			array(
				'chat_id'		=> $user_telegram_id,
				'text'			=> $message,
				'parse_mode' 	=> 'HTML',
				'reply_markup'	=> json_encode(
					array('inline_keyboard' => array(KeyboardServiceTg::keyParamsMessage($keyboard_id, $params, $user_telegram_id)))
				)
			),
			dbname
		);
	}
	public static function editSendKeyboard($user_telegram_id, $message_id, $keyboard_id, $message, $params=null){
		return TgAPI::sendTelegram('editMessageText',
			array(
				'chat_id' => $user_telegram_id,
        		'message_id' => $message_id,
        		'text' => $message,
        		'reply_markup'	=> json_encode(
					array('inline_keyboard' => array(KeyboardServiceTg::keyParamsMessage($keyboard_id, $params, $user_telegram_id)))
				)
			),
			dbname
		);
	}
	public static function SendKeyboardLocation($user_telegram_id, $keyboard_id, $message){
		return TgAPI::sendTelegram('sendMessage',
			array(
				'chat_id'		=> $user_telegram_id,
				'text'			=> $message,
				'parse_mode' 	=> 'HTML',
				'reply_markup'	=> json_encode(
					array('keyboard' => array(KeyboardServiceTg::keyLocation($keyboard_id)))
				)
			),
			dbname
		);
	}
	public static function SendKeyboardRemove($user_telegram_id, $message){
		return TgAPI::sendTelegram('sendMessage',
			array(
				'chat_id'		=> $user_telegram_id,
				'text'			=> $message,
				'reply_markup'	=> json_encode(
					array('remove_keyboard' => true)
				)
			),
			dbname
		);
	}
	public static function sendInvoice($user_telegram_id, $keyboard_id, $message){
		return TgAPI::sendTelegram('sendInvoice',
			array(
				'chat_id' => $user_telegram_id,
				'title' => "Оплата доступа",
				'description' => "Добавить ещё 5 попыток.",
				'photo_url' => "https://ylianova.ru/800/600/https/cdn3.vectorstock.com/i/1000x1000/40/27/shopping-basket-flat-icon-vector-19134027.jpg",
				'photo_width' => 90,
				'photo_height' => 50,
				'payload' => "telebot-test-invoice",
				'provider_token' => "401643678:TEST:8b807066-27e4-4633-a7e7-58461af9655b",
				'start_parameter' => "pay",
				'currency' => "RUB",
				'prices' => json_encode(array(array('label' => "Доступ к игре", 'amount' => 1100))),
			),
			dbname
		);
	}
	public static function SendPhoto($user_telegram_id, $message, $photo){
		return TgAPI::sendTelegram('sendPhoto',
			array(
				'chat_id' => $user_telegram_id,
				'caption' => $message,
				'photo' => $photo,
				'parse_mode' => 'html'
			),
			dbname
		);
	}
	public static function SendDocument($user_telegram_id, $message, $document){
		return TgAPI::sendTelegram('sendDocument',
			array(
				'chat_id' => $user_telegram_id,
				'caption' => $message,
				'document' => $document
			),
			dbname
		);
	}
	public static function SendAudio($user_telegram_id, $message, $audio){
		return TgAPI::sendTelegram('sendAudio',
			array(
				'chat_id' => $user_telegram_id,
				'caption' => $message,
				'audio' => $audio
			),
			dbname
		);
	}
	public static function SendVoice($user_telegram_id, $voice){
		return TgAPI::sendTelegram('sendVoice',
			array(
				'chat_id' => $user_telegram_id,
				'voice' => $voice
			),
			dbname
		);
	}
	public static function SendVideo($user_telegram_id, $video){
		return TgAPI::sendTelegram('sendVideo',
			array(
				'chat_id' => $user_telegram_id,
				'video' => $video
			),
			dbname
		);
	}
	public static function SendLocation($user_telegram_id, $latitude, $longitude){
		return TgAPI::sendTelegram('sendLocation',
			array(
				'chat_id' => $user_telegram_id,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'live_period' => 900
			),
			dbname
		);
	}
	public static function editMessageLiveLocation($user_telegram_id, $message_id, $latitude, $longitude){
		return TgAPI::sendTelegram('editMessageLiveLocation',
			array(
				'chat_id' => $user_telegram_id,
				'message_id' => $message_id,
				'latitude' => $latitude,
				'longitude' => $longitude
			),
			dbname
		);
	}
	public static function stopMessageLiveLocation($user_telegram_id, $message_id){
		return TgAPI::sendTelegram('stopMessageLiveLocation',
			array(
				'chat_id' => $user_telegram_id,
				'message_id' => $message_id
			),
			dbname
		);
	}
	public static function sendVenue($user_telegram_id, $latitude, $longitude, $address){
		return TgAPI::sendTelegram('sendVenue',
			array(
				'chat_id' => $user_telegram_id,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'title' => 'Нажмите на карту:',
				'address' => $address
			),
			dbname
		);
	}
	public static function sendPoll($user_telegram_id, $options=null){
		$options = array('Водитель','Пассажир', 'Грузоперевозчик');
		return TgAPI::sendTelegram('sendPoll',
			array(
				'chat_id' => $user_telegram_id,
				'question' => 'Кем Вы являетесь?',
				'options' => json_encode($options),
				'is_anonymous' => false
			),
			dbname
		);
	}
	public static function sendSticker($user_telegram_id, $sticker){
		return TgAPI::sendTelegram('sendSticker',
			array(
				'chat_id' => $user_telegram_id,
				'sticker' => $sticker
			),
			dbname
		);
	}
	public static function answerCallbackQuery($callback_query_id, $text){
		return TgAPI::sendTelegram('answerCallbackQuery',
			array(
				'callback_query_id' => $callback_query_id,
				'text' => $text,
				'show_alert' => true
			),
			dbname
		);
	}
}
