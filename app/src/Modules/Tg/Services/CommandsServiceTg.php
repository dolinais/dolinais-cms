<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Tg\Services;

use app\Modules\Tg\Models\TgUserModels;
use app\Modules\Tg\Models\TgUserCommandModels;
use app\Modules\Tg\Services\ServiceMessageTg;
use app\Modules\Api\Models\ApiNotificationModels;
use app\Modules\Account\Models\AccountModels;

class CommandsServiceTg
{
    public static function startMessage($output)
    {
        if($tgmodel = TgUserModels::get()->from()->where(['chat_member_id', '=', $output->getChatId()])->fetch()){
            $tgusermodelid = $tgmodel[0]->id;
        }else{
            ServiceMessageTg::SendMassage($output->getChatId(), 'Ошибка!' );
        }
        if ($command_step = TgUserCommandModels::get()->from()->where(['telegram_id', '=', $tgusermodelid])->fetch()[0]->command_step) {
            switch ($command_step) {
                //taxi
                case 'massager':
                    ApiNotificationModels::Create(array(
                        'title' => self::Profile($output),
                        'text' => $output->getText(),
                        'user_id' => 1
                    ));
                    TgUserCommandModels::Update(array('telegram_id' => $tgusermodelid));
                    ServiceMessageTg::SendMassage($output->getChatId(), 'Отправлено!' );
                    ServiceMessageTg::SendUserKeyboard($output->getChatId(), 'Навигация', 0 );
                break;
                default:
                    // $output->deleteMessageSelf();
                break;
            }
        }else{
            if(isset(json_decode($output->getText())->params)){
                switch (json_decode($output->getText())->params) {
                    case 'menu':
                        switch (json_decode($output->getText())->services) {
                            case 'stock':
                                ServiceMessageTg::SendUserKeyboard($output->getChatId(), 'Навигация', 1 );
                            break;
                            case 'revenue':
                                ServiceMessageTg::SendUserKeyboard($output->getChatId(), 'Выручка', 1 );
                            break;
                            case 'services':
                                ServiceMessageTg::SendUserKeyboard($output->getChatId(), 'Навигация', 3 );
                            break;
                        }
                    break;
                    case 'services':
                        switch (json_decode($output->getText())->services) {
                            case 'ads':
                                TgUserCommandModels::Update(array(
                                    'telegram_id' => $tgusermodelid,
                                    'command_step' => 'massager'
                                ));
                                ServiceMessageTg::SendMassage($output->getChatId(), 'Напишите сообщение!' );
                            break;
                            case 'game':
                                ServiceMessageTg::SendMassage($output->getChatId(), self::Profile($output));
                                ServiceMessageTg::SendUserKeyboard($output->getChatId(), 'Навигация' );
                            break;
                        }
                    break;
                    default:
                        ServiceMessageTg::SendUserKeyboard($output->getChatId(), 'Навигация' );
                    break;
                }
            }else{
                ServiceMessageTg::SendUserKeyboard($output->getChatId(), 'Навигация' );
            }
        }
    }

    public static function Profile($output){
        if($tgmodel = TgUserModels::get()->from()->where(['chat_member_id', '=', $output->getChatId()])->fetch()){
            $user = AccountModels::get()->from()->where(['id', '=', $tgmodel[0]->user_id])->fetch()[0];
            return $user->last_name.' '.$user->first_name;
        }
    }
}