<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Tg\Services;

use app\Modules\Tg\Services\ServiceMessageTg;

class TgRequstService
{
    // для хранения массива данных от Телеграм
    public $data;

    /**
     * Bot constructor.
     */
    public function __construct()
    {
        // записываем в свойство данные
        $this->data = json_decode(file_get_contents('php://input'));
    }
    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->data;
    }
    /**
     * @return mixed
     */
    public function getUpdateId()
    {
        return $this->data->update_id;
    }
    /** getBusiness
     * @return mixed
     */
    public function getBusiness(){
        return $this->data->business_message->business_connection_id;
    }
    public function getBusinessFromId(){
        return $this->data->business_message->from->id;
    }
    public function getBusinessFromFirstName(){
        return $this->data->business_message->from->first_name;
    }
    public function getBusinessFromUsername(){
        return $this->data->business_message->from->username;
    }
    public function getBusinessFromMessageId(){
        return $this->data->business_message->message_id;
    }
    public function getBusinessFromText(){
        return $this->data->business_message->text;
    }
    public function getBusinessReplyToMessage(){
        return $this->data->business_message->reply_to_message;
    }
    /** Получаем id
     * @return mixed
     */
    public function getChatId()
    {
        if ($this->isCallBack()) {
            return $this->data->callback_query->message->chat->id;
        } elseif ($this->isMessage()) {
            return $this->data->message->chat->id;
        } elseif ($this->isEditedMessage()) {
            return $this->data->edited_message->chat->id;
        } elseif ($this->isChatMember()) {
            return $this->data->my_chat_member->from->id;
        } else {
            return null;
        }
    }

    /** Получаем first_name
     * @return mixed
     */
    public function getChatFirstName()
    {
        if ($this->isCallBack()) {
            return $this->data->callback_query->message->chat->first_name;
        } elseif ($this->isMessage()) {
            return $this->data->message->chat->first_name;
        } elseif ($this->isEditedMessage()) {
            return $this->data->edited_message->chat->first_name;
        } else {
            return null;
        }
    }

    /** Получаем last_name
     * @return mixed
     */
    public function getChatLastName()
    {
        if ($this->isCallBack()) {
            return $this->data->callback_query->message->chat->last_name;
        } elseif ($this->isMessage()) {
            return $this->data->message->chat->last_name;
        } elseif ($this->isEditedMessage()) {
            return $this->data->edited_message->chat->last_name;
        } else {
            return null;
        }
    }

    /** Получаем username
     * @return mixed
     */
    public function getChatUserName()
    {
        if ($this->isCallBack()) {
            return $this->data->callback_query->message->chat->username;
        } elseif ($this->isMessage()) {
            return $this->data->message->chat->username;
        } elseif ($this->isEditedMessage()) {
            return $this->data->edited_message->chat->username;
        } else {
            return null;
        }
    }

    public function getFullName()
    {
        return trim($this->getChatFirstName() . " " . $this->getChatLastName());
    }

    /** Получаем id сообщения
     * @return mixed
     */
    public function getMessageId()
    {
        if ($this->isCallBack()) {
            return $this->data->callback_query->message->message_id;
        } elseif ($this->isMessage()) {
            return $this->data->message->message_id;
        } elseif ($this->isEditedMessage()) {
            return $this->data->edited_message->message_id;
        } else {
            return null;
        }
    }
    public function getStatus()
    {
        if ($this->isChatMember()) {
            return $this->data->my_chat_member;
            // return $this->data->my_chat_member->old_chat_member->status;
            // return $this->data->my_chat_member->from->id;
        } else {
            return NULL;
        }
    }

    /** Получим значение текст
     * @return mixed
     */
    public function getText()
    {
        if ($this->isCallBack()) {
            return $this->data->callback_query->data;
        } elseif ($this->isMessage()) {
            if ($this->isAudio() || $this->isDocument() || $this->isVoice() || $this->isPhoto()) {
                return $this->data->message->caption;
            } elseif ($this->isLocation()) {
                return json_encode($this->data->message->location);
            } elseif ($this->isText()) {
                return $this->data->message->text;
            } else {
                return NULL;
            }
        } elseif ($this->isEditedMessage()) {
            if ($this->isEditedAudio() || $this->isEditedDocument() || $this->isEditedVoice() || $this->isEditedPhoto()) {
                return $this->data->edited_message->caption;
            } elseif ($this->isEditedLocation()) {
                return json_encode($this->data->edited_message->location);
            } elseif ($this->isEditedText()) {
                return $this->data->edited_message->text;
            } else {
                return NULL;
            }
        }
        return NULL;
    }

    /** Узнаем какой тип данных пришел
     * @return bool|string
     */
    public function getType()
    {
        if (isset($this->data->callback_query)) {
            return "callback_query";
        } elseif (isset($this->data->message)) {
            return "message";
        } elseif (isset($this->data->edited_message)) {
            return "edited_message";
        } elseif (isset($this->data->my_chat_member)) {
            return "my_chat_member";
        } else {
            return false;
        }
    }

    public function isCallBack()
    {
        return $this->getType() == "callback_query";
    }

    public function isMessage()
    {
        return $this->getType() == "message";
    }

    public function isEditedMessage()
    {
        return $this->getType() == "edited_message";
    }

    public function isChatMember()
    {
        return $this->getType() == "my_chat_member";
    }

    public function isStatus()
    {
        return isset($this->data->my_chat_member->old_chat_member->status);
    }

    public function isText()
    {
        return isset($this->data->message->text);
    }

    public function isEditedText()
    {
        return isset($this->data->edited_message->text);
    }

    public function isPhoto()
    {
        return isset($this->data->message->photo);
    }

    public function isEditedPhoto()
    {
        return isset($this->data->edited_message->photo);
    }

    public function isAudio()
    {
        return isset($this->data->message->audio);
    }

    public function isEditedAudio()
    {
        return isset($this->data->edited_message->audio);
    }

    public function isDocument()
    {
        return isset($this->data->message->document);
    }

    public function isEditedDocument()
    {
        return isset($this->data->edited_message->document);
    }

    public function isAnimation()
    {
        return isset($this->data->message->animation);
    }

    public function isEditedAnimation()
    {
        return isset($this->data->edited_message->animation);
    }

    public function isSticker()
    {
        return isset($this->data->message->sticker);
    }

    public function isEditedSticker()
    {
        return isset($this->data->edited_message->sticker);
    }

    public function isVoice()
    {
        return isset($this->data->message->voice);
    }

    public function isEditedVoice()
    {
        return isset($this->data->edited_message->voice);
    }

    public function isVideoNote()
    {
        return isset($this->data->message->video_note);
    }

    public function isEditedVideoNote()
    {
        return isset($this->data->edited_message->video_note);
    }

    public function isVideo()
    {
        return isset($this->data->message->video);
    }

    public function isEditedVideo()
    {
        return isset($this->data->edited_message->video);
    }

    public function isLocation()
    {
        return isset($this->data->message->location);
    }

    public function isEditedLocation()
    {
        return isset($this->data->edited_message->location);
    }

    public function isReplyMessage()
    {
        return isset($this->data->message->reply_to_message);
    }

    public function getReplyMessageId()
    {
        return $this->data->message->reply_to_message->message_id;
    }

    public function replyForwardFromId()
    {
        return $this->data->message->reply_to_message->forward_from->id;
    }

    public function isBotReplyMessage()
    {
        return !!$this->data->message->reply_to_message->from->is_bot;
    }

    /** Получаем entities
     * @return object | null
     */
    public function getEntities()
    {
        if ($this->isMessage()) {
            return isset($this->data->message->entities) ? $this->data->message->entities : NULL;
        } elseif ($this->isEditedMessage()) {
            return isset($this->data->edited_message->entities) ? $this->data->edited_message->entities : NULL;
        } else {
            return NULL;
        }
    }

    /** Получаем тип сообщения
     * @return string
     */
    public function getMessageType()
    {
        if ($this->isText()) {
            return "text";
        } elseif ($this->isPhoto()) {
            return "photo";
        } elseif ($this->isAudio()) {
            return "audio";
        } elseif ($this->isDocument()) {
            return "document";
        } elseif ($this->isAnimation()) {
            return "animation";
        } elseif ($this->isSticker()) {
            return "sticker";
        } elseif ($this->isVoice()) {
            return "voice";
        } elseif ($this->isVideoNote()) {
            return "video_note";
        } elseif ($this->isVideo()) {
            return "video";
        } elseif ($this->isLocation()) {
            return "location";
        } else {
            return "no_detected";
        }
    }

    public function getMessageFileId()
    {
        $message = $this->data->message;
        if ($this->isPhoto()) {
            return end($message->photo)->file_id;
        } elseif ($this->isAudio()) {
            return $message->audio->file_id;
        } elseif ($this->isDocument()) {
            return $message->document->file_id;
        } elseif ($this->isAnimation()) {
            return $message->animation->file_id;
        } elseif ($this->isSticker()) {
            return $message->sticker->file_id;
        } elseif ($this->isVoice()) {
            return $message->voice->file_id;
        } elseif ($this->isVideoNote()) {
            return $message->video_note->file_id;
        } elseif ($this->isVideo()) {
            return $message->video->file_id;
        } else {
            return NULL;
        }
    }

    /** Уведомление в клиенте
     * @param $text
     * @param bool $type
     */
    public function notice($text = "", $type = false)
    {
        if (!empty($text)) {
            $data['text'] = $text;
        }
        TelegramServiceMessage::answerCallbackQuery($this->data->callback_query->id, $text, $type);
    }

    /**
     * @param string $text
     * @param bool $type
     */
    public function noticeDelete($text = "", $type = false)
    {
        if ($this->isCallBack()) {
            $this->notice($text, $type);
            $this->deleteMessageSelf();
        }
    }

    /** Удаляем сообщение без параметров
     * @return mixed
     */
    public function deleteMessageSelf()
    {
        ServiceMessageTg::deleteMessage($this->getChatId(), $this->getMessageId());
        return $this->getMessageId();
    }
    /** Отправляем копию сообщения $TelegramRequstService->sendCopy(413896765, 1);
     * @param $user_id
     * @param int $type
     * @return mixed
     */
    public function sendCopy($user_id, $type = 0)
    {
        $messageType = $this->getMessageType();
        $dop = "";

        if ($type) {
            $link = !empty($this->getChatUserName())
                ? "@" . $this->getChatUserName()
                : "<a href='tg://user?id=" . $this->getChatId() . "'>Профиль</a>";
            $dop = "USER_ID::" . $this->getChatId() . "::\nfrom <b>" . $this->getFullName() . "</b> | " . $link . "\n----- " . $messageType . " ----\n";
        }

        // дополнительное сообщение для feedback
        $dopSend = !in_array($messageType, ['location', 'sticker', 'video_note']) ? 0 : 1;

        $data['chat_id'] = $user_id;
        $data['parse_mode'] = 'html';

        if($messageType == "location") {
            $data['longitude'] = $this->data->location->longitude;
            $data['latitude'] = $this->data->location->latitude;
        } elseif($messageType == "text") {
            $data['text'] = $dop . $this->prepareMessageWithEntities($this->getText(), $this->getEntities());
        } else {
            $data[$messageType] = $this->getMessageFileId();
        }

        if(!in_array($messageType, ['text', 'location', 'sticker', 'video_note'])) {
            $data['caption'] = $dop . $this->prepareMessageWithEntities($this->getText(), $this->getEntities());
        }

        $method = $this->getMethodByMessageType($messageType);
        if (!is_null($method)) {
            // отправляем доп инфу (USER_ID) админу это в тех случаях когда в сообщении нет текстовой оcновы
            if ($type && $dopSend) {
                return 'Можно отправить только текст!';
            }
            $result = TelegramServiceMessage::SendMassageQuery('sendMessage', $data);
            return $result['result']['message_id'];
        }
    }
    /** Определяем метод по подтипу message
     * @param $type
     * @return string|null
     */
    public function getMethodByMessageType($type) {
        if($type == "photo") {
            return "sendPhoto";
        } elseif($type == "text") {
            return "sendMessage";
        } elseif($type == "audio") {
            return "sendAudio";
        } elseif($type == "document") {
            return "sendDocument";
        } elseif($type == "animation") {
            return "sendAnimation";
        } elseif($type == "sticker") {
            return "sendSticker";
        } elseif($type == "voice") {
            return "sendVoice";
        } elseif($type == "video_note") {
            return "sendVideoNote";
        } elseif($type == "video") {
            return "sendVideo";
        } elseif($type == "location") {
            return "sendLocation";
        } else {
            return NULL;
        }
    }

    /** Добавляем форматирование
     * @param $text
     * @param $entities
     * @return mixed
     */
    public function prepareMessageWithEntities($text, $entities)
    {
        if ($entities) {
            $prepareText = "";
            foreach ($entities as $key => $entity) {
                // добавляем все что между форматированием
                if ($entity->offset > 0) {
                    /*
                     * старт = если начало больше 0 и это первый элемент то берем сначала с нуля
                     * если не первый то берем сразу после предыдущего элемента
                     *
                     * длина = это разница между стартом и текущим началом
                     */
                    $start = $key == 0 ? 0 : ($entities[$key - 1]->offset + $entities[$key - 1]->length);
                    $length = $entity->offset - $start;
                    // добавляем
                    $prepareText .= mb_substr($text, $start, $length);
                }
                // выбираем текущий элемент форматирования
                $charts = mb_substr($text, $entity->offset, $entity->length);
                // обрамляем в необходимый формат
                if ($entity->type == "bold") {
                    $charts = "<b>" . $charts . "</b>";
                } elseif ($entity->type == "italic") {
                    $charts = "<i>" . $charts . "</i>";
                } elseif ($entity->type == "code") {
                    $charts = "<code>" . $charts . "</code>";
                } elseif ($entity->type == "pre") {
                    $charts = "<pre>" . $charts . "</pre>";
                } elseif ($entity->type == "text_link") {
                    $charts = "<a href='" . $entity->url . "'>" . $charts . "</a>";
                }
                // добавляем
                $prepareText .= $charts;
            }
            // добавляем остатки текста если такие есть
            $prepareText .= mb_substr($text, (end($entities)->offset + end($entities)->length));
            // возвращаем результат
            return $prepareText;
        }
        return $text;
    }
}