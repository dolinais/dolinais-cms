<?php

declare(strict_types=1);

namespace app\Modules\Tg\Services;


use app\Modules\Tg\Models\TgUserModels;
use app\Modules\Tg\Models\TgKeyboard;

class KeyboardServiceTg
{
	public static function keyParamsMessage($key=null, $params=null, $chat_member_id=null)
	{
		foreach(TgKeyboard::get()->from()->where(['keyboard_group', '=', $key])->orderBy('sorting', 'ASC')->fetch() as $p){
			if (isset($params)) {
				if ($params['params'] == true) {
					if (isset($params['callback_data'])) {
						$p->callback_data = $params['callback_data'];
					}
					if (isset($params['title_keyboard'])) {
						$p->title_keyboard = $params['title_keyboard'];
					}
				}
				if (isset($params['buttons'])) {
					$buttons = $params['buttons'];
				}
			}else{
				$buttons = 'callback_data';
			}
			if (!$p->callback_data) {
				$p->callback_data = 0;
			}
			$parts[] = array('text'=> json_decode('" '.$p->user_emoji.' "').$p->title_keyboard, $buttons => $p->callback_data);
		}
		return $parts;
	}
	public static function keyLocation($key)
	{
		foreach (TgKeyboard::get()->from()->where(['keyboard_group', '=', 0])->fetch() as $row) {
			return array(array('text' => json_decode('" '.$row['user_emoji'].' "').$row['title_keyboard'], 'request_location' => true));
		}
	}
}
