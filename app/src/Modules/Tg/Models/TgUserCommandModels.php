<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Tg\Models;

use app\Core\SelectPDOCore;

class TgUserCommandModels extends SelectPDOCore
{
    public static function Update($data=array()){
        $data = json_decode(json_encode($data));
        if(!self::get()->from()->where(['telegram_id', '=', $data->telegram_id])->fetch()){
            self::get()->getInsert(array('telegram_id' => $data->telegram_id));
        }
        self::get()->getUpdate(
            array(
                'command_name' => null,
                'command_step' => $data->command_step ?? null,
                'command_id' => null,
                'message_id' => null,
                'updated_at' => time()
            ),
            $data->telegram_id
        );
        return true;
    }

    public static function tableName()
    {
        return 'telegram_command';
    }
}