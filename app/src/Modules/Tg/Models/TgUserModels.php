<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Tg\Models;

use app\Core\SelectPDOCore;

class TgUserModels extends SelectPDOCore
{
    public static function Insert(array $data){
        self::get()->getInsert($data);
    }

    public static function tableName()
    {
        return 'telegram_user';
    }
}