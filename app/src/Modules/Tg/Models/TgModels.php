<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Tg\Models;

use app\Core\SelectPDOCore;

class TgModels extends SelectPDOCore
{
    public static function token($status = false, $token = null, $code = null){
        return self::get()->from()->where(['code', '=', $code])->limit(1)->fetch();
    }

    public static function tokenGiga($status = false, $token = null){
        if($status){
            self::get()->getUpdate(
                array(
                    'token' => $token
                ),
                1
            );
        }
        return self::get()->findOne(
            array('id' => 1)
        );
    }

    public static function tableName()
    {
        return 'telegram';
    }
}