<?php

declare(strict_types=1);

namespace app\Modules\Tg\Components;

use app\modules\telegram\models\TgModels;

class TgAPI
{

    public static function CompanyAPI( $code ) {
        $model = TgModels::get()->from()->where(['code', '=', $code])->limit(1)->fetch();
        if ($model) {
            return $model->token;
        }else{
            return null;
        }
    }

    public static function sendTelegram($method, $response, $code = null)
    {
        if($code == 'kassa'){
            $token = env('TELEGRAMBOTAPIKASSA');
        }else{
            $token = env('TELEGRAMBOTAPI');
        }
        $ch = curl_init('https://api.telegram.org/'.$token. '/' . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($res, true);

        return $result;
    }

    public static function file($file, $urlbot){

        $ch = curl_init('https://api.telegram.org/bot' . env('TELEGRAMBOTAPIKASSA') . '/getFile');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('file_id' => $file));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($res, true);
        if ($res['ok']) {
            $src  = 'https://api.telegram.org/file/bot' . env('TELEGRAMBOTAPIKASSA') . '/' . $res['result']['file_path'];
            return $src;
        }
    }
}