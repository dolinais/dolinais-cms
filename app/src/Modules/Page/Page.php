<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

/**
 * Module PAGE
 */

namespace app\Modules\Page;

use app\Controllers\ErrorController;
use app\Models\TreePageModels;
use app\Modules\Page\Controllers\PageController;

class Page
{
    public function __construct()
    {
        if(isset(\DolinaIS::getName()->Request[0])){
            $aliases = \DolinaIS::getName()->Request[0];
            $module = TreePageModels::get()->findOne(
                array(
                    'page_alias' => $aliases,
                    'page_status' => 1
                )
            )[0];

            if($module['page_type'] == "page_category"){
                if(!isset(\DolinaIS::getName()->Request[1])){
                    $className = [
                        'modules' => ['\app\Modules\Page\Controllers\\IndexPageController', 'actionIndex'],
                    ];
                }else{
                    $action = 'actionIndex';
                    if(isset(\DolinaIS::getName()->Request[2])){
                        $action = 'action'.ucfirst(\DolinaIS::getName()->Request[2]);
                    }
                    $className = [
                        'modules' => ['\app\Modules\Page\Controllers\\'.ucfirst(\DolinaIS::getName()->Request[1]).'PageController', $action],
                    ];
                }
                if(class_exists($className["modules"][0])){
                    $return = new $className["modules"][0];
                    if(method_exists($return, $className["modules"][1])){
                        $action = $className["modules"][1];
                        return $return->$action();
                    }else{
                        return new ErrorController();
                    }
                }
            }
            if($module['page_type'] == "page_news"){
                $className = [
                    'modules' => ['\app\Modules\Page\Controllers\\PagePageController', 'actionIndex'],
                ];
                $return = new $className["modules"][0];
                $action = $className["modules"][1];
                return $return->$action($module);
            }
            $action = 'actionIndex';
            if(isset(\DolinaIS::getName()->Request[1])){
                $action = 'action'.ucfirst(\DolinaIS::getName()->Request[1]);
            }
            $className = [
                'modules' => ['\app\Modules\Page\Controllers\\'.ucfirst(\DolinaIS::getName()->Request[0]).'PageController', $action],
            ];
        }

        if(class_exists($className["modules"][0])){
            $return = new $className["modules"][0];
            if(method_exists($return, $className["modules"][1])){
                $action = $className["modules"][1];
                return $return->$action();
            }else{
                $className = [
                    'modules' => ['\app\Modules\Page\Controllers\\'.ucfirst(\DolinaIS::getName()->Request[1]).'PageController', 'actionIndex'],
                ];
                $action = $className["modules"][1];
                return $return->$action();
            }
        }else{
            return new ErrorController();
        }
    }
}
