<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Models;

use app\Core\SelectPDOCore;

class СategoriesPageModels extends SelectPDOCore {

    public static function tableName()
    {
        return 'categories';
    }
}