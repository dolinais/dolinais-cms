<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Core\RedirectCore;
use app\Models\TreePageModels;

class CreatePageModels extends SelectPDOCore {

    public static function Create(array $data, array $files)
    {
        if (empty($data['TitlePage'])) {
            throw new \InvalidArgumentException('Не передан TitlePage');
        }


        if($data['UrlPage'] === ""){
            $data['UrlPage'] = Translit::en($data['TitlePage']);
        }

        if($data['StatusPage'] === 'on') {
            $status = 1;
        }else{
            $status = 0;
        }

        if(isset($_GET["id"])){
            $page_id = $_GET["id"];
        }else{
            $page_id = null;
        }

        $InsertPageId = TreePageModels::get()->getInsert(array(
            'page_title' => htmlspecialchars(strip_tags($data['TitlePage'])),
            'page_text' => htmlspecialchars(strip_tags($data['TextPost'])),
            'page_type' => htmlspecialchars(strip_tags($data['TypePageModels'])),
            'page_alias' => htmlspecialchars(strip_tags($data['UrlPage'])),
            'page_status' => $status,
            'page_id' => $page_id,
            'parent_id' => $page_id,
            'module_id' => TreePageModels::get()->findOne(
                array('id' => $page_id)
            )[0]["module_id"] ?? htmlspecialchars(strip_tags($data['ModulesModels'])),
            'page_sorting' => 100,
            'created_at' => time(),
            'updated_at' => time()
        ));

        if ($data['TypePageModels'] == 'page_news') {
            $InsertArticlesId = self::get()->getInsert(array(
                'title' => htmlspecialchars(strip_tags($data['TitlePage'])),
                'description' => htmlspecialchars(strip_tags($data['TextPost'])),
                'slug' => htmlspecialchars(strip_tags($data['UrlPage'])),
                'tree_id' => $InsertPageId,
                'articles_status' => $status,
                'created_at' => time(),
                'updated_at' => time()
            ));

            RedirectCore::to('/'.\DolinaIS::getName()->Request[0].'/update?id='.$InsertArticlesId);
        }
    }

    public static function tableName()
    {
        return 'articles';
    }
}