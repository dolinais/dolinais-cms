<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Core\RedirectCore;
use app\Models\FilesModels;

class PageModels extends SelectPDOCore {

    public static function Create(array $data, array $files)
    {
        if ($data['TypePageModels'] == 6) {
            var_dump($data['TypePageModels']);
        }
        exit;
        if (empty($data['TitlePost'])) {
            throw new \InvalidArgumentException('Не передан TitlePost');
        }

        if (!in_array($files["filePost"]['type'], array('image/gif', 'image/png', 'image/jpeg'))){
            throw new \InvalidArgumentException('Ошибка загрузки файла.');
        }

        if($data['StatusPost'] === 'on') {
            $status = 1;
        }else{
            $status = 0;
        }

        if (!move_uploaded_file($files["filePost"]['tmp_name'], UPLOADS . '/' . basename($files["filePost"]["name"]))) {
            throw new \InvalidArgumentException('Ошибка загрузки файла.');

        } else {
            $inputImagePath = UPLOADS . '/' . $files["filePost"]["name"];
            $outputImagePath = UPLOADS . '/thumbs/' . basename($files["filePost"]["name"]).'.webp';

            self::convertToWebp($inputImagePath, $outputImagePath, 80);

            if($data['UrlPage'] === ""){
                $data['UrlPage'] = Translit::en($data['TitlePost']);
            }

            $getInsert = self::get()->getInsert(array(
                'title' => htmlspecialchars(strip_tags($data['TitlePost'])),
                'description' => htmlspecialchars(strip_tags($data['TextPost'])),
                'categories_id' => htmlspecialchars(strip_tags($data['CategoryPost'])),
                'slug' => htmlspecialchars(strip_tags($data['UrlPage'])),
                'files_post' => basename($files["filePost"]["name"]),
                'status_page' => $status,
                'created_at' => time(),
                'updated_at' => time()
            ));
            FilesModels::get()->getInsert(array(
                'file_name' => htmlspecialchars(strip_tags($files["filePost"]["name"])),
                'file_title' => htmlspecialchars(strip_tags($files["filePost"]["name"])),
                'file_description' => htmlspecialchars(strip_tags($files["filePost"]["name"])),
                'file_page_id' => $getInsert,
                'file_status' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ));
            RedirectCore::to('/'.\DolinaIS::getName()->Request[0].'/update?id='.$getInsert);
        }
    }

    public static function Update(array $data, array $files, $id=null)
    {
        $total_files = count($files['filePost']['name']);
        for($key = 0; $key < $total_files; $key++) {
            if(isset($files['filePost']['name'][$key])
                && $files['filePost']['size'][$key] > 0) {

                $original_filename = $files['filePost']['name'][$key];

                $ext = pathinfo($original_filename, PATHINFO_EXTENSION);

                $filename_without_ext = basename($original_filename, '.'.$ext);

                $new_filename = str_replace(' ', '_', $filename_without_ext) . '_' . time() . '.' . $ext;

                if (move_uploaded_file($files['filePost']['tmp_name'][$key], UPLOADS . '/' . $new_filename)){
                    $inputImagePath = UPLOADS . '/' . $new_filename;
                    $outputImagePath = UPLOADS . '/' . basename($new_filename).'.webp';

                    self::convertToWebp($inputImagePath, $outputImagePath, 80);

                    FilesModels::get()->getInsert(array(
                        'file_name' => htmlspecialchars(strip_tags($new_filename)),
                        'file_title' => htmlspecialchars(strip_tags($new_filename)),
                        'file_description' => htmlspecialchars(strip_tags($new_filename)),
                        'file_page_id' => 1,
                        'file_status' => 1,
                        'created_at' => time(),
                        'updated_at' => time()
                    ));
                }
            }
        }

        if(isset($data['StatusPost']) && $data['StatusPost'] === 'on') {
            $status = 1;
        }else{
            $status = 0;
        }

        if($data['UrlPage'] === ""){
            $data['UrlPage'] = Translit::en($data['TitlePost']);
        }

        self::get()->getUpdate(
            array(
                'title' => htmlspecialchars(strip_tags($data['TitlePost'])),
                'description' => $data['TextPost'],
                'categories_id' => htmlspecialchars(strip_tags($data['CategoryPost'])),
                'slug' => htmlspecialchars(strip_tags($data['UrlPage'])),
                'files_post' => md5($data['TitlePost']).'.png',
                'articles_status' => $status,
                'updated_at' => time()
            ),
            $id
        );
    }

    public static function convertToWebp($inputImagePath, $outputImagePath, $quality = 80) {

        $imagick = new \Imagick($inputImagePath);
        $imagick->setImageFormat('WEBP');
        $imagick->setImageCompressionQuality($quality);
        $imagick->writeImage($outputImagePath);
        $imagick->clear();
        $imagick->destroy();
    }

    public static function tableName()
    {
        return 'articles';
    }
}