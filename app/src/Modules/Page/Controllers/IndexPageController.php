<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Controllers;

use app\Core\ControllerCore;
use app\Core\MobileCore;
use app\Models\FilesModels;
use app\Controllers\ErrorController;
use app\Modules\Page\Models\PageModels;

class IndexPageController extends ControllerCore{

    public function actionIndex()
    {
        // if(MobileCore::IsMobile()){
        //     var_dump('mobile');
        // }
        // var_dump(PageModels::get()->from()->where(['articles_status', '=', 1])->orderBy('created_at', 'DESC')->limit(20)->fetch());
        $post = PageModels::get()->from()->where(['articles_status', '=', 1])->orderBy('created_at', 'DESC')->limit(20)->fetch();
        $this->title = 'Статьи';
        $this->description = 'Лучшие статьи и новости только у нас!';
        $this->keywords = 'статьи, новости';

        $data = array(
            'title' => 'Статьи и новости',
            'description' => 'Читай с пользой!',
            'post' => PageModels::get()->findAll(),
            // 'post' => PageModels::get()->from()->where(['articles_status', '=', 1])->orderBy('created_at', 'ASC')->limit(20)->fetch(),
            $this
        );
        $this->rendercontroller(get_class($this), 'index', $data, 'assets');
    }
}