<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Controllers;

use app\Core\ControllerCore;
use app\Controllers\ErrorController;
use app\Core\RedirectCore;
use app\Models\ModulesModels;
use app\Models\TreePageModels;
use app\Models\TypePageModels;
use app\Modules\Page\Models\CreatePageModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Page\Models\СategoriesPageModels;

class CreatePageController extends ControllerCore{

    public function actionIndex()
    {
        if(!\DolinaIS::getName()->isGuest){
            RedirectCore::to('/account/auth');
        }
        if(\DolinaIS::getName()->isGuest){
            if (!empty($_POST)) {
                try {
                    $user = CreatePageModels::Create($_POST, $_FILES);
                } catch (\InvalidArgumentException $e) {
                    $e = $e->getMessage();
                }
            }
        }

        $this->title = 'Создать страницу';
        $this->description = 'Создать страницу';
        $this->keywords = 'Создать страницу';

        $new = array();
        foreach (TreePageModels::get()->findAll() as $a){
        $new[$a['page_id']][] = $a;
        }
        $tree = $this->createTree($new, array(TreePageModels::get()->findAll()[0]));
        // echo "<pre>";
        // print_r($tree);
        // echo "</pre>";

        $data = array(
            'title' => 'Создать страницу',
            'description' => '',
            'ModulesModels' => ModulesModels::get()->findAll(),
            'TypePageModels' => TypePageModels::get()->from()->orderBy('id', 'ASC')->limit(20)->fetch(),
            'TreePageModels' => $tree,
            'text' => null,
            'error' => $e ?? null,
            $this
        );

        $this->rendercontroller(get_class($this), 'index', $data, 'assets');
    }

    public function createTree(&$list, $parent){
        $tree = array();
        foreach ($parent as $k=>$l){
            if(isset($list[$l['id']])){
                $l['children'] = $this->createTree($list, $list[$l['id']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }
}