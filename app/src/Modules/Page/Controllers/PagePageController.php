<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Controllers;

use app\Core\ControllerCore;
use app\Components\DateTimeComponents;
use app\Models\TreePageModels;
use app\Models\ReviewsModels;
use app\Controllers\ErrorController;
use app\Modules\Page\Models\PageModels;

class PagePageController extends ControllerCore{

    public function actionIndex($module=null)
    {

        $new = array();
        foreach (ReviewsModels::get()->findOne(
                    array(
                        'page_id' => 5
                    )
                ) as $a){
        $new[$a['reply_id']][] = $a;
        }
        $tree = $this->createTree($new, array(ReviewsModels::get()->findOne(
                    array(
                        'page_id' => 5
                    ))[0]));

        // echo "<pre>";
        // print_r($tree);
        // echo "</pre>";
        if(!$module){
            return new ErrorController();
        }
        $page = PageModels::get()->findOne(
            array('tree_id' => $module["id"])
        );
        if(!$page){
            return new ErrorController();
        }

        $output = json_decode(json_encode(PageModels::get()->findOne(
            array('tree_id' => $module["id"])
        )[0]));

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            foreach(ReviewsModels::get()->from()->where(['page_id', '=', $output->tree_id])->orderBy('id', 'ASC')->limit(20)->fetch() as $ReviewsModels){
                if($ReviewsModels->reply_id){
                    $ReviewsPX = 48;
                }else{
                    $ReviewsPX = 0;
                }
                echo '<div class="card mb-3">
                    <div class="card-body">
                        <div class="d-flex flex-start">
                            <img class="rounded-circle shadow-1-strong me-3"
                                src="/uploads/avatarg.jpg" alt="avatar" width="60"
                                height="60" />
                            <div>
                                <h6 class="fw-bold mb-1">Гость</h6>
                                <div class="d-flex align-items-center mb-3">
                                    <p class="mb-0" style="font-size: 12px;">
                                        '. DateTimeComponents::T($ReviewsModels->created_at, 5).'
                                    </p>
                                    <a href="#!'.$ReviewsModels->id.'" class="link-muted"><i class="fas fa-pencil-alt ms-2"></i></a>
                                    <a href="#!'.$ReviewsModels->id.'" class="link-muted"><i class="fas fa-redo-alt ms-2"></i></a>
                                    <a href="#!'.$ReviewsModels->id.'" class="link-danger"><i class="fas fa-heart ms-2"></i> ('.$ReviewsModels->reviews_like.')</a>
                                </div>
                                <p class="mb-0">
                                    '.$ReviewsModels->message.'
                                </p>
                            </div>
                        </div>
                    </div>
                </div>';
            }
            exit;
        }


        foreach($page as $page){
            $this->title = $output->title;
            $this->description = $page["m_description"];
            $this->keywords = $page["m_keywords"];
            $data = array(
                'title' => $page["title"],
                'description' => $page["description"],
                'id' => $page["id"],
                'img' => $page["files_post"],
                'articles_like' => $page["articles_like"],
                'ReviewsModels' => ReviewsModels::get()->from()->where(['page_id', '=', $page["tree_id"]])->orderBy('id', 'ASC')->limit(20)->fetch(),
                $this
            );
        }

        $this->rendercontroller(get_class($this), 'index', $data, 'assets');
    }

    public function createTree(&$list, $parent){
        $tree = array();
        foreach ($parent as $k=>$l){
            if(isset($list[$l['id']])){
                $l['children'] = $this->createTree($list, $list[$l['id']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }
}