<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Controllers;

use app\Controllers\ErrorController;
use app\Models\FilesModels;
use app\Modules\Page\Models\PageModels;

class ViewsPageController{

    public function actionIndex(){

        header('Content-Type: application/json');
        if(isset($_GET["id"])){
            foreach(FilesModels::get()->findOne(
                array(
                    'file_page_id' => $_GET["id"]
                )
                ) as $filename){
                if(!$filename){
                    $initialPreview = [];
                    $initialPreviewConfig = [];
                }else{
                    $initialPreview[] = '/uploads/thumbs/'.$filename['file_name'].'.webp';
                    $initialPreviewConfig[] = [
                        'caption' => $filename['file_name'],
                        'url' => '/'.\DolinaIS::getName()->Request[0].'/files',
                        'key' => $filename['id'],
                        'extra' => ['id' => $filename['file_name']],
                        'width' => '120px',
                    ];
                }
            }
        }
        $result = [
            "append" => true,
            "initialPreview" => $initialPreview ?? '',
            'initialPreviewConfig' => $initialPreviewConfig ?? ''
        ];
        echo json_encode($result);
    }
}