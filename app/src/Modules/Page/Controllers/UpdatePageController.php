<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Controllers;

use app\Core\ControllerCore;
use app\Core\RedirectCore;
use app\Models\TypePageModels;
use app\Controllers\ErrorController;
use app\Modules\Page\Models\PageModels;
use app\Modules\Page\Models\СategoriesPageModels;

class UpdatePageController extends ControllerCore
{

    public function actionIndex()
    {
        if(!\DolinaIS::getName()->isGuest){
            RedirectCore::to('/account/auth');
        }
        if(isset($_GET["tree_id"])){
            RedirectCore::to('/page/update?id='.PageModels::get()->findOne(
                array('tree_id' => $_GET["tree_id"])
            )[0]["id"]);
        }

        if(isset($_GET["id"]))
        {
            $id = $_GET["id"];
        }else
        {
            RedirectCore::to('/page');
        }

        if(\DolinaIS::getName()->isGuest())
        {
            if (!empty($_POST)) {
                try {
                    $user = PageModels::Update($_POST, $_FILES, $id);
                } catch (\InvalidArgumentException $e) {
                    $e = $e->getMessage();
                }
            }
        }

        $PageModels = PageModels::get()->findOne(
            array('id' => $id)
        );
        if(!$PageModels){
            RedirectCore::to('/page');
        }
        $output = json_decode(json_encode($PageModels[0]));

        $this->title = $output->title;
        $this->description = $output->title;
        $this->keywords = $output->title;

        $data = array(
            'title' => $PageModels[0]["title"],
            'description' => '',
            'СategoriesPageModels' => СategoriesPageModels::get()->findAll(),
            'TypePageModels' => TypePageModels::get()->findAll(),
            'value' => $PageModels[0],
            'text' => null,
            'error' => $e ?? null,
            $this
        );

        $this->rendercontroller(get_class($this), 'index', $data, 'assets');
    }
}