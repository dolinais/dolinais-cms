<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Page\Controllers;

use app\Core\RedirectCore;
use app\Models\FilesModels;
use app\Controllers\ErrorController;
use app\Modules\Page\Models\PageModels;

class FilesPageController
{

    public function actionIndex()
    {
        header('Content-Type: application/json');
        $outData = self::upload();
        echo json_encode($outData);
        exit();
    }

    public static function upload()
    {
        if (!empty($_POST) && !empty($_POST['key'])) {
            FilesModels::get()->getDelete(
                array(
                    'id' => $_POST['key']
                )
            );
            if (unlink(UPLOADS.'/'.$_POST['id']) && unlink(UPLOADS.'/thumbs/'.$_POST['id'].'.webp')){
                return true;
            }else{
                return [
                    'error' => 'Error'
                ];
            }
        }
        $preview = $config = $errors = [];
        $targetDir = UPLOADS;

        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        $fileBlob = 'fileBlob';

        if (isset($_FILES[$fileBlob])) {

            $file = $_FILES[$fileBlob]['tmp_name'];  // the path for the uploaded file chunk
            $fileName = $_POST['fileName'];          // you receive the file name as a separate post data
            $fileSize = $_POST['fileSize'];          // you receive the file size as a separate post data
            $fileId = $_POST['fileId'];              // you receive the file identifier as a separate post data
            $index =  $_POST['chunkIndex'];          // the current file chunk index
            $totalChunks = $_POST['chunkCount'];     // the total number of chunks for this file
            $targetFile = $targetDir.'/'.$fileName;  // your target file path

            if(move_uploaded_file($file, $targetFile)) {

                $inputImagePath = UPLOADS . '/' . $fileName;
                $outputImagePath = UPLOADS . '/thumbs/' . $fileName.'.webp';

                self::convertToWebp($inputImagePath, $outputImagePath, 80);
                FilesModels::get()->getInsert(array(
                    'file_name' => htmlspecialchars(strip_tags($fileName)),
                    'file_title' => htmlspecialchars(strip_tags($fileName)),
                    'file_description' => htmlspecialchars(strip_tags($fileName)),
                    'file_page_id' => $_GET["id"],
                    'file_status' => 1,
                    'created_at' => time(),
                    'updated_at' => time()
                ));
                return [
                    'chunkIndex' => $index,
                    'initialPreview' => '/uploads/thumbs/' . $fileName.'.webp',
                    'initialPreviewConfig' => [
                        [
                            'type' => 'image',
                            'caption' => $fileName,
                            'key' => $fileId,
                            'fileId' => $fileId,
                            'size' => $fileSize,
                            'zoomData' => '/uploads/' . $fileName,
                        ]
                    ],
                    'append' => true
                ];
            } else {
                return [
                    'error' => 'Error uploading chunk ' . $_POST['chunkIndex']
                ];
            }
        }
        return [
            'error' => 'No file found'
        ];
    }

    public static function convertToWebp($inputImagePath, $outputImagePath, $quality = 80)
    {

        $imagick = new \Imagick($inputImagePath);
        $imagick->setImageFormat('WEBP');
        $imagick->setImageCompressionQuality($quality);
        $imagick->writeImage($outputImagePath);
        $imagick->clear();
        $imagick->destroy();
    }
}