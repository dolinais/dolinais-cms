<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Account\Models;

use app\Core\SelectPDOCore;
use app\Core\RedirectCore;

class AccountModels extends SelectPDOCore
{
    public static function signUp(array $userData)
    {
        if (empty($userData['logout'])) {
            self::logout();
        }

        if (empty($userData['username'])) {
            throw new \InvalidArgumentException('Укажите номер телефона в формате +79000000000 или 89000000000');
        }

        $username = self::Phone($userData['username']);

        if (empty($userData['password'])) {
            throw new \InvalidArgumentException('Неправильно указан пароль');
        }
        if(strlen($userData['password']) < 5){
            throw new \InvalidArgumentException('Неправильно указан пароль, укажите пароль не менее 6 символов');
        }

        if($auth_key = self::user($username,$userData['password'])) {
            setcookie('PHPSESSID', $auth_key['auth_key'], strtotime('+30 days'), '/');
            RedirectCore::to('/account');
        }else{
            if(self::get()
            ->findOne(
                array(
                    'username' => $username
                )
            )){
                throw new \InvalidArgumentException('Неверный логин или пароль!');
            }else{
                self::get()->getInsert(array(
                    'username' => $username,
                    'phone' => $username,
                    'password_hash' => password_hash($userData['password'], PASSWORD_DEFAULT),
                    'auth_key' => $auth_key = md5(password_hash($userData['password'], PASSWORD_DEFAULT)),
                    'accessToken' => md5(password_hash($userData['password'], PASSWORD_DEFAULT)),
                    'email' => NULL,
                    'created_at' => time(),
                    'updated_at' => time()
                ));
                setcookie('PHPSESSID', $auth_key, strtotime('+30 days'), '/');
                RedirectCore::to('/account');
            }
        }
    }

    public static function Phone($phoneNumber)
    {
        $phoneNumber = preg_replace('/\s|\+|-|\(|\)/','', $phoneNumber); // удалим пробелы, и прочие не нужные знаки

        if(is_numeric($phoneNumber))
        {
            if(strlen($phoneNumber) < 5) // если длина номера слишком короткая, вернем false
            {
                throw new \InvalidArgumentException('Ошибка номера телефона');
            }
            else
            {
                return $phoneNumber;
            }
        }
        else
        {
            return FALSE;
        }
    }

    public static function logout(){
        if (isset($_COOKIE['PHPSESSID'])) {
            unset($_COOKIE['PHPSESSID']);
            setcookie('PHPSESSID', '', -1, '/');
            RedirectCore::to('/account/auth');
            return true;
        } else {
            return false;
        }
    }

    public static function userId(){
        if (isset($_COOKIE['PHPSESSID'])) {
            if($userid = self::get()
                ->findOne(
                    array(
                        'auth_key' => $_COOKIE['PHPSESSID']
                    )
                )
            ){
                return $userid[0]['id'];
            }else{
                return false;
            }
        }
    }

    public static function isUserIDJS($params)
    {
        if (isset($params)) {
            $user = self::get()
            ->findOne(
                array(
                    'auth_key' => $params
                )
            );
            if(!isset($user[0])){
                return null;
            }else{
                return $user[0]['id'];
            }
        }
        return null;
    }

    public static function isUserAccessToken($accessToken)
    {
        if (isset($accessToken)) {
            $user = self::get()
            ->findOne(array(
                'accessToken' => $accessToken
            ));

            if(!isset($user[0])){
                return false;
            }

            return true;
        }
    }

    public static function InfoUserAccessToken($accessToken)
    {
        if (isset($accessToken)) {
            $user = self::get()
            ->findOne(array(
                'accessToken' => $accessToken
            ));

            if(!isset($user[0])){
                return false;
            }

            return $user[0]["id"];
        }
    }

    public static function CreateCode($accessToken)
    {
        if (isset($accessToken)) {
            $user = self::get()
            ->findOne(array(
                'accessToken' => $accessToken
            ));

            if(!isset($user[0])){
                return false;
            }

            $UID = $user[0]["id"];
            $code = rand(1000000, 9999999);

            if(count(self::get()
                ->findOne(array(
                    'code' => $code
                ))) > 1){
                return self::CreateCode($accessToken);
            }

            if($tguser = \app\Modules\Tg\Models\TgUserModels::get()
            ->findOne(array(
                'user_id' => $UID
            ))){
                if($tguser[0]["status"] == 1){
                    return 200;
                }
            }

            self::get()->getUpdate(
            array(
                'code' => $code,
                'updated_at' => time()
            ),
            $UID
        );

            return $code;
        }
    }

    public static function UserList($accessToken)
    {
        if (isset($accessToken)) {
            $user = self::get()
            ->findOne(array(
                'status' => 1
            ));

            if(!isset($user)){
                return false;
            }

            return $user;
        }
    }

    public static function InfoUserInAuchToken($accessToken)
    {
        if (isset($accessToken)) {
            $user = self::get()
            ->findOne(
                array(
                    'accessToken' => $accessToken
                )
            );
            if(!isset($user[0])){
                return array();
            }else{
                return $user;
            }
        }
        return null;
    }

    public static function isUserPerson($id)
    {
        if (isset($id)) {
            $user = self::get()
            ->findOne(
                array(
                    'id' => $id
                )
            );
            if(!isset($user[0])){
                return null;
            }else{
                return $user[0];
            }
        }
        return null;
    }

    public static function user($username, $password){
        if($userid = self::get()
            ->findOne(
                array(
                    'username' => $username
                )
            )
        ){
            if(password_verify($password, $userid[0]['password_hash'])){
                return $userid[0];
            }
        }
    }

    public static function tableName()
    {
        return 'account';
    }
}