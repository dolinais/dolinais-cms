<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Account\Models;

use app\Core\SelectPDOCore;
use app\Core\RedirectCore;

class AccountAuthHistoryModels extends SelectPDOCore
{
    public static function history($userData=null, $auth_status=null)
    {
        self::get()->getInsert(array(
            'user_id' => $userData,
            'auth_status' => $auth_status,
            'created_at' => time(),
            'updated_at' => time()
        ));
        return true;
    }

    public static function tableName()
    {
        return 'account_auth_history';
    }
}