<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Account\Controllers;

use app\Core\ControllerCore;
use app\Core\RedirectCore;
use app\Controllers\ErrorController;
use app\Modules\Account\Models\AccountModels;

class AuthAccountController extends ControllerCore
{

    public function actionIndex()
    {
        if(isset(($_POST["logout"]))){
            if (!empty($_POST)) {
                try {
                    $user = AccountModels::signUp($_POST);
                } catch (\InvalidArgumentException $e) {
                    $e = $e->getMessage();
                }
            }
        }
        if(!\DolinaIS::getName()->isGuest){
            if (!empty($_POST)) {
                try {
                    $user = AccountModels::signUp($_POST);
                } catch (\InvalidArgumentException $e) {
                    $e = $e->getMessage();
                }
            }
        }else{
            RedirectCore::to('/account');
        }

        $this->title = 'Личный кабинет';
        $this->description = 'Авторизуйтесь чтобы продолжить пользоваться сервисом.';
        $this->keywords = 'Личный кабинет';

        $data = array(
            'title' => 'Личный кабинет',
            'description' => 'Авторизуйтесь чтобы продолжить пользоваться сервисом.',
            'error' => $e ?? null,
            $this
        );

        $this->rendercontroller(get_class($this), 'index', $data, 'assets');
    }
}