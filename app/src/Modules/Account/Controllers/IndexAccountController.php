<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Account\Controllers;

use app\Core\ControllerCore;
use app\Modules\Page\Models\PageModels;
use app\Controllers\ErrorController;
use app\Modules\Account\Models\AccountModels;
use app\Core\RedirectCore;

class IndexAccountController extends ControllerCore
{

    public function actionTest()
    {
        echo 1;
    }
    public function actionIndex()
    {
        $this->title = 'Личный кабинет';
        $this->description = '';
        $this->keywords = '';

        if(!\DolinaIS::getName()->isGuest){
            RedirectCore::to('/account/auth');
        }else{
            $cabinet = 'Здесь будут храиться все ваши данные';
        }

        $data = array(
            'title' => 'Личный кабинет',
            'description' => $cabinet,
            $this
        );

        $this->rendercontroller(get_class($this), 'index', $data, 'assets');
    }
}