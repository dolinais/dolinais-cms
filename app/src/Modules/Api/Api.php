<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

/**
 * Module Api
 */

namespace app\Modules\Api;

use app\Controllers\ErrorController;
use app\Modules\Api\Controllers\ApiController;

class Api
{
    public function __construct()
    {
        if(!isset(\DolinaIS::getName()->Request[1])){
            $className = [
                'modules' => ['\app\Modules\Api\Controllers\IndexApiController', 'actionIndex'],
            ];
        }else{
            $action = 'actionIndex';
            if(isset(\DolinaIS::getName()->Request[2])){
                $action = 'action'.ucfirst(\DolinaIS::getName()->Request[2]);
            }

            $className = [
                'modules' => ['\app\Modules\Api\Controllers\\'.ucfirst(\DolinaIS::getName()->Request[1]).'ApiController', $action],
            ];
        }

        if(class_exists($className["modules"][0])){
            $return = new $className["modules"][0];
            if(method_exists($return, $className["modules"][1])){
                $action = $className["modules"][1];
                return $return->$action();
            }else{
                return new ErrorController('Метод: <code>'.\DolinaIS::getName()->Request[2].'</code> не существует');
            }
        }else{
            new ErrorController();
        }
    }
}
