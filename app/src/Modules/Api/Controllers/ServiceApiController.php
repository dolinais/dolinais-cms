<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Modules\Api\Services\RequstService;
use app\Modules\Api\Services\CommandsServiceApi;
use app\Modules\Api\Controllers\AuthApiController;

class ServiceApiController
{
    public function actionIndex()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        $headers = getallheaders();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            if (isset($headers['authorization']) && substr($headers['authorization'], 0, 6) !== 'Bearer') {
                echo json_encode(['status' => 401, "error" => "Bearer keyword is missing"]);
                exit;
            }

            if (isset($headers['authorization'])){
                if(!AuthApiController::auth(trim(substr($headers['authorization'], 6)))){
                    echo json_encode([
                        "code" => 401,
                        "error" => "Bearer keyword is missing"]);
                    exit;
                }
            }

            echo json_encode(array(
                'status' => 200,
                'output' => CommandsServiceApi::startMessage(new RequstService())
            ));
            return;
        }else{
            echo json_encode([
                "code" => 401,
                "error" => "Bearer keyword is missing"]);
            exit;
        }
    }
}