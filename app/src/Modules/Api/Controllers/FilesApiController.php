<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Models\ReviewsModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Api\Services\RequstService;

class FilesApiController
{
    public function actionIndex()
    {
        $filename = $_GET['file'];

        if(ini_get('zlib.output_compression'))
            ini_set('zlib.output_compression', 'Off');

        $file_extension = strtolower(substr(strrchr($filename,"."),1));

        if( $filename == "" )
        {
            echo "<html><title>eLouai's Download Script</title><body>ERROR: download file NOT SPECIFIED. USE force-download.php?file=filepath</body></html>";
            exit;
        } elseif ( ! file_exists( $filename ) )
        {
            echo "<html><title>eLouai's Download Script</title><body>ERROR: File not found. USE force-download.php?file=filepath</body></html>";
            exit;
        };

        switch( $file_extension )
        {
            case "txt": $ctype = "text/plain"; break;
            case "xml": $ctype = "text/xml; charset=utf-8"; break;
            case "pdf": $ctype = "application/pdf"; break;
            case "exe": $ctype = "application/octet-stream"; break;
            case "zip": $ctype = "application/zip"; break;
            case "doc": $ctype = "application/msword"; break;
            case "xls": $ctype = "application/vnd.ms-excel"; break;
            case "ppt": $ctype = "application/vnd.ms-powerpoint"; break;
            case "gif": $ctype = "image/gif"; break;
            case "png": $ctype = "image/png"; break;
            case "jpeg":
            case "jpg": $ctype = "image/jpg"; break;
            default: $ctype = "application/octet-stream";
        }
        // header("Pragma: public"); // required
        // header("Expires: 0");
        // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        // header("Cache-Control: private",false); // required for certain browsers
        // header("Content-Type: $ctype");
        // // change, added quotes to allow spaces in filenames, by Rajkumar Singh
        // header("Content-Disposition: attachment; filename=\"".basename($filename)."\";" );
        // header("Content-Transfer-Encoding: binary");
        // header("Content-Length: ".filesize($filename));
        // readfile("$filename");

        $out  = "User-agent: *" . PHP_EOL;
        $out .= "Disallow: /" . PHP_EOL;
        $out .= "User-agent: Yandex" . PHP_EOL;
        $out .= "Host: https://".$_SERVER['HTTP_HOST'].PHP_EOL;
        $out .= "Sitemap: https://".$_SERVER['HTTP_HOST']."/sitemap.xml";

        header("Content-Type: $ctype");
        echo $out;

        exit();
    }
}