<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Models\ReviewsModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Api\Services\RequstService;
use app\Modules\Account\Models\AccountModels;
use app\Modules\Account\Models\AccountAuthHistoryModels;

class AuthApiController
{
    public function actionIndex()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            echo json_encode(["status" => "401"]);
            return;
        }

        $output = new RequstService();

        $headers = getallheaders();

        if (isset($headers['Authorization']) && substr($headers['Authorization'], 0, 5) !== 'Basic') {
            echo json_encode(["error" => "Basic keyword is missing"]);
            return;
        }

        if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])){

            $user = AccountModels::user($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
            if(isset($user["accessToken"])){
                AccountAuthHistoryModels::history($user["id"], 1);
                echo json_encode(array(
                    'status' => 200,
                    "accessToken" => $user["accessToken"]
                ));
                return;
            }else{
                echo json_encode([
                    'status' => 401,
                    "error" => "Error accessToken"
                ]);
                return;
            }
        }
    }

    public static function auth($accessToken){
        return AccountModels::isUserAccessToken($accessToken);
    }
}