<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Models\ReviewsModels;
use app\Models\MessengerModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Api\Services\RequstService;
use app\Modules\Account\Models\AccountModels;

class NodeApiController
{
    public function actionIndex()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        if($_SERVER['REQUEST_METHOD'] === 'POST')

        $data = json_decode(file_get_contents("php://input"));

        if(!isset($data->postData)){
            return true;
        }

        $postData = json_decode($data->postData);

        if(!isset($postData->method)){
            return true;
        }

        if(isset($postData->method) && isset($postData->sessionID) && $postData->method === 'onStatus') {
            if($userID = AccountModels::isUserIDJS($postData->session)){
                echo json_encode(array(
                    'code' => 200,
                    'method' => 'onStatus',
                    'sessionID' => $postData->session,
                    'pid' => json_decode($data->pid),
                    'user_id' => $userID,
                    'onStatus' => 1,
                ));
                return;
            }
        }

        if(isset($postData->method) && isset($postData->session) && $postData->method === 'onOpen') {
            if($userID = AccountModels::isUserIDJS($postData->session)){
                echo json_encode(array(
                    'code' => 200,
                    'method' => 'onOpen',
                    'sessionID' => $postData->session,
                    'pid' => json_decode($data->pid),
                    'user_id' => $userID,
                ));
                AccountModels::get()->getUpdate(array(
                    'sonline' => 1,
                    'updated_at' => time()
                ),$userID);
                return;
            }
        }

        if(isset($postData->method) && isset($postData->sessionID) && $postData->method === 'onClose') {
            if($userID = AccountModels::isUserIDJS($postData->sessionID)){
                echo json_encode(array(
                    'code' => 200,
                    'method' => 'onClose',
                    'sessionID' => $postData->sessionID,
                    'pid' => $postData->pid,
                    'user_id' => $userID,
                ));
                AccountModels::get()->getUpdate(array(
                    'sonline' => 0,
                    'updated_at' => time()
                ),$userID);
                return;
            }
        }

        if(isset($postData->method) && isset($postData->session) && $postData->method === 'TypeUser') {
            if($userID = AccountModels::isUserIDJS($postData->session)){
                echo json_encode(array(
                    'code' => 200,
                    'method' => 'TypeUser',
                    'sessionID' => $postData->session,
                    'pid' => json_decode($data->pid),
                    'user_id' => $userID,
                ));
                return;
            }
        }

        if(isset($postData->method) && isset($postData->session) && $postData->method === 'messages') {
            if($userID = AccountModels::isUserIDJS($postData->session)){
                $MessengerModels = MessengerModels::get()->getInsert(array(
                    'user_id' => $userID,
                    'recipient_id' => $postData->userTo,
                    'message' => htmlspecialchars(strip_tags($postData->message)),
                    'avatar' => 'https://laborcolor.ru/wp-content/uploads/f/a/d/fadda6d67d084b4c92424b65d084d144.jpeg',
                    'created_at' => time()
                ));
                if(mb_strlen($postData->message) < 478){
                    $text = htmlspecialchars(strip_tags($postData->message));
                }else{
                    $text = 'Максимальная длинна текста 470 символов!';
                }
                echo json_encode(array(
                    'code' => 200,
                    'method' => 'messages',
                    'pid' => json_decode($data->pid),
                    'sessionID' => $postData->session,
                    'user_id' => $userID,
                    'user' => AccountModels::isUserPerson($userID)['first_name'],
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'text' => $text,
                    'date' => date('H:i'),
                    'dateTest' => strtotime(date("Y-m-d", strtotime("-30 minutes"))),
                    'messagenew' => rand(1,5),
                    'messageID' => time()
                ));
            }
        }
    }
}

// $oldTime = strtotime(date("H:i"));
// $newTime = date("H:i", strtotime('+10 minutes', $oldTime));

// if (date('H:i') > date("H:i", '1698160029')) {
//     echo date('i') - date("i", '1698160029').' минут назад<br>';
//     echo date("i", '1698160029');
// }