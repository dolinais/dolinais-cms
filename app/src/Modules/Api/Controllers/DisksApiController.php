<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Models\ReviewsModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Api\Services\RequstService;

class DisksApiController
{
    public function actionIndex()
    {
        if(php_uname('s')=='Windows NT'){
            // windows
            $disks = exec('fsutil fsinfo drives');
            $disks = explode(' ', $disks);
            unset($disks[0]);

            foreach($disks as $key => $disk) {
                $disks[$key] = $disk;
            }

            return $disks;
        }else{
            $data = exec('mount');
            $data = explode(' ', $data);

            $disks = [];
            foreach($data as $token) {
                if(substr($token,0,5) == '/dev/') {
                    $disks[] = $token;
                }
            }

            return $disks;
        }
    }
}