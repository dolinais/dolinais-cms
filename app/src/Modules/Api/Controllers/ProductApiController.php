<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Models\ReviewsModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Api\Services\RequstService;
use app\Modules\Api\Services\CommandsServiceApi;
use app\Modules\Account\Models\AccountModels;
use app\Modules\Shop\Models\ShopModels;

class ProductApiController
{
    public function actionIndex()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            echo json_encode(["status" => "401"]);
            return;
        }

        // $data = new RequstService(123);
        // echo json_encode($data->getTypeClass());
        // exit;

        $headers = getallheaders();

        if (isset($headers['Authorization']) && substr($headers['Authorization'], 0, 7) !== 'Bearer ') {
            echo json_encode(["error" => "Bearer keyword is missing"]);
            return;
        }

        if (isset($headers['Authorization'])){
            $token = trim(substr($headers['Authorization'], 6));
            if(!AccountModels::isUserAccessToken($token)){
                echo json_encode([
                    "code" => 401,
                    "error" => "Bearer keyword is missing"]);
                return;
            }
            echo json_encode([
                "code" => 200,
                "output" => CommandsServiceApi::startMessage(new RequstService($token)),
                "hash" => md5(rand())
            ]);
            return;
        }
    }
}