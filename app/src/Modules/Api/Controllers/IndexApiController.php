<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Models\ReviewsModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Api\Services\RequstService;
use app\Modules\Tg\Models\TgModels;

class IndexApiController
{
    function getcontents($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    public function actionIndex()
    {
        if(isset($_GET["code"])){
            $dom = new \DomDocument();
            @$dom->loadHTML($this->getcontents("https://service-online.su/text/shtrih-kod/?cod=".$_GET["code"]));
            $h2s = $dom->getElementsByTagName('title');
            foreach( $h2s as $h2 ) {
                echo str_replace("Штрихкод ".$_GET["code"]." - ", "",$h2->textContent);
            }
            return;
        }

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // echo sprintf("Имя: %s, Телефон: %s", 792, 123);
         // throw new \InvalidArgumentException('Не передан nickname');

        $output = new RequstService();

        $headers = getallheaders();

        if (isset($headers['Authorization']) && substr($headers['Authorization'], 0, 7) !== 'Bearer ') {
            // echo json_encode(["error" => "Bearer keyword is missing"]);
            echo json_encode(array(
                'code' => 200,
                'data' => PageModels::get()->findAll()
            ));
            exit;
        }

        if (isset($headers['Authorization'])){
            $token = trim(substr($headers['Authorization'], 6));
            if($token != 123){
                echo json_encode([
                    "code" => 401,
                    "error" => "Bearer keyword is missing"]);
                exit;
            }
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            echo json_encode(array(
                'code' => 200,
                "status" => 'Мы получили ваше сообщение, скоро с Вами свяжемся! ',
                'post' => $output->getOrderID()
            ));
            return;
        }
    }
}