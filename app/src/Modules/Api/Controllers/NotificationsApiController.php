<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Models\ReviewsModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Api\Services\RequstService;

class NotificationsApiController
{
    public function actionIndex()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        $params = json_decode(file_get_contents("php://input"), true);
        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            $message = $params;
        }else{
            $message = 'Null';
        }
        echo json_encode(array(
            'code' => 200,
            'text' => $message,
            'date' => date('d.m.Y H:i'),
            'userid' => rand(1,100).'-test'
        ));
    }
}