<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Controllers;

use app\Models\ReviewsModels;
use app\Modules\Page\Models\PageModels;
use app\Modules\Api\Services\RequstService;

class CommentApiController
{
    public function actionIndex()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        if(!empty($_POST["comment"]) && !empty($_POST["page_id"])){
            $page = PageModels::get()->findOne(
                array('slug' => $_POST["page_id"])
            );
            if(!$page){
                $message = '<label class="text-danger">Error: Comment not posted.</label>';
                $status = array(
                    'error'  => 1,
                    'message' => $message
                );
                echo json_encode($status);
            }
            ReviewsModels::get()->getInsert(array(
                'user_id' => 1,
                'message' => htmlspecialchars(strip_tags($_POST["comment"])),
                'page_id' => $page[0]["tree_id"],
                'reviews_like' => rand(1,100),
                'created_at' => time()
            ));
            $message = '<label class="text-success">Опубликовали</label>';
            $status = array(
                'error'  => 0,
                'message' => $message
            );
        } else {
            $message = '<label class="text-danger">Error: Comment not posted.</label>';
            $status = array(
                'error'  => 1,
                'message' => $message
            );
        }
        echo json_encode($status);
        return;
    }
}