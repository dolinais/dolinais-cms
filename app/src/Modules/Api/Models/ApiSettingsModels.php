<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Models;

use app\Core\SelectPDOCore;
use app\Modules\Account\Models\AccountModels;

class ApiSettingsModels extends SelectPDOCore
{

    public static function Update($data, $token=null){
        if(isset($data->params->values->params) && $data->params->values->params == 'password'){
            $passwordToken = md5(password_hash($data->params->values->password, PASSWORD_DEFAULT));
            AccountModels::get()->getUpdate(
                array(
                    'password_hash' => password_hash($data->params->values->password, PASSWORD_DEFAULT),
                    'auth_key' => $passwordToken,
                    'accessToken' => $passwordToken,
                    'updated_at' => time()
                ),
                AccountModels::InfoUserAccessToken($token)
            );
            return AccountModels::InfoUserInAuchToken($passwordToken)[0];
        }
        AccountModels::get()->getUpdate(
            array(
                'last_name' => htmlspecialchars(strip_tags($data->params->values->last_name ?? null)),
                'first_name' => htmlspecialchars(strip_tags($data->params->values->first_name ?? null)),
                'patronymic' => htmlspecialchars(strip_tags($data->params->values->patronymic ?? null)),
                'birthday' => strtotime($data->params->values->birthday),
                'updated_at' => time()
            ),
            AccountModels::InfoUserAccessToken($token)
        );
        return true;
    }

    public static function tableName()
    {
        return 'settings';
    }
}