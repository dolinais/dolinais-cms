<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Models;

use app\Core\SelectPDOCore;

class ApiModels extends SelectPDOCore
{

    public static function tableName()
    {
        return 'api';
    }
}