<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Models;

use app\Core\SelectPDOCore;
use app\Modules\Account\Models\AccountModels;

class ApiNotificationModels extends SelectPDOCore
{
    public static function Create($data=array())
    {
        $ApiNotificationModels = self::get()->getInsert(array(
            'tiket_id' => md5($data['user_id'].time()),
            'title' =>$data['title'],
            'text' =>$data['text'],
            'user_id' => $data['user_id'],
            'created_at' => time()
        ));
        return $ApiNotificationModels;
    }

    public static function Update($data, $token=null){
        $ShopBasketModels = self::get()->getUpdate(
            array(
                'id' => htmlspecialchars(strip_tags($data->params->values->id ?? null)),
                'status' => 1,
                'updated_at' => time()
            ),
            $data->params->values->id
        );
        return $data->params->values->id;
    }

    public static function AllInfo($data, $token=null){
        $userId = AccountModels::InfoUserAccessToken($token);
        if($notification = self::get()->from()->where(['user_id', '=', $userId])->orderBy('id', 'ASC')->fetch()){
            return $notification;
        }
    }
    public static function Info($data, $token=null)
    {
        $userId = AccountModels::InfoUserAccessToken($token);
        $notification = self::get()->from()->where(['user_id', '=', $userId, "AND", 'status', '=', 0])->orderBy('id', 'ASC')->fetch();
        if(!$notification){
            return array(
                "status" => 1,
                "infocount" => 0,
                "title" => '',
                "text" => ''
            );
        }

        $data = array(
            'user_id'  => $userId,
            'status' => 0
        );
        $infocount = 0;
        foreach($notification
        as $notification){
            $id = $notification->id;
            $infocount++;
            $status = $notification->status;
            $title = $notification->title;
            $text = $notification->text;

        }
        if(isset($id)){
            return array(
                "notificationid" => $id,
                "status" => $status,
                "infocount" => $infocount,
                "title" => $title,
                "text" => $text
            );
        }
    }

    public static function tableName()
    {
        return 'notification';
    }
}