<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Services;

class RequstService
{
    public $data;
    public $token;

    /**
     * Constructor.
     */
    public function __construct($token=null)
    {
        // записываем в свойство данные
        $this->data = json_decode(file_get_contents('php://input'));
        $this->token = $token;
        if(isset($this->data->Params)){
            $this->data->params = $this->data->Params;
        }
        if (isset($this->data->params->type->Class)) {
            $this->data->params->type->class = $this->data->params->type->Class;
        }
    }

    /**
     * @return getDataAll
     */
    public function getToken()
    {
        return $this->token;
    }
    /**
     * @return getDataAll
     */
    public function getDataAll()
    {
        return $this->data;
    }

    /**
     * @return getOrderID
     */
    public function getOrderID()
    {
        if (isset($this->data->order_id)) {
            return $this->data->order_id;
        }
        return false;
    }

    /**
     * @return getLimit
     */
    public function getLimit()
    {
        if (isset($this->data->params->values->limit)) {
            return $this->data->params->values->limit;
        }
        return 10;
    }

     /**
     * @return getLimit
     */
    public function getValue()
    {
        if (isset($this->data->params->values->phone)) {
            return $this->data->params->values->phone;
        }
        return null;
    }

    /**
     * @return getType
     */
    public function getType()
    {
        if (isset($this->data->params->type)) {
            if (isset($this->data->params->type->class)) {
                return 'Class';
            }elseif(isset($this->data->params->type->method)){
                return 'method';
            }else{
                return null;
            }
        }
    }

    /**
     * @return getClass
     */
    public function getTypeClass()
    {
        if (isset($this->data->params->type->class)) {
            if ($this->data->params->type->class === 'User') {
                return 'User';
            }elseif($this->data->params->type->class === 'UserList'){
                return 'UserList';
            }elseif($this->data->params->type->class === 'AccountAuthHistory'){
                return 'AccountAuthHistory';
            }elseif($this->data->params->type->class === 'Settings'){
                return 'Settings';
            }elseif($this->data->params->type->class === 'Telegram'){
                return 'Telegram';
            }elseif($this->data->params->type->class === 'Post'){
                return 'Post';
            }elseif($this->data->params->type->class === 'Module'){
                return 'Module';
            }elseif($this->data->params->type->class === 'Catalog'){
                return 'Catalog';
            }elseif($this->data->params->type->class === 'Product'){
                return 'Product';
            }elseif($this->data->params->type->class === 'ProductProperty'){
                return 'ProductProperty';
            }elseif($this->data->params->type->class === 'ProductItemProperty'){
                return 'ProductItemProperty';
            }elseif($this->data->params->type->class === 'ProductTypeProperty'){
                return 'ProductTypeProperty';
            }elseif($this->data->params->type->class === 'Basket'){
                return 'Basket';
            }elseif($this->data->params->type->class === 'Order'){
                return 'Order';
            }elseif($this->data->params->type->class === 'Stock'){
                return 'Stock';
            }elseif($this->data->params->type->class === 'Storage'){
                return 'Storage';
            }elseif($this->data->params->type->class === 'Category'){
                return 'Category';
            }elseif($this->data->params->type->class === 'Analytics'){
                return 'Analytics';
            }elseif($this->data->params->type->class === 'Discount'){
                return 'Discount';
            }else{
                return null;
            }
        }
    }

     /**
     * @return getTypeMethod
     */
    public function getTypeMethod()
    {
        if (isset($this->data->params->type->method)) {
            if ($this->data->params->type->method === 'info') {
                return 'info';
            }elseif($this->data->params->type->method === 'create'){
                return 'create';
            }elseif($this->data->params->type->method === 'update'){
                return 'update';
            }elseif($this->data->params->type->method === 'delete'){
                return 'delete';
            }else{
                return null;
            }
        }
    }
}