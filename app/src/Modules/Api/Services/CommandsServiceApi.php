<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Api\Services;

use app\Modules\Shop\Models\ShopModels;
use app\Modules\Shop\Models\ShopProductPropertyModels;
use app\Modules\Shop\Models\ShopProductItemPropertyModels;
use app\Modules\Shop\Models\ShopProductTypePropertyModels;
use app\Modules\Shop\Models\ShopCategoryModels;
use app\Modules\Shop\Models\ShopBasketModels;
use app\Modules\Shop\Models\ShopOrderModels;
use app\Modules\Shop\Models\ShopStockModels;
use app\Modules\Shop\Models\ShopStorageModels;
use app\Modules\Shop\Models\ShopAnalyticsModels;
use app\Modules\Shop\Models\ShopDiscountModels;
use app\Modules\Account\Models\AccountModels;
use app\Modules\Account\Models\AccountAuthHistoryModels;
use app\Modules\Api\Models\ApiNotificationModels;
use app\Modules\Api\Models\ApiSettingsModels;

class CommandsServiceApi
{
    public static function startMessage($output)
    {
        switch ($output->getTypeClass()) {
            case 'User':
                switch ($output->getTypeMethod()) {
                    case 'update':
                        return ApiNotificationModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        $user = AccountModels::InfoUserInAuchToken($output->getToken());
                        $notification = ApiNotificationModels::Info($output->getDataAll(), $output->getToken());
                        return array(
                            "code" => 200,
                            "status" => $notification["status"] ?? null,
                            "info" => $notification["text"] ?? null,
                            "notificationid" => $notification["notificationid"] ?? null,
                            "infocount" => $notification["infocount"] ?? null,
                            "balance" => ShopOrderModels::Info($output->getDataAll(), $output->getToken())["price"],
                            "username" => $user, //Переделать вывод!
                            "allnotification" => ApiNotificationModels::AllInfo($output->getDataAll(), $output->getToken())
                        );
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'UserList':
                switch ($output->getTypeMethod()) {
                    case 'info':
                        return AccountModels::UserList($output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'AccountAuthHistory':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return;
                    break;
                    case 'update':
                        return AccountAuthHistoryModels::history(AccountModels::InfoUserAccessToken($output->getToken()), 0);;
                    break;
                    case 'delete':
                        return;
                    break;
                    case 'info':
                        return;
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Settings':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return;
                    break;
                    case 'update':
                        return ApiSettingsModels::Update($output->getDataAll(), $output->getToken());;
                    break;
                    case 'delete':
                        return;
                    break;
                    case 'info':
                        return;
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Telegram':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return AccountModels::CreateCode($output->getToken());
                    break;
                    case 'update':
                        return;
                    break;
                    case 'delete':
                        return;
                    break;
                    case 'info':
                        return;
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Catalog':
                switch ($output->getTypeMethod()) {
                    case 'info':
                        return ShopModels::CatalogInfo($output->getDataAll());
                    break;
                    default:
                        return false;
                    break;
                }
            break;
            case 'Category':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopCategoryModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopCategoryModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopCategoryModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopCategoryModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Product':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'ProductProperty':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopProductPropertyModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopProductPropertyModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopProductPropertyModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopProductPropertyModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'ProductItemProperty':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopProductItemPropertyModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopProductItemPropertyModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopProductItemPropertyModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopProductItemPropertyModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'ProductTypeProperty':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopProductTypePropertyModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return;
                    break;
                    case 'delete':
                        return ShopProductTypePropertyModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopProductPropertyModels::Type($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Basket':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopBasketModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopBasketModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopBasketModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopBasketModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Order':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopOrderModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopOrderModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopOrderModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopOrderModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Stock':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopStockModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopStockModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopStockModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopStockModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Storage':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopStorageModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopStorageModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopStorageModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopStorageModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Analytics':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopAnalyticsModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopAnalyticsModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopAnalyticsModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopAnalyticsModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
            case 'Discount':
                switch ($output->getTypeMethod()) {
                    case 'create':
                        return ShopDiscountModels::Create($output->getDataAll(), $output->getToken());
                    break;
                    case 'update':
                        return ShopDiscountModels::Update($output->getDataAll(), $output->getToken());
                    break;
                    case 'delete':
                        return ShopDiscountModels::Delete($output->getDataAll(), $output->getToken());
                    break;
                    case 'info':
                        return ShopDiscountModels::Info($output->getDataAll(), $output->getToken());
                    break;
                    default:
                        return $output->getTypeMethod();
                    break;
                }
            break;
        }

    }
}