<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Messenger\Controllers;

use app\Core\ControllerCore;

class IndexMessengerController extends ControllerCore{

    public function actionIndex()
    {
        $this->title = 'Messenger';
        $this->description = 'Messenger';
        $this->keywords = 'Messenger';
        if(isset($_GET["sel"])){
            $userName = 'Игорь';
            if($_GET["sel"] == 2){
                $userName = 'Дарья';
            }
            if($_GET["sel"] == 3){
                $userName = 'Вадим';
            }
        }
        $data = array(
            'title' => 'Лучшие статьи только у нас!',
            'description' => 'Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. ',
            'userName' => $userName ?? null,
            $this
        );
        $this->rendercontroller(get_class($this), 'index', $data, 'assets');
    }
}