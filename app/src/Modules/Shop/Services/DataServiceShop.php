<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Services;

use app\Modules\Shop\Models\ShopModels;
use app\Modules\Shop\Models\ShopOrderModels;
use app\Modules\Shop\Models\ShopCategoryModels;
use app\Modules\Account\Models\AccountModels;
use app\Modules\Tg\Models\TgUserModels;
use app\Modules\Tg\Components\TgAPI;

class DataServiceShop
{
	public static function InitSearch($data, $token){
		if(isset($data->params->values->date_begin) && isset($data->params->values->date_end) && isset($data->params->values->time_begin) && isset($data->params->values->time_end)){
			return self::DateTimeSearch($data, $token);
		}
		if(isset($data->params->values->date_begin) && isset($data->params->values->date_end)){
			return self::DateSearch($data, $token);
		}
		if(isset($data->params->values->product_id)){
			return self::ProductIdSearch($data, $token);
		}
        if(isset($data->params->values->category_id) && $data->params->values->category_id != 0){
            return self::CategoryIdSearch($data, $token);
        }
        if(isset($data->params->values->price_from) && isset($data->params->values->price_upto)){
            return self::PriceSearch($data, $token);
        }
		if(isset($data->params->values->user_id) && $data->params->values->user_id != 0){
        	return self::UserSearch($data, $token);
        }
        if(isset($data->params->values->userfinance)){
        	return self::UserFinance($data, $token);
        }
        if(isset($data->params->values->SaleFinance)){
            return self::SaleFinance($data, $token);
        }
        if(isset($data->params->values->PopularCategory)){
            return self::PopularCategory($data, $token);
        }
        if(isset($data->params->values->PopularUser)){
            return self::PopularUser($data, $token);
        }
        if(isset($data->params->values->SaleHeader)){
            return self::AllSearch($data, $token);
        }
        if(isset($data->params->values->SaleHeaderTelegram)){
            return self::SaleHeaderTelegram($data, $token);
        }
        return self::DefaultSearch($data, $token);
	}

	public static function SaleHeaderTelegram($data, $token){

        if($user_id = AccountModels::InfoUserAccessToken($token)){
            if($tgmodel = TgUserModels::get()->from()->where(['user_id', '=', $user_id, ' AND status', '=', 1])->fetch()){
                $default = self::DefaultSearch($data, $token);
                TgAPI::sendTelegram('sendMessage',
                    array(
                        'chat_id' => $tgmodel[0]->chat_member_id,
                        'text' => "Выручка за всё время \n" .'<b>'.self::convert($default['price']).'</b> &#8381',
                        'parse_mode' => 'HTML'
                    ),
                    'kassa'
                );
                return array(
                    'status' => 200,
                    'tgmessage' => 'Отчет направлен в телеграм'
                );
            }
        }
        return array(
            'status' => 200,
            'tgmessage' => 'Ошибка!'
        );
    }

    public static function convert($number){
        # Переводим в рубли
        $number /= 1;

        return number_format($number, 2, ',', ' ');
    }

    public static function AllSearch($data, $token){

        $items = array();
        $PriceSearch = ShopOrderModels::get()->from()->where(['order_status_id', '=', 1])->orderBy('id', 'ASC')->fetch();
        foreach($PriceSearch as $sum){
            $items[] =  $sum->total_price;
            $product_id[] =  array(
                "date" => date('d.m.Y H:i:s', $sum->updated_at),
                "title" => ShopModels::ProductInfo($sum->product_id)->title,
                "count" => $sum->product_count,
                "price" => $sum->price,
                "total_price" => $sum->total_price
            );
            $product_count[] =  $sum->product_count;

        }

        $SELECT =array(
            "SUM(total_price) AS discount, SUM(product_count) AS discountcount,
            (SELECT SUM(total_price) FROM `shop_order` WHERE product_count > 0) AS price,
            (SELECT SUM(product_count) FROM `shop_order` WHERE product_count > 0) AS count"
        );
        foreach(ShopOrderModels::get()->select($SELECT)->from()->where(['total_price' , '<', 0])->orderBy('updated_at', 'ASC')->fetch() as $sum){
            $data = array(
                "discount" => $sum->discount,
                "discountcount" => $sum->discountcount,
                "price" => $sum->price,
                "count" => $sum->count
            );
        }
        if(!$items){
            return 0;
        }
        $data = $data['price'];
        switch($data)
        {
            case $data <= 1000:
                return substr($data, 0, 3)."р";
                break;
            case $data <= 10000:
                return substr($data, 0, 1)."K";
                break;
            case $data <= 100000:
                return substr($data, 0, 2)."K";
                break;
            case $data <= 1000000:
                return substr($data, 0, 3)."K";
                break;
            case $data <= 10000000:
                return substr($data, 0, 1)."M";
                break;
            case $data <= 100000000:
                return substr($data, 0, 2)."M";
                break;
            case $data <= 1000000000:
                return substr($data, 0, 3)."B";
                break;
            case $data <= 10000000000:
                return substr($data, 0, 4)."B";
                break;
            default:
                return 0;
            break;
        }

    }

    public static function DefaultSearch($data, $token){
		$items = array();
        $PriceSearch = ShopOrderModels::get()->from()->where(['order_status_id', '=', 1])->orderBy('updated_at', 'DESC')->fetch();
        foreach($PriceSearch as $sum){
            $items[] =  $sum->total_price;
            $product_id[] =  array(
                "date" => date('d.m.Y H:i:s', $sum->updated_at),
                "title" => ShopModels::ProductInfo($sum->product_id)->title,
                "count" => $sum->product_count,
                "price" => $sum->price,
                "total_price" => $sum->total_price
            );
            $product_count[] =  $sum->product_count;

        }

        $SELECT =array(
            "SUM(total_price) AS discount, SUM(product_count) AS discountcount,
            (SELECT SUM(total_price) FROM `shop_order` WHERE product_count > 0) AS price,
            (SELECT SUM(product_count) FROM `shop_order` WHERE product_count > 0) AS count"
        );
        foreach(ShopOrderModels::get()->select($SELECT)->from()->where(['total_price' , '<', 0])->orderBy('updated_at', 'DESC')->fetch() as $sum){
            $data = array(
                "discount" => $sum->discount,
                "discountcount" => $sum->discountcount,
                "price" => $sum->price,
                "count" => $sum->count
            );
        }
        if(!$items){
            return array(
            "title" => 'Нет данных',
            "price" => 0,
            "productcount" => 0
        );
        }
        return array(
            "title" => 'Скидка '.$data['discount']. ', кол. '.$data['discountcount'],
            "price" => $data['price'],
            "productcount" => $data['count'] - $data['discountcount'],
            "product" => $product_id
        );
	}

	public static function DateTimeSearch($data, $token){
        $date_begin = $data->params->values->date_begin. ' ' .$data->params->values->time_begin.':00';
        $date_end = $data->params->values->date_end. ' '.$data->params->values->time_end.':00';

        $elements = array();
        $items = array();
        $product_id = array();
        $product_count = array();
        if(isset($data->params->values->user_id) && $data->params->values->user_id != 0){
            $where = array(
                "DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) >  '".$date_begin."' AND DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) <  '".$date_end."' AND user_id = '".$data->params->values->user_id."'"
            );
        }else{
            $where = array(
                "DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) >  '".$date_begin."' AND DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) <  '".$date_end."'"
            );
        }

        foreach(ShopOrderModels::get()->from()->where($where)->orderBy('updated_at', 'ASC')->fetch() as $sum){
            $elements[] = $sum->id;
            $product_id[] =  array(
                "date" => date('d.m.Y H:i:s', $sum->updated_at),
                "title" => ShopModels::ProductInfo($sum->product_id)->title,
                "count" => $sum->product_count,
                "price" => $sum->price,
                "total_price" => $sum->total_price
            );
            $product_count[] =  $sum->product_count;
            $items[] =  $sum->total_price;
        }

        return array(
            "price" => array_sum($items),
            "productcount" => array_sum($product_count),
            "params" => date('d.m.Y H:i', strtotime($date_begin)).' по '.date('d.m.Y H:i', strtotime($date_end)),
            "product" => $product_id
        );
	}

	public static function DateSearch($data, $token){
    	$date_begin = $data->params->values->date_begin. ' 00:01:00';
        $date_end = $data->params->values->date_end. ' 23:59:00';

        $elements = array();
        $items = array();
        $product_id = array();
        $product_count = array();
        if(isset($data->params->values->user_id) && $data->params->values->user_id != 0){
            $where = array(
                "DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) >  '".$date_begin."' AND DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) <  '".$date_end."' AND user_id = '".$data->params->values->user_id."'"
            );
        }else{
            $where = array(
                "DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) >  '".$date_begin."' AND DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) <  '".$date_end."'"
            );
        }
        foreach(ShopOrderModels::get()->from()->where($where)->orderBy('updated_at', 'ASC')->fetch() as $sum){
            $elements[] = $sum->id;
            $product_id[] =  array(
                "date" => date('d.m.Y H:i:s', $sum->updated_at),
                "title" => ShopModels::ProductInfo($sum->product_id)->title,
                "count" => $sum->product_count,
                "price" => $sum->price,
                "total_price" => $sum->total_price
            );
            $product_count[] =  $sum->product_count;
            $items[] =  $sum->total_price;
        }
        return array(
            "price" => array_sum($items),
            "productcount" => array_sum($product_count),
            "params" => date('d.m.Y H:i', strtotime($date_begin)).' по '.date('d.m.Y H:i', strtotime($date_end)),
            "product" => $product_id
        );
	}

	public static function ProductIdSearch($data, $token){
        $items = array();
        foreach(ShopOrderModels::get()->from()->where(array('product_id', '=', $data->params->values->product_id))->orderBy('updated_at', 'ASC')->fetch() as $sum){
            $items[] =  $sum->total_price;
            $title =  ShopModels::ProductInfo($sum->product_id)->title;
            $product_id[] =  array(
                "date" => date('d.m.Y H:i:s', $sum->updated_at),
                "title" => $title,
                "count" => $sum->product_count,
                "price" => $sum->price,
                "total_price" => $sum->total_price
            );
            $product_count[] =  $sum->product_count;

        }
        return array(
            "title" => $title,
            "price" => array_sum($items),
            "productcount" => array_sum($product_count),
            "product" => $product_id
        );
	}

    public static function CategoryIdSearch($data, $token){
        $CategoryId = ShopModels::CategoryInfo($data);
        $items = array();
        $itemsCategoryId = array();
        foreach($CategoryId as $CategoryId){
            $itemsCategoryId[] = $CategoryId['price'];
            foreach(ShopOrderModels::get()->from()->where(array('product_id', '=', $CategoryId['id']))->orderBy('updated_at', 'DESC')->fetch() as $sum){
                $items[] =  $sum->total_price;
                $product_id[] =  array(
                    "date" => date('d.m.Y H:i:s', $sum->updated_at),
                    "title" => ShopModels::ProductInfo($sum->product_id)->title,
                    "count" => $sum->product_count,
                    "price" => $sum->price,
                    "total_price" => $sum->total_price
                );
                $product_count[] =  $sum->product_count;

            }
        }
        if(!$items){
            return array(
            "title" => 'Нет данных',
            "price" => 0,
            "productcount" => 0
        );
        }
        return array(
            "title" => ShopCategoryModels::CategoryIDInfo($data)[0]['title'],
            "price" => array_sum($items),
            "productcount" => array_sum($product_count),
            "product" => $product_id
        );
    }

    public static function PriceSearch($data, $token){
        $items = array();
        $DiscountPriceSearch = ShopOrderModels::get()->from()->where(['total_price', '<', 0])->orderBy('id', 'ASC')->limit(1000)->fetch();
        foreach($DiscountPriceSearch as $DiscountPriceSearch){
            $DiscountPriceSearchitems[] = $DiscountPriceSearch->total_price;
        }
        $PriceSearch = ShopOrderModels::get()->from()->where(["total_price BETWEEN  '".$data->params->values->price_from."' AND '".$data->params->values->price_upto."'"])->orderBy('id', 'ASC')->limit(1000)->fetch();

        foreach($PriceSearch as $sum){
            $items[] =  $sum->total_price;
            $product_id[] =  array(
                "date" => date('d.m.Y H:i:s', $sum->updated_at),
                "title" => ShopModels::ProductInfo($sum->product_id)->title,
                "count" => $sum->product_count,
                "price" => $sum->price,
                "total_price" => $sum->total_price
            );
            $product_count[] =  $sum->product_count;
        }
        if(!$items){
            return array(
            "title" => 'Нет данных',
            "price" => 0,
            "productcount" => 0
        );
        }
        return array(
            "title" => 'Исходя из суммы',
            "price" => array_sum($items),
            "discount" => array_sum($DiscountPriceSearchitems),
            "productcount" => array_sum($product_count),
            "product" => $product_id
        );
    }

	public static function UserSearch($data, $token){
        $items = array();
        $PriceSearch = ShopOrderModels::get()->from()->where(array('user_id', '=', $data->params->values->user_id))->orderBy('updated_at', 'DESC')->fetch();

        foreach($PriceSearch as $sum){
            $items[] =  $sum->total_price;
            $product_id[] =  array(
                "date" => date('d.m.Y H:i:s', $sum->updated_at),
                "title" => ShopModels::ProductInfo($sum->product_id)->title,
                "count" => $sum->product_count,
                "price" => $sum->price,
                "total_price" => $sum->total_price
            );
            $product_count[] =  $sum->product_count;
        }
        if(!$items){
            return array(
                "title" => 'Нет данных',
                "price" => 0,
                "productcount" => 0
            );
        }
        $userdata = AccountModels::isUserPerson($data->params->values->user_id);
        return array(
            "title" => $userdata['last_name'].' '.$userdata['first_name'],
            "price" => array_sum($items),
            "productcount" => array_sum($product_count),
            "product" => $product_id
        );
	}

    public static function PopularCategory($data, $token){

        $select = array(
            "p.category_id, SUM(o.product_count) AS TotalQuantity, SUM(o.product_count * o.price) AS TotalRevenue"
        );

        foreach(ShopOrderModels::get()->select($select)->from('o')->join(['shop_product p'])->on(['p.id = o.product_id'])->groupBy('p.category_id', '')->orderBy('TotalRevenue', 'DESC')->limit(5)->fetch() as $PopularCategory){
            if($PopularCategory->TotalRevenue != 0.00){
                $percent[] = $PopularCategory->TotalRevenue;
                $dataPopularCategory[] =  array(
                    "title" => ShopCategoryModels::CategoryID($PopularCategory->category_id)[0]['title'],
                    "count" => $PopularCategory->TotalQuantity,
                    "percent" => round($PopularCategory->TotalRevenue * 100 / round(array_sum($percent))),
                    "total_price" => $PopularCategory->TotalRevenue
                );
            }
        }
        return $dataPopularCategory;
    }

    public static function PopularUser($data, $token){
        $select = array(
            "user_id, SUM(product_count) AS TotalQuantity, SUM(product_count * price) AS TotalRevenue"
        );

        $u = ShopOrderModels::get()->from()->select($select)->groupBy('user_id', '')->orderBy('TotalRevenue', 'DESC')->limit(5)->fetch();
        foreach($u as $PopularUser){
            $user = AccountModels::get()
            ->findOne(
                array(
                    'id' => $PopularUser->user_id
                )
            )[0];
            $percent[] = $PopularUser->TotalRevenue;
            $dataPopularUser[] =  array(
                "user" => $user['last_name'].' '.$user['first_name'],
                "price" => $PopularUser->TotalRevenue,
                "productcount" => $PopularUser->TotalQuantity,
                "percent" => round($PopularUser->TotalRevenue * 100 / round(array_sum($percent))),
            );
        }

        return $dataPopularUser;
    }

    public static function SaleFinance($data, $token){
         // return self::PopularCategory($data, $token); //Здесь остановился!

        $select = array(
            "YEAR(DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' )) as year,
                MONTH(DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' )) as month,
                SUM(total_price) as month_revenue"
        );

        $items = array();
        $data = array();
        $t = ShopOrderModels::get()->from()->select($select)->where(array('MONTH(FROM_UNIXTIME(`updated_at`))', '>=', date('n')-6))->groupBy('year', ', month')->orderBy('year', ', month')->limit(6)->fetch();
        foreach($t as $key => $s){
            $items[] = $s->month_revenue;
            $data[] = $s;
        }

        foreach($t as $key => $s){
            switch($s->month)
            {
                case 1:
                    $s->month = "январь";
                    break;
                case 2:
                    $s->month = "февраль";
                    break;
                case 3:
                    $s->month = "март";
                    break;
                case 4:
                    $s->month = "апрель";
                    break;
                case 5:
                    $s->month = "май";
                    break;
                case 6:
                    $s->month = "июнь";
                    break;
                case 7:
                    $s->month = "июль";
                    break;
                case 8:
                    $s->month = "август";
                    break;
                case 9:
                    $s->month = "сентябрь";
                    break;
                case 10:
                    $s->month = "октябрь";
                    break;
                case 11:
                    $s->month = "ноябрь";
                    break;
                case 12:
                    $s->month = "декабрь";
                    break;
            }

            if(true){
                $month_revenue[] =  array(
                    // "month_revenue_max" => round(100 - ($s->month_revenue / max($items)) * 100),
                    "month_revenue_min" => round($s->month_revenue * 100 / array_sum($items)),
                    // "month_revenue_min" => 100 - round(100 - ($s->month_revenue / max($items)) * 100),
                    "month_revenue" => $s->month_revenue,
                    "month" => $s->month,
                    "date" => strtotime("2024-01-15 00:01:00")
                );
            }
        }

        $max = max($items);
        return $month_revenue;
    }

	public static function UserFinance($data, $token){
        $select = array(
            "product_id, DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' ) as YEAR,
            count(product_count) AS product_count,
            SUM(product_count * price) AS price"
        );
        $user_id = AccountModels::InfoUserAccessToken($token);

        $userbalance = ShopOrderModels::get()->from()->select($select)->where(array('user_id', '=', $user_id, 'AND', "DATE_FORMAT( FROM_UNIXTIME( `updated_at` ) ,  '%Y-%m-%d %H:%i' )>= CURDATE()"))->orderBy('updated_at', 'DESC')->fetch();
        if(!$userbalance){
            return array(
                "price" => 0
            );
        }
        foreach($userbalance as $sum){
            $total_price[] =  $sum->price;
            $product_id =  $sum->product_id;
            $product_count[] =  $sum->product_count;
        }

        return array(
            "price" => array_sum($total_price),
            "productcount" => array_sum($product_count)
        );
	}
}