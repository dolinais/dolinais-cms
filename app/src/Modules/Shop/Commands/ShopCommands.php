<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */



namespace app\Modules\Shop\Commands;

use app\Models\AgentModels;
use app\Modules\Tg\Services\ServiceMessageTg;
use app\Modules\Tg\Models\TgUserModels;
use app\Modules\Shop\Services\DataServiceShop;

class ShopCommands
{
	public function __construct()
    {
    	date_default_timezone_set('Europe/Moscow');
        foreach(AgentModels::get()->findOne(
        	array(
        		'active' => 'Y'
        	)
        ) as $cron){
        	$startTime = strtotime( date('Y-m-d H:i:s', $cron['last_exec_at']) );
			$endTime = strtotime( date('Y-m-d H:i:s') );

			for (; $startTime <= $endTime; $startTime += $cron['agent_interval'] * 60) {
                $results = date('Y-m-d H:i', $startTime);
            }
            if(date('Y-m-d H:i') == $results){
            	$data = json_decode(json_encode(array(
		                'params' => array(
		                	'values' => array(
		                		'user_id' => $cron['user_id']
		                	)
		                )
		            )));
		    	$userInfo = DataServiceShop::InitSearch($data, null);
		    	var_dump($userInfo['title']);
            	ServiceMessageTg::SendMassage(TgUserModels::get()->from()->where(['user_id', '=', $cron['user_id'], ' AND status', '=', 1])->fetch()[0]->chat_member_id, $userInfo['title'].' '.self::convert($userInfo['price']).'₽' );
            }
        }
    }

    public static function CronMinute(){
    	foreach(AgentModels::get()->findOne(
        	array(
        		'active' => 'Y'
        	)
        ) as $cron){
        	$startTime = strtotime( date('Y-m-d H:i:s', $cron['last_exec_at']) );
			$endTime = strtotime( date('Y-m-d H:i:s') );

			for (; $startTime <= $endTime; $startTime += $cron['agent_interval'] * 60) {
                $results = date('Y-m-d H:i', $startTime);
            }
            if(date('Y-m-d H:i') == $results){
            	ServiceMessageTg::SendMassage(TgUserModels::get()->from()->where(['user_id', '=', $cron['user_id'], ' AND status', '=', 1])->fetch()[0]->chat_member_id, 'Сообщение от сервера '.$results );
            }
        }
    }

    public static function convert($number){
        # Переводим в рубли
        $number /= 1;

        return number_format($number, 2, ',', ' ');
    }
}