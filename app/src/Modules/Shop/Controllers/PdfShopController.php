<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Controllers;

use app\Modules\Shop\Models\ShopModels;
use app\Modules\Shop\Models\ShopPrintModels;
// use app\Components\FPDFComponents;
use app\Components\PDF_EAN13Components;
use app\Components\PDF;

class PdfShopController extends PDF_EAN13Components
{

    public function actionIndex()
    {
        $info = ShopPrintModels::get()->from()->where(['status' , '=', 1])->orderBy('id', 'ASC')->fetch();
        $pdf = new PDF_EAN13Components();
        $pdf->AddFont('Arial','','arial.php');
        $pdf->SetTitle(iconv('utf-8', 'windows-1251',"Price"));
        $pdf->SetFont('Arial','',12);


        $str = count($info) / 7;
        $Count = count($info);
        for ($Page = 1; $Page <= $Count; $Page++) {

            if (($Page) % ($Count) == $Count-1){
                if($Count < 21){
                    $PDDDage = $Count-1 - $Page;
                    if($PDDDage < 0){
                        $PDDDage = 0;
                    }
                    $info = ShopPrintModels::get()->from()->where(['status' , '=', 1])->limit("21 OFFSET $PDDDage")->orderBy('id', 'ASC')->fetch();
                    $this->columnOotput($pdf, $info);
                }
            }else{
                if ($Page % 21 == 0){
                    $PDDDage = $Page-21;
                    $info = ShopPrintModels::get()->from()->where(['status' , '=', 1])->limit("21 OFFSET $PDDDage")->orderBy('id', 'ASC')->fetch();
                    $this->columnOotput($pdf, $info);
                }
            }
        }
        // exit;
        $pdf->Output();
    }

    public function columnOotput($pdf, $info){
        $pdf->AddPage();
        $p = 0;
        $left = 190;
        $start = 0;
        foreach (range(10, 130) as $left) {
            if ($left % 60 == 10){
                for ($i = 0; $i <= 2; $i++) {
                    $start++;
                    if(isset(array_chunk($info, ceil(count($info)/3))[$i])){
                        foreach(array_chunk($info, ceil(count($info)/3))[$i] as $key => $data){
                            if ($start % 4 == 1){
                                $p = $p+38;
                                if($p == 304){ $p = 38; }
                                // echo $left."\n<br />";
                                $this->column($pdf, $data, $p, $left);
                            }
                        }
                    }
                }
            }
        }
    }

    public function column($pdf, $info, $p, $w){
        $barcode = (int)$info->barcode;
        $price = 'не указана';
        if($info->price){
            $price = number_format($info->price, 0, ',', ' ').substr('р.', 0, 3);
        }

        $pdf->SetFontSize(6);
        $text=iconv('utf-8', 'windows-1251',$info->title);
        $text = substr($text, 0, 68);


        if(mb_strlen($text, "UTF-8") < 37){
            $pdf->SetXY($w,$p-30);
            $pdf->MultiCell(45,6,$text,1,'C',0);
            $pdf->SetFontSize(18);
            $pdf->SetXY($w,$p-24);
            $pdf->MultiCell(45,8,iconv('utf-8', 'windows-1251',$price),1,'C',0);
        }else{
            $pdf->SetXY($w,$p-30);
            $pdf->MultiCell(45,3,$text,1,'C',0);
            $pdf->SetFontSize(18);
            $pdf->SetXY($w,$p-24);
            $pdf->MultiCell(45,8,iconv('utf-8', 'windows-1251',$price),1,'C',0);
        }

        $pdf->Text(62,$p-10,'|');
        $pdf->Text(122,$p-10,'|');

        if(strlen($barcode) >= 6){
            $pdf->SetXY($w,$p-16);
            $pdf->MultiCell(45,22,'',1,'C',0);
            $pdf->EAN13(5+$w,$p-16, $barcode);
        }
    }

    public function Header()
    {

        $this->SetY(10);
        $this->SetFont('Arial','',20);
        $this->SetTextColor(0,0,0);
        $this->Cell(0,9,"INVOICE",0,0,'R');
     }

     public function Footer()
     {
       //any footer stuff goes here
     }
}