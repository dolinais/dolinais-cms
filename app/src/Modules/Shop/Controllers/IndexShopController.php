<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Controllers;

use app\Modules\Shop\Models\ShopModels;
use app\Components\FPDFComponents;
use app\Components\PDF_EAN13Components;
use app\Components\PDF;

class IndexShopController
{
    public function actionIndex()
    {
        // $pdf->WordWrap($text,30);
        // $pdf->Text(10,27,'--------------------------------------------------------------------------------------------------------------------');
        $info = ShopModels::get()->from()->where(['status' , '=', 1])->orderBy('id', 'ASC')->fetch();
        $pdf = new PDF_EAN13Components();

        $pdf->AddFont('Arial','','arial.php');
        $pdf->SetFont('Arial');
        $pdf->AddPage();

        $pdf->SetFontSize(24);
        $pdf->SetXY(10,10);
        // $pdf->SetTextColor(250,60,100);
        $pdf->Write(0,iconv('utf-8', 'windows-1251',"Прайс"));
        // $pdf->Image('resources/img/logo.png',10,280,20);
        $pdf->Ln(6);

        $pdf->SetFont('Arial');
        $pdf->SetFillColor(128,128,128);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(92,92,92);
        $pdf->SetLineWidth(.3);
        $pdf->SetFontSize(10);
        $pdf->Cell(110,6,iconv('utf-8', 'windows-1251',"Наименование"),1,0,'C',true);
        $pdf->Cell(20,6,iconv('utf-8', 'windows-1251',"Остатки"),1,0,'C',true);
        $pdf->Cell(20,6,iconv('utf-8', 'windows-1251',"Сумма"),1,0,'C',true);
        $pdf->Ln();

        $pdf->SetFillColor(224,235,255);
        $pdf->SetTextColor(0);
        $pdf->SetFont('');

        foreach($info as $info){
            $pdf->SetFont('Arial');
            $pdf->SetFontSize(6);
            $pdf->Cell(110,6,iconv('utf-8', 'windows-1251',$info->title),1,0,'L',true);
            $pdf->Cell(20,6,$info->id,1,0,'C',true);
            $pdf->Cell(20,6,$info->price,1,0,'C',true);
            $pdf->Ln();
        }

        $pdf->Output();

    }
}