<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Controllers;

use app\Modules\Shop\Models\ShopModels;
// use app\Components\FPDFComponents;
use app\Components\PDF_EAN13Components;
use app\Components\PDF;

class DocShopController extends PDF_EAN13Components
{
    function ConvertToUTF8($text){

        $encoding = mb_detect_encoding($text, mb_detect_order(), false);

        if($encoding == "UTF-8")
        {
            $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');
        }


        $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);


        return $out;
    }

    public function actionIndex()
    {
        $html = 'Тестовая страница: <b>bold</b>, <i>italic</i>,
        <u>underlined</u>, or <b><i><u>all at once</u></i></b>!<br><br>You can also insert links on
        text, such as <a href="https://dolinais.ru">https://dolinais.ru</a>, or on an image: click on the logo.';

        $pdf = new PDF();

        $pdf->AddPage();
        $pdf->AddFont('Arial','','arial.php');
        $pdf->SetFont('Arial');
        $pdf->Write(5,iconv('utf-8', 'windows-1251','Нажмите на '));
        $pdf->SetFont('','U');
        $link = $pdf->AddLink();
        $pdf->Write(5,'here',$link);
        $pdf->SetFont('');

        $pdf->AddPage();
        $pdf->SetLink($link);
        $pdf->Image('https://cms.dolinais.ru/resources/img/logo.png',10,12,30,0,'','https://dolinais.ru');
        $pdf->SetLeftMargin(45);
        $pdf->SetFontSize(14);
        $pdf->WriteHTML(iconv('utf-8', 'windows-1251',$html));
        $pdf->Output();
        // $pdf->Output("F", UPLOADS . '/test.pdf');
    }

    public function Header()
    {

        $this->SetY(10);
        $this->SetFont('Arial','',20);
        $this->SetTextColor(0,0,0);
        $this->Cell(0,9,"INVOICE",0,0,'R');
     }

     public function Footer()
     {
       //any footer stuff goes here
     }
}