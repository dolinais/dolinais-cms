<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Controllers;

use app\Core\ControllerCore;
use app\Modules\Shop\Models\ShopModels;

class PrintShopController extends ControllerCore
{
    public function actionIndex()
    {
        $data = array(
            'title' => 'Прайс',
            'description' => '',
            'post' => ShopModels::get()->from()->where(['status' , '=', 1])->orderBy('id', 'ASC')->limit(10)->fetch(),
            $this
        );
        // $this->rendercontroller(get_class($this), 'index', $data, 'assets');
        \DolinaIS::setTemplates(\DolinaIS::setAlias('@templates').'/app/Modules/Shop/Controllers/PrintShopController/index', array('data'=> $data));
    }
}