<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Modules\Shop\Models\ShopModels;
use app\Modules\Shop\Models\ShopBasketModels;
use app\Modules\Shop\Models\ShopBasketIdModels;
use app\Modules\Shop\Models\ShopStockModels;
use app\Modules\Shop\Services\DataServiceShop;
use app\Modules\Account\Models\AccountModels;

class ShopOrderHistoryModels extends SelectPDOCore
{
    public static function tableName()
    {
        return 'shop_order_history';
    }
}