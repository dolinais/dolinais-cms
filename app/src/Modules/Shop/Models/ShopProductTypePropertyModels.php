<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;

class ShopProductTypePropertyModels extends SelectPDOCore
{
    public static function Create($data, $token=null)
    {
        $ShopBasketIdModels = self::get()->getInsert(array(
            'product_id' => $data->params->values->product_id,
            'property_id' => $data->params->values->property_id,
            'sorting' => $data->params->values->sorting,
            'created_at' => time()
        ));
        return $ShopBasketIdModels;
    }
    public static function Delete($data, $token=null)
    {
        self::get()->getDelete(array(
            "id" => $data->params->values->id
        ));
        return true;
    }
    public static function Info($data, $token=null)
    {
        return self::get()->from()->where(['id', '=', $data->params->values->id])->orderBy('id', 'DESC')->limit(1)->fetch();
    }
    public static function InfoID($product_id)
    {
        return self::get()->from()->where(['product_id', '=', $product_id])->orderBy('sorting', 'ASC')->limit(1000)->fetch();
    }

    public static function tableName()
    {
        return 'shop_product_type_property';
    }
}