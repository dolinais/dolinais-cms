<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;

use app\Modules\Shop\Models\ShopProductTypePropertyModels;

class ShopProductPropertyModels extends SelectPDOCore
{
    public static function Create($data, $token=null)
    {
        $ShopBasketIdModels = self::get()->getInsert(array(
            'name' => $data->params->values->name,
            'data' => $data->params->values->data,
            'value' => $data->params->values->value,
            'created_at' => time()
        ));
        return $ShopBasketIdModels;
    }
    public static function Delete($data, $token=null)
    {
        $Id = self::get()->getDelete(array(
            "id" => $data->params->values->id
        ));
        return $id;
    }
    public static function Info($data, $token=null)
    {
        $ProductTypePropertyModels = array();
        foreach(ShopProductTypePropertyModels::InfoID($data->params->values->id) as $ShopProductTypePropertyModels){
            $id[] = $ShopProductTypePropertyModels->id;
            $ProductTypePropertyModels[] = array(
                "id" => $ShopProductTypePropertyModels->id,
                "sorting" => $ShopProductTypePropertyModels->sorting,
                "data" => self::get()->from()->where(['id', '=', $ShopProductTypePropertyModels->property_id])->orderBy('id', 'ASC')->limit(100)->fetch()[0]
            );
        }

        return $ProductTypePropertyModels;
    }

    public static function Type($data, $token=null)
    {
        return self::get()->from()->where([$data->params->values->type, '=', '"'.$data->params->values->value.'"'])->orderBy('id', 'ASC')->limit(1000)->fetch();
    }

    public static function tableName()
    {
        return 'shop_product_property';
    }
}