<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Modules\Shop\Models\ShopBasketModels;
use app\Modules\Shop\Models\ShopBasketIdModels;
use app\Modules\Shop\Models\ShopStockModels;
use app\Modules\Account\Models\AccountModels;

class ShopDiscountModels extends SelectPDOCore
{
    public static function Create($data, $token=null)
    {
        $userId = AccountModels::InfoUserAccessToken($token);
        if(isset($data->params->values->coupon)){
            if(!self::get()->findOne(
                array('coupon' => $data->params->values->coupon)
            ))
            {
                $ShopDiscountModels = self::get()->getInsert(array(
                    'user_id' => $userId,
                    'title' => htmlspecialchars(strip_tags($data->params->values->title ?? null)),
                    'product_id' => htmlspecialchars(strip_tags($data->params->values->product_id ?? null)),
                    'promo_text' => htmlspecialchars(strip_tags($data->params->values->promo_text ?? null)),
                    'coupon' => htmlspecialchars(strip_tags($data->params->values->coupon ?? null)),
                    'assignment_type' => htmlspecialchars(strip_tags($data->params->values->assignment_type ?? null)),
                    'value' => htmlspecialchars(strip_tags($data->params->values->value ?? null)),
                    'active' => htmlspecialchars(strip_tags($data->params->values->active ?? null)),
                    'duration' => htmlspecialchars(strip_tags($data->params->values->duration ?? null)),
                    'created_at' => time(),
                    'updated_at' => time()
                ));
                return $ShopDiscountModels;
            }
        }
        return true;
    }
    public static function Update($data)
    {
        $ShopDiscountModels = self::get()->getUpdate(
            array(
                'title' => htmlspecialchars(strip_tags($data->params->values->title ?? null)),
                'product_id' => htmlspecialchars(strip_tags($data->params->values->product_id ?? null)),
                'promo_text' => htmlspecialchars(strip_tags($data->params->values->promo_text ?? null)),
                'coupon' => htmlspecialchars(strip_tags($data->params->values->coupon ?? null)),
                'assignment_type' => htmlspecialchars(strip_tags($data->params->values->assignment_type ?? null)),
                'value' => htmlspecialchars(strip_tags($data->params->values->value ?? null)),
                'active' => htmlspecialchars(strip_tags($data->params->values->active ?? null)),
                'duration' => htmlspecialchars(strip_tags($data->params->values->duration ?? null)),
                'updated_at' => time()
            ),
            $data->params->values->id
        );
        return true;
    }
    public static function Delete($data)
    {
        $InsertPageId = self::get()->getDelete(array(
            "id" => htmlspecialchars(strip_tags($data->params->values->id ?? null))
        ));
        return true;
    }
    public static function Info($data)
    {
        if(isset($data->params->values->coupon)){
            return self::get()->findOne(
                array('coupon' => $data->params->values->coupon)
            );
        }else{
            return self::get()->findOne(
                array('active' => 1)
            );
        }
    }

    public static function tableName()
    {
        return 'shop_discount';
    }
}