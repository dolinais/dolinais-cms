<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;

class ShopStockModels extends SelectPDOCore
{
    public static function Create($data)
    {
        foreach(self::get()->findOne(
            array('product_id' => $data->params->values->product_id)
        ) as $productid){
            if($data->params->values->product_id == $productid["product_id"]){
                $productcount = $productid["quantity"] + $data->params->values->quantity;
                $ShopBasketModels = self::get()->getUpdate(
                    array(
                        'quantity' => $productcount,
                        'updated_at' => time()
                    ),
                    $productid["id"]
                );
                return true;
            }
        }
        $ShopStockModels = self::get()->getInsert(array(
            'product_id' => $data->params->values->product_id,
            'quantity' => $data->params->values->quantity,
            'created_at' => time(),
            'updated_at' => time()
        ));
        return true;
    }
    public static function Update($data)
    {
        if($info = self::get()->findOne(
            array('product_id' => $data->params->values->product_id)
        )){
            $ShopStockModels = self::get()->getUpdate(
                array(
                    // 'quantity' => htmlspecialchars(strip_tags($info[0]["quantity"] + $data->params->values->quantity)),
                    'quantity' => htmlspecialchars(strip_tags($data->params->values->quantity)),
                    'updated_at' => time()
                ),
                $info[0]["id"]
            );
            return $info[0]["id"];
        }else{
            self::get()->getInsert(array(
                'product_id' => $data->params->values->product_id,
                'quantity' => htmlspecialchars(strip_tags($data->params->values->quantity)),
                'created_at' => time(),
                'updated_at' => time()
            ));
            return true;
        }
    }
    public static function UpdateOrder($product_id, $quantity){
        $info = self::get()->findOne(
            array('product_id' => $product_id)
        );
        $ShopStockModels = self::get()->getUpdate(
            array(
                'quantity' => htmlspecialchars(strip_tags($info[0]["quantity"] - $quantity)),
                'updated_at' => time()
            ),
            $info[0]["id"]
        );
    }
    public static function Delete($data)
    {
        $ShopStockModels = self::get()->getDelete(array(
            "product_id" => $data->params->values->product_id
        ));
        return $user_id;
    }
    public static function Info($data)
    {
        if($info = self::get()->from()->where(['product_id', '=', $data->params->values->product_id])->orderBy('id', 'DESC')->limit(1)->fetch()){
            return $info;
        }else{
            return true;
        }
    }
    public static function InfoStock($product_id)
    {
        if($info = self::get()->from()->where(['product_id', '=', $product_id])->orderBy('id', 'DESC')->limit(1)->fetch()){
            return $info[0];
        }else{
            return false;
        }
    }

    public static function tableName()
    {
        return 'shop_stock';
    }
}