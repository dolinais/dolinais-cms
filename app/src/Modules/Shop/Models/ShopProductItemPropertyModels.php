<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;

class ShopProductItemPropertyModels extends SelectPDOCore
{
    public static function Create($data, $token=null)
    {
        $id = self::get()->getInsert(array(
            'name' => $data->params->values->name,
            'value' => $data->params->values->value ?? md5(time()),
            'created_at' => time()
        ));
        return $id;
    }
    public static function Delete($data, $token=null)
    {
        $InsertPageId = self::get()->getDelete(array(
            "id" => $data->params->values->id
        ));
        return $id;
    }
    public static function Info($data, $token=null)
    {
        return self::get()->from()->where(['id'])->orderBy('name', 'ASC')->limit(1000)->fetch();
    }

    public static function tableName()
    {
        return 'shop_product_item_property';
    }
}