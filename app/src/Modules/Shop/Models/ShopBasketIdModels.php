<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;

class ShopBasketIdModels extends SelectPDOCore
{
    public static function Create($user_id)
    {
        $ShopBasketIdModels = self::get()->getInsert(array(
            'user_id' => $user_id,
            'basket_id' => self::Info($user_id)[0]->basket_id + 1,
            'created_at' => time(),
            'updated_at' => time()
        ));
        return $ShopBasketIdModels;
    }
    public static function Delete($user_id)
    {
        $InsertPageId = self::get()->getDelete(array(
            "user_id" => $user_id
        ));
        return $user_id;
    }
    public static function Info($user_id)
    {
        if(!self::get()->from()->where(['user_id', '=', $user_id])->orderBy('id', 'DESC')->limit(1)->fetch()){
            return  self::get()->getInsert(array(
                'user_id' => $user_id,
                'basket_id' => 1,
                'created_at' => time(),
                'updated_at' => time()
            ));
        }else{
            return self::get()->from()->where(['user_id', '=', $user_id])->orderBy('id', 'DESC')->limit(1)->fetch();
        }
    }

    public static function tableName()
    {
        return 'shop_basket_id';
    }
}