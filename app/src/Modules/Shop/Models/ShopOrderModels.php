<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Modules\Shop\Models\ShopModels;
use app\Modules\Shop\Models\ShopBasketModels;
use app\Modules\Shop\Models\ShopBasketIdModels;
use app\Modules\Shop\Models\ShopStockModels;
use app\Modules\Shop\Models\ShopStorageModels;
use app\Modules\Shop\Models\ShopOrderHistoryModels;
use app\Modules\Shop\Services\DataServiceShop;
use app\Modules\Account\Models\AccountModels;

class ShopOrderModels extends SelectPDOCore
{
    public static function Create($data, $token=null)
    {
        $userId = AccountModels::InfoUserAccessToken($token);
        $ShopBasketModels = ShopBasketModels::get()->findOne(
            array('user_id' => $userId)
        );
        foreach($ShopBasketModels as $ShopBasketModels){
            $md5key = md5($ShopBasketModels["basket_id"].$ShopBasketModels["user_id"].time());
            $ShopOrderModels = self::get()->getInsert(array(
                'order_id' => $md5key,
                'sclad_id' => $ShopBasketModels["sclad_id"],
                'user_id' => $ShopBasketModels["user_id"],
                'customer_id' => $ShopBasketModels["user_id"],
                'order_status_id' => htmlspecialchars(strip_tags($data->params->values->order_status_id ?? null)),
                'product_id' => $ShopBasketModels["product_id"],
                'product_count' => $ShopBasketModels["product_count"],
                'price' => $ShopBasketModels["price"],
                'total_price' => $ShopBasketModels["total_price"],
                'purchase_price' => $ShopBasketModels["purchase_price"],
                'payment_method' => $data->params->values->payment_method,
                'created_at' => time(),
                'updated_at' => time()
            ));
            ShopOrderHistoryModels::get()->getInsert(array(
                'order_id' => $md5key,
                'sclad_id' => $ShopBasketModels["sclad_id"],
                'user_id' => $ShopBasketModels["user_id"],
                'customer_id' => $ShopBasketModels["user_id"],
                'order_status_id' => htmlspecialchars(strip_tags($data->params->values->order_status_id ?? null)),
                'product_id' => $ShopBasketModels["product_id"],
                'product_count' => $ShopBasketModels["product_count"],
                'price' => $ShopBasketModels["price"],
                'total_price' => $ShopBasketModels["total_price"],
                'purchase_price' => $ShopBasketModels["purchase_price"],
                'payment_method' => $data->params->values->payment_method,
                'created_at' => time(),
                'updated_at' => time()
            ));
            ShopStockModels::UpdateOrder($ShopBasketModels["product_id"], $ShopBasketModels["product_count"]);
            ShopStorageModels::UpdateOrder($ShopBasketModels["sclad_id"], $ShopBasketModels["product_count"]);
        }
        ShopBasketIdModels::Create($userId);
        ShopBasketModels::get()->getDelete(array(
            "user_id" => $userId
        ));
        return $md5key;
    }
    public static function Update($data)
    {
        $info = self::get()->findOne(
            array('id' => $data->params->values->id)
        );
        $ShopOrderModels = self::get()->getUpdate(
            array(
                'product_id' => htmlspecialchars(strip_tags($data->params->values->product_id ?? null)),
                'product_count' => htmlspecialchars(strip_tags($data->params->values->product_count ?? null)),
                'total_price' => htmlspecialchars(strip_tags($info[0]["price"] * $data->params->values->product_count)),
                'updated_at' => time()
            ),
            $data->params->values->id
        );
        return $data->params->values->id;
    }
    public static function Delete($data)
    {
        $InsertPageId = self::get()->getDelete(array(
            "order_id" => htmlspecialchars(strip_tags($data->params->values->order_id ?? null))
        ));
        return $data->params->values->id;
    }
    public static function Info($data, $token=null)
    {
        if(isset($data)){
            return DataServiceShop::InitSearch($data, $token);
        }
    }

    public static function tableName()
    {
        return 'shop_order';
    }
}