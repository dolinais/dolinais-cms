<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Core\PaginationCore;
use app\Core\LogCore;
use app\Modules\Shop\Models\ShopStockModels;
use app\Modules\Shop\Models\ShopStorageModels;

class ShopModels extends SelectPDOCore
{
    public $title;
    public $description;
    public $category_id;
    public $status;
    public $code;
    public $barcode;
    public $slug;
    public $files_post;
    public $price;
    public $sorting;

    public function __construct($data){
        if (isset($data->params->values->title)) {
            $this->title = htmlspecialchars(strip_tags($data->params->values->title));
        }else{
            echo null;
        }
        if (isset($data->params->values->description)) {
            $this->description = htmlspecialchars(strip_tags($data->params->values->description));
        }else{
            echo null;
        }
        if (isset($data->params->values->category_id)) {
            $this->category_id = htmlspecialchars(strip_tags($data->params->values->category_id));
        }else{
            echo null;
        }
        if (isset($data->params->values->status)) {
            $this->status = htmlspecialchars(strip_tags($data->params->values->status));
        }else{
            echo null;
        }
        if (isset($data->params->values->code)) {
            $this->code = htmlspecialchars(strip_tags($data->params->values->code));
        }else{
            echo null;
        }
        if (isset($data->params->values->barcode)) {
            $this->barcode = htmlspecialchars(strip_tags($data->params->values->barcode));
        }else{
            echo null;
        }
        if (isset($data->params->values->slug)) {
            $this->slug = htmlspecialchars(strip_tags($data->params->values->slug));
        }else{
            echo null;
        }
        if (isset($data->params->values->files_post)) {
            $this->files_post = htmlspecialchars(strip_tags($data->params->values->files_post));
        }else{
            echo null;
        }
        if (isset($data->params->values->price)) {
            $this->price = htmlspecialchars(strip_tags($data->params->values->price));
        }else{
            echo null;
        }
        if (isset($data->params->values->sorting)) {
            $this->sorting = htmlspecialchars(strip_tags($data->params->values->sorting));
        }else{
            return 500;
        }
    }

    public static function Create($data)
    {
        $validate = new self($data);
        $InsertPageId = self::get()->getInsert(array(
            'site_id' => 1,
            'title' => $validate->title,
            'description' => $validate->description,
            'category_id' => $validate->category_id,
            'status' => $validate->status,
            'slug' => $validate->slug,
            'files_post' => $validate->files_post,
            'code' => $validate->code,
            'barcode' => $validate->barcode,
            'price' => $validate->price,
            'sorting' => 500,
            'created_at' => time()
        ));
        return $InsertPageId;
    }

    public static function Update($data)
    {
        $InsertPageId = self::get()->getUpdate(
            array(
                'title' => htmlspecialchars(strip_tags($data->params->values->title ?? null)),
                'description' => htmlspecialchars(strip_tags($data->params->values->description ?? null)),
                'category_id' => htmlspecialchars(strip_tags($data->params->values->category_id ?? null)),
                // 'tree_id' => htmlspecialchars(strip_tags($data->params->values->tree_id ?? null)),
                'files_post' => htmlspecialchars(strip_tags($data->params->values->files_post ?? null)),
                'status' => htmlspecialchars(strip_tags($data->params->values->status ?? null)),
                'slug' => htmlspecialchars(strip_tags(Translit::en($data->params->values->title ?? null))),
                'code' => htmlspecialchars(strip_tags($data->params->values->code ?? null)),
                'barcode' => htmlspecialchars(strip_tags($data->params->values->barcode ?? null)),
                'price' => htmlspecialchars(strip_tags($data->params->values->price ?? null)),
                'sorting' => 100,
                'updated_at' => time()
            ),
            $data->params->values->id
        );
        return $data->params->values->id;
    }

    public static function Delete($data)
    {
        $InsertPageId = self::get()->getDelete(array(
            "id" => htmlspecialchars(strip_tags($data->params->values->id ?? null))
        ));
        return $data->params->values->id;
    }

    public static function CatalogInfo($data)
    {
        $output = array();
        // LogCore::Log('log', $data->params->values->title);
        if(isset($data->params->values->title) && $data->params->values->title != null){
            foreach(self::get()->from()->like('title', $data->params->values->title)->limit(10)->orderBy('id', 'DESC')->fetch() as $product){
                foreach(ShopStorageModels::get()->from()->where(['product_id', '=', $product->id])->orderBy('id', 'DESC')->fetch() as $ShopStorageModels){
                        $output[] = array(
                        "id" => $product->id,
                        "title" => $product->title,
                        "sclad_id" => $ShopStorageModels->id,
                        "category_id" => $product->category_id,
                        "description" => $product->description,
                        "barcode" => $product->barcode.' '.date('d.m.Y', $ShopStorageModels->created_at),
                        "sku" => $product->sku,
                        "files_post" => $product->files_post,
                        "price" => $ShopStorageModels->total_price,
                        "quantity" => $ShopStorageModels->quantity ?? 0
                    );
                }

            }
            return $output;
        }

        if(isset($data->params->values->id) && $data->params->values->id != null){
            return self::get()->findOne(
                array('id' => $data->params->values->id)
            );
        }
        if(isset($data->params->values->per_page) && $data->params->values->per_page != ""){
            $total = self::get('COUNT(id) as count')->from('id')->where(['status', '=', 1])->limit($data->params->values->per_page)->fetch()[0]->count;
            $pagination = new PaginationCore($data->params->values->cur_page, $total, $data->params->values->per_page);

            if($pagination->getTotalPage() < $data->params->values->cur_page){
                return array(array(
                    "pagination" => range(1, $pagination->getTotalPage()),
                    "id" => 0,
                    "title" => ' ',
                    "description" => ' ',
                    "barcode" => ' ',
                    "sku" => ' ',
                    "files_post" => ' ',
                    "price" => ' ',
                    "quantity" => ' '
                ));
            }

            foreach (self::get()->from()->where(['status', '=', 1])->orderBy('id', 'ASC')->limit("{$pagination->offSet()}, {$data->params->values->per_page}")->fetch() as $product){

                $output[] = array(
                    "pagination" => range(1, $pagination->getTotalPage()),
                    "paginationTotalPage" => $pagination->getTotalPage(),
                    "id" => $product->id,
                    "title" => $product->title,
                    "description" => $product->description,
                    "barcode" => $product->barcode,
                    "sku" => $product->sku,
                    "files_post" => $product->files_post,
                    "price" => $product->price,
                    "quantity" => ShopStockModels::InfoStock($product->id)->quantity ?? 0
                );
            }
            return $output;
        }
    }

    public static function Info($data)
    {
        return self::get()->findOne(
            array('id' => $data->params->values->id)
        );
    }

    public static function CategoryInfo($data)
    {
        return self::get()->findOne(
            array('category_id' => $data->params->values->category_id)
        );
    }

    public static function CategoryIDInfo($id)
    {
        return self::get()->findOne(
            array('category_id' => $id)
        );
    }

    public static function ProductInfo($product_id)
    {
        if($info = self::get()->from()->where(['id', '=', $product_id])->orderBy('id', 'DESC')->limit(1)->fetch()){
            return $info[0];
        }else{
            return true;
        }
    }

    public static function tableName()
    {
        return 'shop_product';
    }
}