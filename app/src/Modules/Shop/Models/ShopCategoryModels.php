<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Modules\Account\Models\AccountModels;

class ShopCategoryModels extends SelectPDOCore
{
    public static function Create($data, $token=null)
    {
        $userId =  AccountModels::InfoUserAccessToken($token);
        $ShopCategoryID = self::get()->getInsert(array(
            'title' => htmlspecialchars(strip_tags($data->params->values->title ?? null)),
            'description' => htmlspecialchars(strip_tags($data->params->values->description ?? null)),
            'tree_id' => htmlspecialchars(strip_tags($data->params->values->tree_id ?? null)),
            'code' => htmlspecialchars(strip_tags(Translit::en($data->params->values->title ?? null))),
            'slug' => htmlspecialchars(strip_tags(Translit::en($data->params->values->title ?? null))),
            'sorting' => htmlspecialchars(strip_tags($data->params->values->sorting ?? null)),
            'status' => htmlspecialchars(strip_tags($data->params->values->status ?? null)),
            'user_id' => $userId,
            'site_id' => 1,
            'created_at' => time()
        ));
        return $ShopCategoryID;
    }
    public static function Update($data)
    {
        $info = self::get()->findOne(
            array('id' => $data->params->values->id)
        );
        $ShopOrderModels = self::get()->getUpdate(
            array(
                'title' => htmlspecialchars(strip_tags($data->params->values->title ?? null)),
                'description' => htmlspecialchars(strip_tags($data->params->values->description ?? null)),
                'tree_id' => htmlspecialchars(strip_tags($data->params->values->tree_id ?? null)),
                'slug' => htmlspecialchars(strip_tags(Translit::en($data->params->values->title ?? null))),
                'code' => htmlspecialchars(strip_tags($data->params->values->code ?? null)),
                'sorting' => htmlspecialchars(strip_tags($data->params->values->sorting ?? 500)),
                'status' => htmlspecialchars(strip_tags($data->params->values->status ?? 1)),
                'user_id' => AccountModels::InfoUserAccessToken($token),
                'updated_at' => time()
            ),
            $data->params->values->id
        );
        return $data->params->values->id;
    }
    public static function Delete($data)
    {
        $InsertPageId = self::get()->getDelete(array(
            "id" => htmlspecialchars(strip_tags($data->params->values->id ?? null))
        ));
        return $data->params->values->id;
    }
    public static function Info($data=null)
    {
        $new = array();
        $category = json_decode(json_encode(self::get()->from()->where(['status', '=', 1])->fetch()), true);
        foreach ($category as $a){
        $new[$a['tree_id']][] = $a;
        }

        return self::createTree($new, array(json_decode(json_encode(self::get()->from()->where(['tree_id', '=', 0])->fetch()), true))[0]);
    }

    public static function createTree(&$list, $parent){
        $tree = array();
        foreach ($parent as $k=>$l){
            if(isset($list[$l['id']])){
                $l['children'] = self::createTree($list, $list[$l['id']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }

    public static function CategoryIDInfo($data)
    {
        return self::get()->findOne(
            array('id' => $data->params->values->category_id)
        );
    }

    public static function CategoryID($id)
    {
        return self::get()->findOne(
            array('id' => $id)
        );
    }

    public static function Type($data, $token=null)
    {
        return self::get()->from()->where([$data->params->values->type, '=', '"'.$data->params->values->value.'"'])->orderBy('id', 'ASC')->limit(1000)->fetch();
    }

    public static function tableName()
    {
        return 'shop_category';
    }
}