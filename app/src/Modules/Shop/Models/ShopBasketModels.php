<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Modules\Shop\Models\ShopBasketIdModels;
use app\Modules\Shop\Models\ShopStockModels;
use app\Modules\Shop\Models\ShopStorageModels;
use app\Modules\Shop\Models\ShopModels;
use app\Modules\Account\Models\AccountModels;

class ShopBasketModels extends SelectPDOCore
{
    public static function Create($data, $token=null)
    {
        $userId = AccountModels::InfoUserAccessToken($token);

        if(isset(ShopBasketIdModels::Info($userId)[0]->basket_id)){
            $basketId = ShopBasketIdModels::Info($userId)[0]->basket_id;
        }else{
            $basketId = 1;
        }
        if(isset($data->params->values->sclad_id)){
            $ScladID = ShopStorageModels::InfoStock($data->params->values->sclad_id);
        }

        foreach(self::get()->findOne(
            array('user_id' => $userId)
        ) as $basket){
            if(isset($data->params->values->sclad_id) && $basket["sclad_id"] == $ScladID->id){
                if($basket["product_count"] == $ScladID->quantity){
                    return true;
                }
                $productcount = $basket["product_count"] + 1;
                $ShopBasketModels = self::get()->getUpdate(
                    array(
                        'product_count' => $productcount,
                        'total_price' => htmlspecialchars(strip_tags($basket["price"] * $productcount - $basket["discount"])),
                        'updated_at' => time()
                    ),
                    $basket["id"]
                );
                return true;
            }

            if(isset($data->params->values->barcode_id) && $data->params->values->barcode_id == $basket["product_barcode"]){
                if($basket["product_count"] == ShopStorageModels::InfoStock($basket["sclad_id"])->quantity){
                    return true;
                }
                foreach(ShopModels::get()->findOne(
                    array('barcode' => $data->params->values->barcode_id)
                ) as $ShopModels){
                    if(count(ShopStorageModels::get()->findOne(array('product_id' => $ShopModels["id"]))) > 1){
                        return array(
                            'count' => 'true',
                            'title' => $ShopModels["title"],
                            'product_id' => $ShopModels["id"]
                        );
                    }
                }
                $productcount = $basket["product_count"] + 1;
                $ShopBasketModels = self::get()->getUpdate(
                    array(
                        'product_count' => $productcount,
                        'total_price' => htmlspecialchars(strip_tags($basket["price"] * $productcount - $basket["discount"])),
                        'updated_at' => time()
                    ),
                    $basket["id"]
                );
                return true;
            }
        }
        if(isset($data->params->values->barcode_id)){
            foreach(ShopModels::get()->findOne(
                array('barcode' => $data->params->values->barcode_id)
            ) as $ShopModels){
                if(count(ShopStorageModels::get()->findOne(array('product_id' => $ShopModels["id"]))) > 1){
                    return array(
                        'count' => 'true',
                        'title' => $ShopModels["title"],
                        'product_id' => $ShopModels["id"]
                    );
                }else{
                    if(ShopStorageModels::ProductIDInfo($ShopModels["id"])[0]->quantity == 0){
                        return true;
                    }
                    $sclad_id = ShopStorageModels::ProductIDInfo($ShopModels["id"])[0];
                    $ShopBasketModels = self::get()->getInsert(array(
                        'basket_id' => $basketId,
                        'sclad_id' => $sclad_id->id,
                        'user_id' => $userId,
                        'product_id' => $ShopModels["id"],
                        'product_name' => $ShopModels["title"],
                        'product_barcode' => $ShopModels["barcode"],
                        'product_count' => 1,
                        'price' => $sclad_id->total_price,
                        'total_price' => $sclad_id->total_price,
                        'purchase_price' => $sclad_id->price,
                        'created_at' => time(),
                        'updated_at' => time()
                    ));
                    if(!$ShopBasketModels){
                        return 0;
                    }
                    return $ShopBasketModels;
                }
            }
        }

        if(isset($data->params->values->sclad_id)){
            if($sclad_id = ShopStorageModels::InfoStock($data->params->values->sclad_id)){
                foreach(ShopModels::get()->findOne(
                    array('id' => $sclad_id->product_id)
                ) as $ShopModels){
                    if($sclad_id->quantity == 0){
                        return true;
                    }

                    $ShopBasketModels = self::get()->getInsert(array(
                        'basket_id' => $basketId,
                        'sclad_id' => $data->params->values->sclad_id,
                        'user_id' => $userId,
                        'product_id' => $sclad_id->product_id,
                        'product_name' => $ShopModels["title"],
                        'product_barcode' => $ShopModels["barcode"],
                        'product_code' => $ShopModels["code"],
                        'product_count' => 1,
                        'price' => $sclad_id->total_price,
                        'total_price' => $sclad_id->total_price,
                        'purchase_price' => $sclad_id->price,
                        'created_at' => time(),
                        'updated_at' => time()
                    ));
                    return $ShopBasketModels;
                }
            }
        }
    }
    public static function Update($data, $token=null)
    {
        $info = self::get()->findOne(
            array('id' => $data->params->values->id)
        );
        // return ShopStockModels::InfoStock($info[0]["product_id"])->quantity;
        if($data->params->values->product_count > ShopStockModels::InfoStock($info[0]["product_id"])->quantity){
            return false;
        }
        $ShopBasketModels = self::get()->getUpdate(
            array(
                'product_count' => htmlspecialchars(strip_tags($data->params->values->product_count ?? null)),
                'total_price' => htmlspecialchars(strip_tags($info[0]["price"] * $data->params->values->product_count - $info[0]["discount"])),
                'updated_at' => time()
            ),
            $data->params->values->id
        );
        return $data->params->values->id;
    }
    public static function Delete($data)
    {
        $InsertPageId = self::get()->getDelete(array(
            "id" => htmlspecialchars(strip_tags($data->params->values->id ?? null))
        ));
        return $data->params->values->id;
    }
    public static function Info($data, $token=null)
    {
        $userId = AccountModels::InfoUserAccessToken($token);
        $product = self::get()->findOne(
            array('user_id' => $userId)
        );
        $output = array();
        foreach ($product as $product){
            $allprice[] = $product["total_price"];
            $output[] = array(
                "id" => $product["id"],
                "user_id" => $product["user_id"],
                "basket_id" => $product["basket_id"],
                "price" => $product["price"],
                "product_count" => $product["product_count"],
                "product_id" => $product["product_id"],
                "sclad_id" => $product["sclad_id"],
                "product_name" => $product["product_name"],
                "product_barcode" => ShopModels::ProductInfo($product["product_id"])->barcode,
                "total_price" => $product["total_price"],
                "discount" => $product["discount"],
                "quantity" => ShopStorageModels::InfoStock($product["sclad_id"])->quantity,
                "allprice" => array_sum($allprice)

            );
        }
        return $output;
    }

    public static function tableName()
    {
        return 'shop_basket';
    }
}