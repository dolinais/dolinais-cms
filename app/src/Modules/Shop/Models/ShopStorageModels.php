<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Modules\Shop\Models;

use app\Core\SelectPDOCore;
use app\Core\Translit;
use app\Modules\Account\Models\AccountModels;
use app\Modules\Shop\Models\ShopStockModels;
use app\Modules\Shop\Models\ShopModels;

class ShopStorageModels extends SelectPDOCore
{
    public static function Create($data, $token=null)
    {
        $userId = AccountModels::InfoUserAccessToken($token);
        $ShopStorageModels = self::get()->getInsert(array(
            'product_id' => $data->params->values->product_id,
            'price' => $data->params->values->price,
            'total_price' => $data->params->values->total_price,
            'quantity' => $data->params->values->quantity,
            'comment' => $data->params->values->commentstock,
            'user_id' => $userId,
            'created_at' => time()
        ));
        ShopModels::get()->getUpdate(
            array(
                'price' => $data->params->values->total_price,
                'updated_at' => time()
            ),
            $data->params->values->product_id
        );
        if($stock = ShopStockModels::get()->findOne(
            array('product_id' => $data->params->values->product_id)
        )){
            foreach($stock as $productid){
                if($data->params->values->product_id == $productid["product_id"]){
                    $productcount = $productid["quantity"] + $data->params->values->quantity;
                    $ShopBasketModels = ShopStockModels::get()->getUpdate(
                        array(
                            'price' => $data->params->values->price,
                            'total_price' => $data->params->values->total_price,
                            'quantity' => $productcount,
                            'updated_at' => time()
                        ),
                        $productid["id"]
                    );
                    return true;
                }
            }
        }
        if(!$stock){
            ShopStockModels::get()->getInsert(array(
                'product_id' => $data->params->values->product_id,
                'price' => $data->params->values->price,
                'total_price' => $data->params->values->total_price,
                'quantity' => $data->params->values->quantity,
                'created_at' => time()
            ));
        }

        return $ShopStorageModels;
    }
    public static function Update($data)
    {
        return true;
    }
    public static function UpdateOrder($id, $quantity){
        $info = self::get()->findOne(
            array('id' => $id)
        );
        $ShopStockModels = self::get()->getUpdate(
            array(
                'quantity' => htmlspecialchars(strip_tags($info[0]["quantity"] - $quantity)),
                'updated_at' => time()
            ),
            $info[0]["id"]
        );
    }
    public static function Delete($data)
    {
        $storage = self::InfoStock($data->params->values->storage_id);
        if(!$storage){
            return true;
        }

        $ShopStockModels = self::get()->getDelete(array(
            "id" => $data->params->values->storage_id
        ));

        foreach(ShopStockModels::get()->findOne(
            array('product_id' => $storage->product_id)
        ) as $productid){
            if($storage->product_id == $productid["product_id"]){
                $productcount = $productid["quantity"] - $storage->quantity;
                $ShopBasketModels = ShopStockModels::get()->getUpdate(
                    array(
                        'quantity' => $productcount,
                        'updated_at' => time()
                    ),
                    $productid["id"]
                );
                return true;
            }else{
                return true;
            }
        }
        return true;
    }
    public static function Info($data)
    {
        if($info = self::get()->from()->where(['product_id', '=', $data->params->values->product_id])->orderBy('id', 'DESC')->fetch()){
            return $info;
        }else{
            return array();
        }
    }
    public static function ProductIDInfo($product_id)
    {
        if($info = self::get()->from()->where(['product_id', '=', $product_id])->orderBy('id', 'DESC')->fetch()){
            return $info;
        }else{
            return array();
        }
    }
    public static function InfoStock($id)
    {
        if($info = self::get()->from()->where(['id', '=', $id])->orderBy('id', 'DESC')->limit(1)->fetch()){
            return $info[0];
        }else{
            return true;
        }
    }

    public static function tableName()
    {
        return 'shop_storage';
    }
}