<div class="alert alert-success alert-dismissible fade show notifications" role="alert" id="notificationsdisplay">
    <span style="font-size: 12px; cursor: pointer;" onclick="linksOpen('/messenger?sel=2')">перейти в чат.</span>
    <h5 class="alert-heading">Дарья Фролова <span style="font-size: 9px;" id="useronline"></span></h5>
    <span style="
            position: fixed;
            width: 350px;
            margin-top: -10px;
            padding-bottom: 10px;
            font-size: 12px;
            text-align: center;
        " id="newmessagerinfo"></span>
    <div class="newmessager" id="newmessager">
        <?php foreach(app\Models\MessengerModels::get()->findAll() as $MessengerModels ): ?>
        <?php if($MessengerModels['user_id'] == 1): ?>
        <div class="d-flex flex-row p-3">
            <img src="https://img.icons8.com/color/48/000000/circled-user-male-skin-type-7.png" width="30" height="30">
            <div class="chat ml-2 p-3"><?=$MessengerModels['message']; ?>
                <span style="margin-top: 10px; float: right; margin-left: 50px;"><?=date('H:i'); ?></span>
            </div>
        </div>
        <?php elseif($MessengerModels['user_id'] == 2): ?>
        <div class="d-flex flex-row p-3 transition_<?=$MessengerModels['id']; ?>" id="transition_<?=$MessengerModels['id']; ?>">
            <img src="https://img.icons8.com/color/48/000000/circled-user-male-skin-type-7.png" width="30" height="30">
            <div class="chat ml-2 p-3 bg-white"><span class="text-muted"><?=$MessengerModels['message']; ?></span>
                <span style="margin-top: 10px; float: right; margin-left: 50px;"><?=date('H:i'); ?></span>
            </div>
            <span style="position: relative; font-size: 12px;">
                <i class="fas fa-check ms-2" style="color:#a7aba8;"></i>
                <i class="fas fa-check ms-2" style="color:#a7aba8; float: left; margin-right: -23px;"></i>
            </span>
        </div>
        <?php endif; ?>
        <?php endforeach; ?>
        <hr>

        <div id="scrolling"></div>
        <div id="notifications"><div class="d-flex flex-row p-3" id="transition"></div></div>
        <div class="im-activity" id="im-activity">
            <div id="fast"></div>
            <div id="regular"></div>
            <div id="slow"></div>
            <div id="typeT"><span id="typeTuserName"></span> печатает</div>
        </div>
        <div id="messageInfoData" style="
            font-size: 10px;
            text-align: center;
        ">Нет новых сообщений</div>
    </div>

    <button type="button" class="btn-close" onclick="CloseEl(document.querySelector('#notificationsdisplay'))"></button>
    <input type="text" class="messageBox" id="messageBox" placeholder="Напишите что нибудь..."/>
    <button id="send" class="buttonBox" title="Отправить">⎆</button>
</div>
<script type="text/javascript" src="/resources/js/main.js" ></script>
<script type="text/javascript" src="/resources/js/nodeServer.js" ></script>
<style>
.im-activity{
    margin: auto; max-width: 500px; margin-left: 10px; display: none;
}
#typeT {
    float: left; margin-right: 10px; font-size: 10px; font-size: 11px;
    opacity: 0.3;
}
#fast {
    width: 2px; height: 2px; float: left; margin-right: 10px; margin-top: 8px; box-shadow: 1px 1px 5px #333;
    animation:             blink 800ms linear infinite;
        -moz-animation:    blink 800ms linear infinite;
        -webkit-animation: blink 800ms linear infinite;
}

#regular {
    width: 2px; height: 2px; float: left; margin-right: 10px; margin-top: 8px; box-shadow: 1px 1px 5px #333;
    animation:             blink 850ms linear infinite;
        -moz-animation:    blink 850ms linear infinite;
        -webkit-animation: blink 850ms linear infinite;
}

#slow {
    width: 2px; height: 2px; float: left; margin-right: 10px; margin-top: 8px; box-shadow: 1px 1px 5px #333;
    animation:             blink 900ms linear infinite;
        -moz-animation:    blink 900ms linear infinite;
        -webkit-animation: blink 900ms linear infinite;
}

@keyframes             blink { 0% {opacity:0;} 50% {opacity:1;} 100% {opacity:0;} }
    @-moz-keyframes    blink { 0% {opacity:0;} 50% {opacity:1;} 100% {opacity:0;} }
    @-webkit-keyframes blink { 0% {opacity:0;} 50% {opacity:1;} 100% {opacity:0;} }

.transition {
    background: #d0eddb;
}

.newmessager{
    height: 350px;
    overflow: auto;
    scrollbar-heidht: none;
}
.newmessager::-webkit-scrollbar {
    display: none;
}
.time{
    position: absolute;
    margin-top: 10px;
    float: right;
    margin-left: 50px;
}
.notifications{
    position: fixed;
    right: 0;
    bottom: 0;
    height: 500px;
/*        display: none;*/
    width: 400px;
    cursor: move;
    z-index: 9;
}
.preenotifications{
    background: #fff;
    padding: 10px;
}
.messageBox{
    display: block;
    width: 350px;
    height: 50px;
    margin-bottom: 10px;
    padding: 10px;
    bottom: 0;
    position: absolute;
}
.buttonBox{
    width: 50px;
    height: 50px;
    position: absolute;
    margin-bottom: 10px;
    padding: 10px;
    right: 0;
    bottom: 0;
    margin-right: 27px;
    border: none;
    background: none;
}

.adiv{
    background: #04CB28;
    margin-top: -80px;
    border-radius: 15px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
    font-size: 12px;
    height: 46px;
}
.chat{
    border: none;
    background: #E2FFE8;
/*    min-width: 200px;*/
    font-size: 10px;
    border-radius: 20px;
    word-wrap: break-word;
    overflow-wrap: break-word;
    overflow: hidden;
}
.bg-white{
    border: 1px solid #E7E7E9;
    font-size: 10px;
    border-radius: 20px;
}
.myvideo img{
    border-radius: 20px
}
.dot{
    font-weight: bold;
}
.form-control{
    /*border-radius: 12px;
    border: 1px solid #F0F0F0;
    font-size: 8px;*/
}
.form-control:focus{
    box-shadow: none;
    }
.form-control::placeholder{
    /*font-size: 8px;
    color: #C4C4C4;*/
}
</style>