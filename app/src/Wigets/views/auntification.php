<!-- Modal Auntification -->
<div class="modal fade" id="exampleModalReg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Авторизация</h5>
        <button type="button" class="btn-close" data-mdb-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <form action="/account/auth/" method="post">
          <!-- Email input -->
          <div class="form-outline mb-4">
            <label class="form-label" for="form2Example1">Телефон</label>
            <input type="text" name="username" id="form2Example1" class="form-control" />
          </div>

          <!-- Password input -->
          <div class="form-outline mb-4">
            <label class="form-label" for="form2Example2">Пароль</label>
            <input type="password" name="password" id="form2Example2" class="form-control" />
          </div>

          <!-- 2 column grid layout for inline styling -->
          <div class="row mb-4">
            <div class="col d-flex justify-content-center">
              <!-- Checkbox -->
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="form2Example31" checked />
                <label class="form-check-label" for="form2Example31"> Запомнить меня </label>
              </div>
            </div>

            <div class="col">
              <!-- Simple link -->
              <a href="#!">Забыли пароль?</a>
            </div>
          </div>

          <!-- Submit button -->
          <button type="submit" class="btn btn-primary btn-block mb-4">Авторизоваться</button>

          <!-- Register buttons -->
          <div class="text-center">
            <p>Или:</p>
            <button type="button" class="btn btn-link btn-floating mx-1">
              <i class="fab fa-telegram" onclick="location.href='tg://resolve?domain=dev_dolinaisbot&start=123';"></i>
            </button>

            <button type="button" class="btn btn-link btn-floating mx-1">
              <i class="fab fa-vk"></i>
            </button>

            <button type="button" class="btn btn-link btn-floating mx-1">
              <i class="fab fa-odnoklassniki"></i>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Auntification -->