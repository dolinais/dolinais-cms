<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Widgets;

use app\Modules\Page\Models\PageModels;

class IndexWiget
{

	public function __construct()
    {
        ob_start(function($buffer){
                if(preg_match_all("|\[\[blocks.title\.(.*)\]\]|U", $buffer, $matches))
                {
                    if (!empty($matches[1])) {
                        foreach ($matches[1] as $row) {
                            $PageModels = PageModels::get()->findOne(
                                array('id' => $row)
                            );
                            foreach($PageModels as $content){
                                $buffer = str_ireplace('[[blocks.title.' .  $row . ']]', $content['title'], $buffer);
                            }
                        }
                    }
                }
                if(preg_match_all("|\[\[blocks.description\.(.*)\]\]|U", $buffer, $matches))
                {
                    if (!empty($matches[1])) {
                        foreach ($matches[1] as $row) {
                            $PageModels = PageModels::get()->findOne(
                                array('id' => $row)
                            );
                            foreach($PageModels as $content){
                                $buffer = str_ireplace('[[blocks.description.' .  $row . ']]', $content['m_description'], $buffer);
                            }
                        }
                    }
                }
                if(preg_match_all("|\[\[blocks.text\.(.*)\]\]|U", $buffer, $matches))
                {
                    if (!empty($matches[1])) {
                        foreach ($matches[1] as $row) {
                            $PageModels = PageModels::get()->findOne(
                                array('id' => $row)
                            );
                            foreach($PageModels as $content){
                                $buffer = str_ireplace('[[blocks.text.' .  $row . ']]', $content['description'], $buffer);
                            }
                        }
                    }
                }
                return $buffer;
            }
        );
  	}
}