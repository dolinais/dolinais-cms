<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

namespace app\Commands;

use app\Models\TreePageModels;

class IndexCommands
{
	public function __construct()
    {
		/**
		 * Обращаемся к базе данных, смотрим существующие страницы (модули)
		 */
        $module = TreePageModels::get()->findOne(
        	array(
        		'page_status' => 1,
        		'page_type' => 'page_modules'
        	)
        );

		if(!$module){
			return 500;
		}

		foreach($module as $module_id){
			if($module_id["module_id"] != NULL){
				$className = [
				    'modules' => ['\app\Modules\\'.ucfirst($module_id["module_id"]).'\\Commands\\'.ucfirst($module_id["module_id"]).'Commands', 'actionIndex'],
				    'files' => CONSOLEMODULES . '/' .ucfirst($module_id["module_id"]) .'/Commands/'.ucfirst($module_id["module_id"]).'Commands.php'
				];
				if (file_exists($className["files"])) {
					return new $className["modules"][0];
				}
			}
		}
    }
}