<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

class DolinaIS extends app\Core\ConfigCore
{
    public static $app;
    public static $copyright = 'Dolina IS CMS';
    public static $version = '0.0.1';
}
