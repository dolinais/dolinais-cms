<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

return [
    "db" => [
        "host" => "localhost",
        "dbname" => "dolinaiscms",
        "username" => "dolinais",
        "password" => 123456
    ]
];
