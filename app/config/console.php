<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

return [
    "modules" => [
        "websocket" => 'app\Modules\Websocket\Commands\IndexWebsocketCommands'
    ]
];
