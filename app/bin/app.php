<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);


$_SERVER[' DOCUMENT_ROOT '] = realpath(__DIR__ . '/../../');


define('APP', $_SERVER[' DOCUMENT_ROOT '] . '/app/src');
define('CONSOLEROOT', $_SERVER[' DOCUMENT_ROOT ']);
define('CONSOLEAPP', $_SERVER[' DOCUMENT_ROOT '] . '/app');
define('CONSOLESRC', $_SERVER[' DOCUMENT_ROOT '] . '/app/src');
define('CONSOLECONFIG', $_SERVER[' DOCUMENT_ROOT '] . '/app/config');
define('CONSOLEMODULES', $_SERVER[' DOCUMENT_ROOT '] . '/app/src/Modules');
define('CONSOLETEMPLATES', $_SERVER[' DOCUMENT_ROOT '] . '/app/src/templates');
define('CONSOLEUPLOADS', $_SERVER[' DOCUMENT_ROOT '] . '/WWW/uploads');
define('CONSOLERESOURCES', $_SERVER[' DOCUMENT_ROOT '] . '/WWW/resources');
define('CONSOLEVENDOR', $_SERVER[' DOCUMENT_ROOT '] . '/app/vendor');

require(APP.'/ClassesAutoloader.php');
require(CONSOLEVENDOR . '/autoload.php');
require(CONSOLESRC . '/env.php');
require(CONSOLECONFIG . '/config.php');

use Symfony\Component\Dotenv\Dotenv;


if (file_exists(CONSOLECONFIG. '/.env')) {
    (new Dotenv())->usePutenv()->load(CONSOLECONFIG . '/.env');
}

require(APP . '/Commands/IndexCommands.php');

new app\Commands\IndexCommands();
