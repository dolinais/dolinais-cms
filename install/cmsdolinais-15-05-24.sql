-- --------------------------------------------------------
-- Хост:                         192.168.88.252
-- Версия сервера:               10.11.6-MariaDB-0+deb12u1 - Debian 12
-- Операционная система:         debian-linux-gnu
-- HeidiSQL Версия:              12.7.0.6850
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дамп структуры базы данных dolinais
CREATE DATABASE IF NOT EXISTS `dolinais` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `dolinais`;

-- Дамп структуры для таблица dolinais.account
CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `phone` bigint(12) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `patronymic` varchar(100) DEFAULT NULL,
  `birthday` int(11) DEFAULT 0,
  `role` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `password_hash` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT 0,
  `sonline` int(11) DEFAULT 0,
  `created_at` int(11) DEFAULT 0,
  `updated_at` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Дамп данных таблицы dolinais.account: ~6 rows (приблизительно)
INSERT INTO `account` (`id`, `username`, `phone`, `email`, `first_name`, `last_name`, `patronymic`, `birthday`, `role`, `status`, `password_hash`, `auth_key`, `password_reset_token`, `accessToken`, `code`, `sonline`, `created_at`, `updated_at`) VALUES
	(1, '79200887766', 79200887766, NULL, 'Игорь', 'Меденцев', NULL, 0, NULL, 0, '$2y$10$lBqT0jwBvO7gsIRRm1X67e9pgmspLNND77OBFUe3Sqz0Z2VDU28cO', '405d27af11a43cb537560ed039877bf3', NULL, '405d27af11a43cb537560ed039877bf3', 0, 1, 1695388947, 1699701973),
	(2, '79258407070', 79258407070, NULL, 'Дарья', 'Меденцева', NULL, 0, NULL, 0, '$2y$10$fL33Kqf2CMtJ4JtPNRXcwOf9YSRcON6350sIkQVUq0T8mfdNjpug.', 'c3f00375d7652df2eb1b190b27f2038a', NULL, 'c3f00375d7652df2eb1b190b27f2038a', 0, 0, 1695388984, 1699690148),
	(3, '79155003020', 79155003020, NULL, 'Вадим', 'Меденцев', NULL, 0, NULL, 0, '$2y$10$aWsiH5hr22sNo.j1X0ruCOf4lx5Qc2xJkGK2NIsOAjmiPD.eHAKC.', '9a4dcef22cab707d3db2715846c59227', NULL, '9a4dcef22cab707d3db2715846c59227', 0, 0, 1695389079, 1695389079),
	(4, '12121', 12121, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$yZOFl5wRbGgGvVb3NxgaKOy8uyqwxYfGa.ftR3X6kL4cCcjJn9JkC', 'b83ce14726b999168b0cf97b8caa70f8', NULL, NULL, 0, 0, 1705250589, 1705250589),
	(5, '123123', 123123, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$Bo7Et2VeAMHAvRDA8lbdgeJRRU0vRRRWS8XX9M5rGFMjdXiBG9nNi', '88cc035cdae664eeb087cc938388e252', NULL, NULL, 0, 0, 1705250601, 1705250601),
	(6, '12345', 12345, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$xzWEEsLJtWG9LtYoFBfj0O3fUMORo3pludbUY54J0EuyfSk/7vRh.', '971a074aab1f106285b12dd3b36a129f', NULL, NULL, 0, 0, 1705250635, 1705250635),
	(7, '+79200887766', 79200887766, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$4JxiDA58US0SBq.FvJUVouynG29MfwqSVQzRkQ74bo6YM3P0vqTFq', '5dddb3338a7aa8cff1bfd22bd81772cc', NULL, NULL, 0, 0, 1713340763, 1713340763);

-- Дамп структуры для таблица dolinais.articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `m_keywords` varchar(100) DEFAULT NULL,
  `m_description` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL,
  `tree_id` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `files_post` varchar(255) DEFAULT NULL,
  `articles_status` int(11) DEFAULT NULL,
  `articles_like` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.articles: ~4 rows (приблизительно)
INSERT INTO `articles` (`id`, `title`, `m_keywords`, `m_description`, `description`, `categories_id`, `tree_id`, `slug`, `files_post`, `articles_status`, `articles_like`, `sorting`, `created_at`, `updated_at`) VALUES
	(1, 'How to reset ID in MySQL', 'AUTO_INCREMENT', 'При удаление всех данных из таблиц, иногда возникает необходимость сбросить счетчик AUTO_INCREMENT', 'При удаление всех данных из таблиц, иногда возникает необходимость сбросить счетчик AUTO_INCREMENT т.к. если этого не сделать то значения для новых данных будут присваиваться начиная с того, на котором стоял AUTO_INCREMENT до удаления данных из таблицы.\r\nALTER TABLE tablename AUTO_INCREMENT = 1', 1, 5, 'how-to-reset-auto-increment-in-mysql', 'scale_1200.png', 1, NULL, NULL, 1697480065, 1697481799),
	(2, 'Система управления содержимым', 'cms, dolinais', 'CMS это система управления контентом, на которой создают, запускают и контролируют работу сайта', '<p>CMS это система управления контентом, на которой создают, запускают и контролируют работу сайта, меняют картинки, видео и другие файлы и собирают данные клиентов без знания программирования с минимальными финансовыми и временными вложениями.</p>\r\n<p>Чтобы пользоваться CMS не нужно быть программистом и полностью прописывать блоки с нуля &mdash; вместо этого используют готовые шаблоны.</p>', 1, 6, 'sistema-upravleniya-soderzhimym', '17bc7c9d4a7a863e5ad66ec8526a731c.png', 1, NULL, NULL, 1697481566, 1713446991),
	(3, 'О нас', 'О нас', 'Рады представить вам новую версию нашей системы управления сайтом Dolina IS', '<p>Уважаемые коллеги!</p>\r\n<p>Рады представить вам новую версию нашей системы управления сайтом Dolina IS. Эта система уже много лет помогает нам создавать эффективные интернет-решения для наших клиентов.</p>\r\n<p>Dolina IS разработана специально для компаний, которые хотят иметь полный контроль над своим сайтом. Она позволяет легко управлять контентом, добавлять новые страницы, товары и услуги, а также настраивать дизайн сайта под свои нужды.</p>\r\n<p>Основные преимущества Dolina IS:</p>\r\n<p>&mdash; Простота использования. Система имеет интуитивно понятный интерфейс, который позволяет быстро освоить все её возможности даже новичкам.</p>\r\n<p>&mdash; Гибкость настройки. Вы можете выбрать готовый шаблон дизайна или создать свой собственный. Также есть возможность настроить функциональность сайта под конкретные задачи вашей компании.</p>\r\n<p>&mdash; Безопасность данных. Все данные хранятся на защищённом сервере, что гарантирует их сохранность и конфиденциальность.</p>\r\n<p>&mdash; Поддержка SEO-оптимизации. Система автоматически генерирует метатеги для каждой страницы, что повышает её видимость в поисковых системах.</p>\r\n<p>&mdash; Интеграция с другими сервисами. Вы можете интегрировать сайт с различными внешними сервисами (например, социальными сетями), чтобы расширить аудиторию вашего бизнеса.</p>\r\n<p>Мы уверены, что система Dolina IS станет незаменимым помощником для любой компании, которая хочет эффективно управлять своим сайтом и привлекать новых клиентов.</p>', 1, 11, 'about', '4d4b965543303cec8425b75a4a839242.png', 1, 500, NULL, 1697629820, 1713446925),
	(4, 'DOLINA IS CMS', 'Скачать CMS, cms dolinais', 'Рады представить вам новую версию нашей системы управления сайтом Dolina IS.', '<p>Рады представить вам новую версию нашей системы управления сайтом Dolina IS. \r\nЭта система уже много лет помогает нам создавать эффективные интернет-решения для наших клиентов.</p>\r\n<p>Dolina IS разработана специально для компаний, которые хотят иметь полный контроль над своим сайтом. Она позволяет легко управлять контентом, добавлять новые страницы, товары и услуги, а также настраивать дизайн сайта под свои нужды.</p>', 1, 14, 'download', 'be5a69fc6f1a879d700956468825e27c.png', 1, NULL, NULL, 1699976354, 1713463024);

-- Дамп структуры для таблица dolinais.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `created` int(11) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Дамп данных таблицы dolinais.categories: ~6 rows (приблизительно)
INSERT INTO `categories` (`id`, `name`, `description`, `created`, `modified`) VALUES
	(1, 'Интернет', 'Category for anything related to fashion.', 0, 0),
	(2, 'В мире', 'Gadgets, drones and more.', 0, 0),
	(3, 'Авто', 'Motor sports and more', 0, 0),
	(5, 'Фильмы', 'Movie products.', 0, 0),
	(6, 'Книги', 'Kindle books, audio books and more.', 0, 0),
	(13, 'Спорт', 'Drop into new winter gear.', 0, 0);

-- Дамп структуры для таблица dolinais.files
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT '0',
  `file_title` varchar(255) DEFAULT '0',
  `file_description` varchar(255) DEFAULT '0',
  `file_page_id` int(11) DEFAULT NULL,
  `file_status` int(11) DEFAULT NULL,
  `file_default` int(11) DEFAULT NULL,
  `file_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.files: ~3 rows (приблизительно)
INSERT INTO `files` (`id`, `file_name`, `file_title`, `file_description`, `file_page_id`, `file_status`, `file_default`, `file_sorting`, `created_at`, `updated_at`) VALUES
	(2, 'scale_1200.png', 'scale_1200.png', 'scale_1200.png', 2, 1, NULL, NULL, 1697481722, 1697481722),
	(3, 'about_sql_5dcf267e9c.jpg', 'about_sql_5dcf267e9c.jpg', 'about_sql_5dcf267e9c.jpg', 1, 1, NULL, NULL, 1697481867, 1697481867),
	(4, 'web.jpg', 'web.jpg', 'web.jpg', 3, 1, NULL, NULL, 1697629947, 1697629947),
	(9, 'logo-dolina-is.jpg', 'logo-dolina-is.jpg', 'logo-dolina-is.jpg', 4, 1, NULL, NULL, 1713464206, 1713464206);

-- Дамп структуры для таблица dolinais.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `menu_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.menu: ~5 rows (приблизительно)
INSERT INTO `menu` (`id`, `title`, `page_id`, `slug`, `menu_sorting`, `created_at`, `updated_at`) VALUES
	(1, 'О нас', 1, 'about', NULL, NULL, NULL),
	(2, 'Статьи', NULL, 'page', NULL, NULL, NULL),
	(3, 'Скачать', 4, 'download', NULL, NULL, NULL),
	(4, 'Контакты', NULL, 'contacts', NULL, NULL, NULL),
	(5, 'API', NULL, 'api', NULL, NULL, NULL);

-- Дамп структуры для таблица dolinais.messenger
CREATE TABLE IF NOT EXISTS `messenger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.messenger: ~13 rows (приблизительно)
INSERT INTO `messenger` (`id`, `user_id`, `recipient_id`, `username`, `avatar`, `message`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, 'Игорь', 'https://rito56.ru/artimg/images/obsluzhivanie-ohrannyh-sistem-i-kamer-videonablyudeniya(1).jpg', 'Привет! Как дела? Такая сойдет камера?', 1697629947, 1697480065),
	(2, 2, 1, 'Дмитрий', 'https://irkutsk.meldana.com/upload/iblock/83b/83bd35e6840c24b7c108610dcecd86b6.jpg', 'Добрый день! Всё хорошо! Лучше такую!', 1697629947, 1697480065),
	(3, 1, 2, 'Игорь', NULL, 'Все эти камеры китайские, поэтому будут стоить не очень дорого)', 1697480065, 1697480065),
	(4, 2, 1, 'Дмитрий', NULL, 'Понял. То что выше фото,чем она хуже?', 1697480065, 1697480065),
	(5, 2, 1, NULL, NULL, 'Ну как тебе сказать.. Дерьмо(((', 1699633654, NULL),
	(6, 1, 2, NULL, NULL, 'Фигово...', 1699633718, NULL),
	(7, 2, 1, NULL, NULL, 'Еще вопрос. Как будем её подключать? Через роутер? Она сетевая или обычная?', 1699635024, NULL),
	(8, 1, 2, NULL, NULL, 'Сетевая! Поэтому она будет норм)', 1699635091, NULL),
	(9, 2, 1, NULL, NULL, 'Понял, отлично тогда!', 1699635118, NULL),
	(10, 1, 2, NULL, NULL, 'Как-то так)', 1699635294, NULL),
	(11, 2, 1, NULL, NULL, 'И ещё есть пару вопросов у меня)', 1699637226, NULL),
	(12, 1, 2, NULL, NULL, 'Каких?', 1699637249, NULL),
	(13, 2, 1, NULL, 'https://laborcolor.ru/wp-content/uploads/f/a/d/fadda6d67d084b4c92424b65d084d144.jpeg', 'как на счё поставить камеру не по сети, а потом подключить к роутеру?', 1699637336, NULL);

-- Дамп структуры для таблица dolinais.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `modules_version` varchar(255) DEFAULT NULL,
  `modules_size` int(11) DEFAULT NULL,
  `modules_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.modules: ~7 rows (приблизительно)
INSERT INTO `modules` (`id`, `module_id`, `type`, `description`, `slug`, `status`, `title`, `modules_version`, `modules_size`, `modules_sorting`, `created_at`, `updated_at`) VALUES
	(1, 'api', 'Module', 'Api сайта', 'api', 1, 'Api', '0.0.1', 100, NULL, NULL, NULL),
	(2, 'page', 'Module', 'Модуль страницы', 'page', 1, 'Страницы', '0.0.1', 100, NULL, NULL, NULL),
	(3, 'admin', 'Module', 'Admin modules', 'admin', 1, 'Панель администратора', '0.0.1', 100, NULL, NULL, NULL),
	(4, 'account', 'Module', 'User modules', 'account', 1, 'ЛК', '0.0.1', 100, NULL, NULL, NULL),
	(5, 'index', 'Module', 'Home', 'index', 1, 'Главная страница', '0.0.1', 100, NULL, NULL, NULL),
	(6, 'messenger', 'Module', 'Messenger', 'messenger', 1, 'Messenger', '0.0.1', 100, NULL, NULL, NULL),
	(7, 'websocket', 'Module', 'Websocket', 'websocket', 1, 'Websocket', '0.0.1', 100, NULL, NULL, NULL),
	(8, 'tg', 'Module', 'Telegram Api', 'tg', 1, 'Telegram', '0.0.1', 100, NULL, NULL, NULL);

-- Дамп структуры для таблица dolinais.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `reply_id` int(11) DEFAULT NULL,
  `reviews_like` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.reviews: ~17 rows (приблизительно)
INSERT INTO `reviews` (`id`, `user_id`, `message`, `page_id`, `reply_id`, `reviews_like`, `created_at`, `updated_at`) VALUES
	(1, '3', 'Я использую битрикс, работает отлично. Недавно провели статистику загрузки контента, результат поразил 10 сек', 11, NULL, 10, 1697798134, NULL),
	(2, '1', 'Самая лучшая на мой взгляд это Dolina IS CMS. Рекомендую!', 11, NULL, 555, 1697798171, NULL),
	(3, '2', 'Мое знакомство с сайтостроением началось с дипломной работы - для наглядности я решила проиллюстрировать ее сайтом. Мне посоветовали воспользоваться бесплатной системой управления содержимым (CMS) Joomla. И я, абсолютный гуманитарий не только сделала дипломный проект, но и множество других сайтов - для себя и на заказ.', 11, NULL, 150, NULL, NULL),
	(4, '4', 'Наверно все, кто ранее занимались сайтами или чем то подобным, сейчас боготворят людей создавших CMS, в частности WordPress (на самом деле их много, но данная самая удобная и дружелюбная). Захотел блог, обзорный сайт, галерею или сайт визитку, без проблем ВордПресс всегда поможет.', 11, NULL, 220, NULL, NULL),
	(5, '1', 'Интересные у вас статьи! Почаще пишите)', 6, NULL, 14, 1697918777, NULL),
	(6, '1', 'А как можно сделать определенный запрос, к примеру удалить все записи где user_id=5 ?', 5, NULL, 50, 1697919120, NULL),
	(7, '1', 'Есть решение, пишите кому интересно)', 5, 1, 56, 1697919370, NULL),
	(8, '1', 'Интересно...', 6, NULL, 66, 1697920042, NULL),
	(9, '1', 'Кто нибудь использовал самописные системы?', 11, NULL, 46, 1697920129, NULL),
	(10, '1', 'А на вот это? Будет коммент?', 5, 6, 72, 1697921651, NULL),
	(11, '1', 'Будет конечно))', 5, 6, 29, 1697921859, NULL),
	(12, '1', 'Заключительный этап!', 5, 7, 68, 1697921924, NULL),
	(13, '1', 'Новый комментарий!', 5, 12, 28, 1697922724, NULL),
	(14, '1', 'Ответ на новый комментарий!', 5, 13, 49, 1697922780, NULL),
	(15, '1', 'Есть решение, ответ!', 5, 6, 89, 1697922822, NULL),
	(16, '1', 'Ок', 6, NULL, 36, 1699303290, NULL),
	(17, '1', 'Всем привет! Я тут новенький! ))', 6, NULL, 32, 1705250410, NULL),
	(18, '1', 'Hello', 6, NULL, 17, 1713340530, NULL),
	(19, '1', 'Классная статья!', 14, NULL, 46, 1714649483, NULL);

-- Дамп структуры для таблица dolinais.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `settings_name` varchar(255) NOT NULL,
  `settings_text` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.settings: ~0 rows (приблизительно)
INSERT INTO `settings` (`id`, `settings_name`, `settings_text`, `created_at`, `updated_at`) VALUES
	(1, 'title', 'CMS', NULL, NULL);

-- Дамп структуры для таблица dolinais.shop_basket
CREATE TABLE IF NOT EXISTS `shop_basket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `basket_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(250) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_barcode` varchar(50) DEFAULT NULL,
  `product_count` int(11) DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `total_price` decimal(15,2) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.shop_basket: ~1 rows (приблизительно)

-- Дамп структуры для таблица dolinais.shop_basket_id
CREATE TABLE IF NOT EXISTS `shop_basket_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `basket_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.shop_basket_id: ~41 rows (приблизительно)
INSERT INTO `shop_basket_id` (`id`, `user_id`, `basket_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1715018029, 1715018029),
	(2, 1, 2, 1715018087, 1715018087),
	(3, 2, 1, 1715018125, 1715018125),
	(4, 2, 2, 1715018133, 1715018133),
	(5, 2, 3, 1715018148, 1715018148),
	(6, 1, 3, 1715018212, 1715018212),
	(7, 1, 4, 1715018454, 1715018454),
	(8, 1, 5, 1715019517, 1715019517),
	(9, 1, 6, 1715072870, 1715072870),
	(10, 1, 7, 1715074197, 1715074197),
	(11, 1, 8, 1715074526, 1715074526),
	(12, 1, 9, 1715074627, 1715074627),
	(13, 1, 10, 1715074711, 1715074711),
	(14, 1, 11, 1715074951, 1715074951),
	(15, 1, 12, 1715077075, 1715077075),
	(16, 1, 13, 1715077135, 1715077135),
	(17, 1, 14, 1715078182, 1715078182),
	(18, 1, 15, 1715078242, 1715078242),
	(19, 1, 16, 1715078717, 1715078717),
	(20, 2, 4, 1715079765, 1715079765),
	(21, 1, 17, 1715100411, 1715100411),
	(22, 1, 18, 1715103779, 1715103779),
	(23, 1, 19, 1715117383, 1715117383),
	(24, 1, 20, 1715161432, 1715161432),
	(25, 1, 21, 1715161482, 1715161482),
	(26, 1, 22, 1715166107, 1715166107),
	(27, 1, 23, 1715166299, 1715166299),
	(28, 1, 24, 1715166470, 1715166470),
	(29, 1, 25, 1715166481, 1715166481),
	(30, 1, 26, 1715171998, 1715171998),
	(31, 1, 27, 1715172018, 1715172018),
	(32, 1, 28, 1715172035, 1715172035),
	(33, 1, 29, 1715172079, 1715172079),
	(34, 1, 30, 1715172211, 1715172211),
	(35, 1, 31, 1715199277, 1715199277),
	(36, 1, 32, 1715200827, 1715200827),
	(37, 1, 33, 1715258451, 1715258451),
	(38, 1, 34, 1715529939, 1715529939),
	(39, 1, 35, 1715532161, 1715532161),
	(40, 1, 36, 1715532194, 1715532194),
	(41, 1, 37, 1715721331, 1715721331);

-- Дамп структуры для таблица dolinais.shop_category
CREATE TABLE IF NOT EXISTS `shop_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT 0,
  `updated_at` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.shop_category: ~0 rows (приблизительно)

-- Дамп структуры для таблица dolinais.shop_order
CREATE TABLE IF NOT EXISTS `shop_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `order_status_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_count` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `insert_price` int(11) DEFAULT NULL,
  `change_price` int(11) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.shop_order: ~59 rows (приблизительно)
INSERT INTO `shop_order` (`id`, `order_id`, `user_id`, `customer_id`, `order_status_id`, `product_id`, `product_count`, `price`, `total_price`, `insert_price`, `change_price`, `payment_method`, `created_at`, `updated_at`) VALUES
	(1, '2fbb3a61e03463aa3ab1bef6b47e6cb1', 1, 1, 1, 1, 1, 60000, 60000, NULL, NULL, NULL, 1715018087, 1715018087),
	(2, '2fbb3a61e03463aa3ab1bef6b47e6cb1', 1, 1, 1, 2, 2, 85000, 170000, NULL, NULL, NULL, 1715018087, 1715018087),
	(3, 'e458121757916d52666c7a8e7853d136', 2, 2, 1, 12, 2, 97990, 195980, NULL, NULL, NULL, 1715018133, 1715018133),
	(4, '9fa16859d6e5f76f7a4bc3c4c83b3924', 2, 2, 1, 12, 3, 97990, 293970, NULL, NULL, NULL, 1715018148, 1715018148),
	(5, 'f1a6b994b5f15f468bcd97b3d4564b39', 1, 1, 1, 2, 2, 85000, 170000, NULL, NULL, NULL, 1715018212, 1715018212),
	(6, 'f1a6b994b5f15f468bcd97b3d4564b39', 1, 1, 1, 9, 1, 100000, 100000, NULL, NULL, NULL, 1715018212, 1715018212),
	(7, 'f1a6b994b5f15f468bcd97b3d4564b39', 1, 1, 1, 4, 1, 150000, 150000, NULL, NULL, NULL, 1715018212, 1715018212),
	(8, '3887d2aa7fc207da29c92aec85718215', 1, 1, 1, 12, 2, 97990, 195980, NULL, NULL, NULL, 1715018454, 1715018454),
	(9, '19a49fcb9f8e06f92ca4cbc3a37ce6da', 1, 1, 1, 12, 3, 97990, 293970, NULL, NULL, NULL, 1715019517, 1715019517),
	(10, '637301367d520a74b8fa3d52a1c426bf', 1, 1, 1, 12, 3, 97990, 293970, NULL, NULL, NULL, 1715072870, 1715072870),
	(11, '637301367d520a74b8fa3d52a1c426bf', 1, 1, 1, 10, 2, 160000, 320000, NULL, NULL, NULL, 1715072870, 1715072870),
	(12, '99debdf4f386e64a9c62afdb2487815e', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715074080, 1715074080),
	(13, '20dfdbef7f40b2d67fd65c0db5fa6992', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715074143, 1715074143),
	(14, '948ecdd7d971c619aae61d60c147c76c', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715074197, 1715074197),
	(15, 'a6a50c551af70b15bd8d79887d1f5e48', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715074504, 1715074504),
	(16, 'fff1ff6a879c40f21c91a3b1f4d64fbb', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715074526, 1715074526),
	(17, 'fff1ff6a879c40f21c91a3b1f4d64fbb', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715074526, 1715074526),
	(18, 'bda6324747f586c5e92c288d0baf5673', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715074627, 1715074627),
	(19, 'ecb69444a1c94334ada69616503a8f72', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715074711, 1715074711),
	(20, '2500255eec6192ad3b0e2126b733d124', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715077075, 1715077075),
	(21, '2500255eec6192ad3b0e2126b733d124', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715077075, 1715077075),
	(22, 'ed4b1bf08149e38a01490e39ef27b983', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715077135, 1715077135),
	(23, 'ed4b1bf08149e38a01490e39ef27b983', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715077135, 1715077135),
	(24, 'd62630d9af1e61a382e9d1851c4527f6', 1, 1, 1, 1, 1, 60000, 60000, NULL, NULL, NULL, 1715078118, 1715078118),
	(25, '7c50110959c775bbecb0c3ceb6d97987', 1, 1, 1, 1, 1, 60000, 60000, NULL, NULL, NULL, 1715078122, 1715078122),
	(26, '5148ea722ccf39ea1d9371e0e5c3bbf6', 1, 1, 1, 1, 1, 60000, 60000, NULL, NULL, NULL, 1715078182, 1715078182),
	(27, '5148ea722ccf39ea1d9371e0e5c3bbf6', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715078182, 1715078182),
	(28, '584a6370c49e0e2eb9dd5e6e8d461173', 1, 1, 1, 10, 5, 160000, 800000, NULL, NULL, NULL, 1715078242, 1715078242),
	(29, '0acbcc95235543663283b9fd384e3309', 1, 1, 1, 1, 1, 60000, 60000, NULL, NULL, NULL, 1715078717, 1715078717),
	(30, '35151b651ede476a00a0109f68d46b28', 2, 2, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715079765, 1715079765),
	(31, '892dda9363c88f9424d4b4b3e7eb0a5a', 1, 1, 1, 1, 2, 60000, 120000, NULL, NULL, NULL, 1715100411, 1715100411),
	(32, '892dda9363c88f9424d4b4b3e7eb0a5a', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715100411, 1715100411),
	(33, '305a48534610d7afd02a2675226f4b8d', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715103779, 1715103779),
	(34, '305a48534610d7afd02a2675226f4b8d', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715103779, 1715103779),
	(35, '305a48534610d7afd02a2675226f4b8d', 1, 1, 1, 1, 1, 60000, 60000, NULL, NULL, NULL, 1715103779, 1715103779),
	(36, '305a48534610d7afd02a2675226f4b8d', 1, 1, 1, 9, 1, 100000, 100000, NULL, NULL, NULL, 1715103779, 1715103779),
	(37, 'd2e70654aaa1c962ce38c1808ea3979e', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715117383, 1715117383),
	(38, 'd2e70654aaa1c962ce38c1808ea3979e', 1, 1, 1, 1, 1, 60000, 60000, NULL, NULL, NULL, 1715117383, 1715117383),
	(39, '0ccff583663d7e4a6bc35c8418d61b90', 1, 1, 1, 1, 1, 60000, 60000, NULL, NULL, NULL, 1715161432, 1715161432),
	(40, '1d602ec53ee93cc78929f15365fd132f', 1, 1, 1, 1, 3, 60000, 180000, NULL, NULL, NULL, 1715161482, 1715161482),
	(41, '694e5e53ed5f23166a824cdb53a64681', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715166107, 1715166107),
	(42, '694e5e53ed5f23166a824cdb53a64681', 1, 1, 1, 10, 5, 160000, 800000, NULL, NULL, NULL, 1715166107, 1715166107),
	(43, '1c530eed1dfbc05ffa902024549ffd46', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715166299, 1715166299),
	(44, '50bc37af7e2f3a8cd0a6ada09dd5ff5b', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715166470, 1715166470),
	(45, '624a7669202ac22667d130861d5ab037', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715166481, 1715166481),
	(46, 'ec52ecdf018434df68f2969bed8db7b5', 1, 1, 1, 1, 2, 60000, 120000, NULL, NULL, NULL, 1715171998, 1715171998),
	(47, '41b5f5c7478ecfb5f483c79a3de4459c', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715172018, 1715172018),
	(48, '1e1749e896a8a49014762e3612ff7b46', 1, 1, 1, 10, 1, 160000, 160000, NULL, NULL, NULL, 1715172035, 1715172035),
	(49, '89bf40659d36422f37b943840e9ec289', 1, 1, 1, 10, 5, 160000, 800000, NULL, NULL, NULL, 1715172079, 1715172079),
	(50, 'd48d8fb7895b3abbacb4197ebad097f5', 1, 1, 1, 10, 4, 160000, 640000, NULL, NULL, NULL, 1715172211, 1715172211),
	(51, 'a0c4f4300340462f156632bbbc9bcd64', 1, 1, 1, 14, 1, 2800, 2800, NULL, NULL, NULL, 1715199277, 1715199277),
	(52, '5592edec6587f0b0197d47c63c576058', 1, 1, 1, 14, 1, 2800, 2800, NULL, NULL, NULL, 1715200827, 1715200827),
	(53, 'eb1d2de1a092fef3ec0d29f80ec9794e', 1, 1, 1, 15, 1, 8337, 8337, NULL, NULL, NULL, 1715258451, 1715258451),
	(54, 'eb1d2de1a092fef3ec0d29f80ec9794e', 1, 1, 1, 14, 1, 2800, 2800, NULL, NULL, NULL, 1715258451, 1715258451),
	(55, '4ae8552db81077da2713dd28ba07662c', 1, 1, 1, 15, 5, 8337, 41685, NULL, NULL, NULL, 1715529939, 1715529939),
	(56, '4ae8552db81077da2713dd28ba07662c', 1, 1, 1, 14, 2, 2800, 5600, NULL, NULL, NULL, 1715529939, 1715529939),
	(57, 'a9f28d878f0bb038afd1cde6f429f313', 1, 1, 1, 14, 2, 2800, 5600, NULL, NULL, NULL, 1715532161, 1715532161),
	(58, '100d9887fe6bd5ce2962d5c6bd8bb94c', 1, 1, 1, 12, 1, 97990, 97990, NULL, NULL, NULL, 1715532194, 1715532194),
	(59, '6bd451d8ebcfcca9f7dfe0c17f0d3613', 1, 1, 1, 3, 1, 95000, 95000, NULL, NULL, NULL, 1715721331, 1715721331);

-- Дамп структуры для таблица dolinais.shop_order_history
CREATE TABLE IF NOT EXISTS `shop_order_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.shop_order_history: ~0 rows (приблизительно)

-- Дамп структуры для таблица dolinais.shop_product
CREATE TABLE IF NOT EXISTS `shop_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tree_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `files_post` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `viewed` int(11) DEFAULT NULL,
  `like` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- Дамп данных таблицы dolinais.shop_product: ~11 rows (приблизительно)
INSERT INTO `shop_product` (`id`, `title`, `description`, `category_id`, `tree_id`, `code`, `barcode`, `sku`, `slug`, `files_post`, `status`, `price`, `quantity`, `viewed`, `like`, `sorting`, `created_at`, `updated_at`) VALUES
	(1, 'IPhone 10', 'IPhone - это линейка смартфонов, разработанных и продаваемых компанией Apple Inc. и использующих мобильную операционную систему Apple iOS. IPhone первого поколения был анонсирован бывшим генеральным директором Apple Стивом Джобсом 9 января 2007 года. С тех пор Apple ежегодно выпускает новые модели iPhone и обновления iOS.', 1, 5, 'AAX1234567', NULL, NULL, 'how-to-reset-auto-increment-in-mysql', 'iphone-11-promax.jpg', 1, 60000.00, NULL, NULL, NULL, NULL, 1697480065, 1697481799),
	(2, 'IPhone 11', 'Серия смартфонов, разработанных корпорацией Apple. Работают под управлением операционной системы iOS, представляющей собой упрощённую и оптимизированную для функционирования на мобильном устройстве версию macOS.', 1, 6, '3211233', NULL, NULL, 'sistema-upravleniya-soderzhimym', 'iphone-11-promax.jpg', 1, 85000.00, NULL, NULL, NULL, NULL, 1697481566, 1697481656),
	(3, 'IPhone 12', 'Линейка iPhone значительно дороже, и это объясняется политикой компании. Каждое устройство проходит этап тестирования, и для всех моделей предусмотрены строгие правила по учету характеристик. Даже в ситуациях, когда принимается решение по модернизации гаджета, набор параметров остается неизменным. Слот для карты памяти. Если вы смотрите в сторону покупки iPhone, вам следует учитывать, что установить карту памяти формата microSD в него не получится.', NULL, 11, '', '1234567891234', NULL, 'about', 'iphone-11-promax.jpg', 1, 95000.00, NULL, NULL, 500, NULL, 1697629820, 1697629820),
	(4, 'IPhone 13', 'Это устройство действительно оказало значительное влияние на то, как люди взаимодействуют друг с другом и окружающим миром. iPhone облегчил пользователям общение через социальные сети, приложения для обмена сообщениями и видеозвонки, а также упростил доступ к мобильному интернету. С точки зрения технологий iPhone расширил границы возможностей смартфона. ', NULL, 14, '5555555', NULL, NULL, 'zagruzka-izobrazheniy', 'iphone-11-promax.jpg', 1, 150000.00, NULL, NULL, NULL, NULL, 1699976354, 1699976354),
	(9, 'Iphone 14', 'Iphone 13', 1, 1, '123', NULL, NULL, 'iphone-14', 'iphone-11-promax.jpg', 1, 100000.00, NULL, NULL, NULL, 100, 1714043792, NULL),
	(10, 'Iphone 15', 'Смартфон Apple iPhone 15 превзойдет самые смелые ожидания наиболее взыскательных пользователей. Так, внутри корпуса из металла и стекла находится поражающий быстродействием 6-ядерный процессор Apple A15 Bionic. Ваш надежный ассистент удивит устойчивостью к негативному воздействию влаги и постоянному оседанию пыли, а также порадует отсутствием следов от отпечатков пальцев. Благодаря 6.1-дюймовому OLED-дисплею вы будете ежедневно восхищаться качеством транслируемого контента, ведь его максимальное разрешение достигает 2532х1170 пикселей. Доступ посторонних к важной пользователю информации исключит встроенный сканер лица, которым оборудован смартфон Apple iPhone 15. Двойная основная камера, сконструированная на базе сенсоров Sony IMX603 и Sony IMX372, займется созданием детализированных кадров при любых окружающих условиях. Технология Dolby Atmos призвана заполнить пространство объемным звучанием без эха и посторонних помех. О подзарядке аккумулятора вам не нужно задумываться на протяжении 75 ч при работе устройства в режиме прослушивания треков.', 1, 1, '123', NULL, NULL, 'iphone-15', 'iphone-11-promax.jpg', 1, 160000.00, NULL, NULL, NULL, 100, 1714143598, NULL),
	(12, 'Samsung Galaxy S24', 'Встречайте новые Galaxy S24 и S24+ со встроенным искусственным интеллектом, который полностью изменит ваше представление о смартфонах. Искусственный интеллект Galaxy AI. Просто. Для тебя.', NULL, NULL, 'SG12213355', NULL, NULL, 'samsung-galaxy-s24', 'samsung.jpg', 1, 97990.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'Скидка 500', 'www', NULL, NULL, 'A500', NULL, NULL, NULL, NULL, 1, -500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 'ТЭН для водогревателя ARISTON Серии AM 800 ВТ', 'ТЭН для водогревателя ARISTON Серии AM 800 ВТ', 1, NULL, '', NULL, NULL, NULL, 'cad00a3ad4647a81db36b052540ceebc.jpg', NULL, 2800.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 'Система обратного осмоса Барьер Профи Осмо 100Л 5 ступеней', 'Система обратного осмоса Барьер Профи Осмо 100Л 5 ступеней', 1, NULL, '', NULL, NULL, NULL, 'fb855bad0b169bd3b47ead07981bbd4c.jpg', NULL, 8337.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 'Deonica Антиперспирант Роликовый For Men Невидимый 50мл', 'Deonica Антиперспирант Роликовый For Men Невидимый 50мл', NULL, NULL, '', '4600104033407', NULL, NULL, NULL, NULL, 360.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 'Old Spice (web) Deodorant Legend 50 Ml', 'Old Spice (web) Deodorant Legend 50 Ml', NULL, NULL, '', '8001090159106', NULL, NULL, NULL, NULL, 700.00, NULL, NULL, NULL, NULL, NULL, NULL);

-- Дамп структуры для таблица dolinais.shop_product_property
CREATE TABLE IF NOT EXISTS `shop_product_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.shop_product_property: ~0 rows (приблизительно)

-- Дамп структуры для таблица dolinais.shop_shipping
CREATE TABLE IF NOT EXISTS `shop_shipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.shop_shipping: ~0 rows (приблизительно)

-- Дамп структуры для таблица dolinais.shop_stock
CREATE TABLE IF NOT EXISTS `shop_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.shop_stock: ~9 rows (приблизительно)
INSERT INTO `shop_stock` (`id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
	(1, 12, 4, NULL, 1715532194),
	(2, 10, 0, 1715072121, 1715172211),
	(3, 3, 9, NULL, 1715721331),
	(4, 1, 0, NULL, 1715171998),
	(5, 9, 0, 1715100652, 1715103779),
	(6, 14, 3, NULL, 1715532161),
	(7, 15, 0, NULL, 1715529939),
	(8, 16, 5, NULL, NULL),
	(9, 17, 10, NULL, NULL);

-- Дамп структуры для таблица dolinais.telegram
CREATE TABLE IF NOT EXISTS `telegram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.telegram: ~0 rows (приблизительно)

-- Дамп структуры для таблица dolinais.telegram_user
CREATE TABLE IF NOT EXISTS `telegram_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.telegram_user: ~3 rows (приблизительно)
INSERT INTO `telegram_user` (`id`, `user_id`) VALUES
	(1, 413896765),
	(2, 152256440),
	(3, 544923226),
	(4, 1148831907);

-- Дамп структуры для таблица dolinais.tree_page
CREATE TABLE IF NOT EXISTS `tree_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) DEFAULT NULL,
  `page_text` text DEFAULT NULL,
  `page_meta_title` varchar(255) DEFAULT NULL,
  `page_meta_description` varchar(255) DEFAULT NULL,
  `page_meta_keywords` varchar(255) DEFAULT NULL,
  `page_type` varchar(255) DEFAULT NULL,
  `page_file` varchar(255) DEFAULT NULL,
  `page_alias` varchar(255) DEFAULT NULL,
  `module_id` varchar(255) DEFAULT NULL,
  `page_status` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lvl` int(11) DEFAULT NULL,
  `page_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.tree_page: ~23 rows (приблизительно)
INSERT INTO `tree_page` (`id`, `page_title`, `page_text`, `page_meta_title`, `page_meta_description`, `page_meta_keywords`, `page_type`, `page_file`, `page_alias`, `module_id`, `page_status`, `page_id`, `parent_id`, `lvl`, `page_sorting`, `created_at`, `updated_at`) VALUES
	(1, 'Главная', 'Главная', NULL, 'меню, каталог', 'меню, категории', 'page_category', NULL, 'categories', NULL, 1, NULL, NULL, NULL, 100, NULL, NULL),
	(2, 'Статьи', 'Статьи', NULL, NULL, NULL, 'page_category', NULL, 'page', 'page', 1, 1, NULL, NULL, 100, NULL, NULL),
	(3, 'Api', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'api', 'api', 1, 7, NULL, NULL, 100, NULL, NULL),
	(4, 'Личный кабинет', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'account', 'account', 1, 7, NULL, NULL, 100, NULL, NULL),
	(5, 'How to reset AUTO_INCREMENT in MySQL', NULL, NULL, NULL, NULL, 'page_news', NULL, 'how-to-reset-auto-increment-in-mysql', 'page', 1, 2, 2, NULL, 100, 1697480065, 1697480065),
	(6, 'Система управления содержимым', NULL, NULL, NULL, NULL, 'page_news', NULL, 'sistema-upravleniya-soderzhimym', 'page', 1, 2, 2, NULL, 100, 1697481566, 1697481566),
	(7, 'Модули', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'modules', NULL, 1, 1, 1, NULL, 100, 1697554265, 1697554265),
	(8, 'Обратная связь', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'callback', NULL, 1, 9, 9, NULL, 100, 1697555282, 1697555282),
	(9, 'Виджеты', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'widgets', NULL, 1, 1, 1, NULL, 100, 1697555349, 1697555349),
	(10, 'Admin', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'admin', 'admin', 1, 7, 7, NULL, 100, 1697611207, 1697611207),
	(11, 'О нас', 'Дизайн и разработка веб-сайта для производственной компании может быть довольно сложной задачей. Однако, когда к нам обратилась компания по производству пружин, мы были рады взяться за этот проект. Наш клиент хотел веб-сайт, который продемонстрировал бы их продукты и услуги и помог бы им привлечь больше клиентов в Интернете. Вот как мы разработали сайт для компании по производству пружин.\r\n\r\nПонимание потребностей клиента\r\n\r\nПервым шагом в этом процессе было понимание потребностей клиента. У нас была серия встреч с клиентом, чтобы обсудить его бизнес-цели, целевую аудиторию и то, чего они хотели достичь с помощью своего веб-сайта. Мы также спросили их об их конкурентах и ​​о том, что им нравится и не нравится в веб-сайтах их конкурентов.\r\n\r\nСоздание концепции дизайна\r\n\r\nПосле того, как у нас было четкое понимание потребностей клиента, мы создали концепцию дизайна сайта. Мы хотели убедиться, что веб-сайт не только выглядит профессионально, но и удобен в навигации. Мы также хотели убедиться, что веб-сайт отражает ценности компании и ее фирменный стиль.\r\n\r\nРазработка веб-сайта\r\n\r\nПосле того, как концепция дизайна была одобрена клиентом, мы приступили к разработке сайта. Мы использовали WordPress в качестве системы управления контентом и разработали собственную тему для веб-сайта. Мы также позаботились о том, чтобы веб-сайт был оптимизирован для поисковых систем и был адаптивным, что означает, что он будет отлично выглядеть на любом устройстве.\r\n\r\nСоздание контента\r\n\r\nМы работали с клиентом над созданием контента для сайта. Мы позаботились о том, чтобы контент был информативным, привлекательным и легко читаемым. Мы также позаботились о том, чтобы контент был оптимизирован для поисковых систем и содержал правильные ключевые слова.\r\n\r\nТестирование и запуск\r\n\r\nПеред запуском сайта мы протестировали его на разных устройствах и в разных браузерах, чтобы убедиться, что он работает корректно. Мы также позаботились о безопасности сайта и установке всех необходимых плагинов и обновлений. Когда сайт нас устроил, мы его запустили.\r\n\r\nРезультат\r\n\r\nСайт, который мы разработали для компании по производству пружин, удался. Веб-сайт помог компании привлечь больше клиентов в Интернете и продемонстрировать свои продукты и услуги. На сайте было легко ориентироваться, а контент был информативным и привлекательным. Веб-сайт также отражал ценности компании и ее фирменный стиль. Заказчик остался доволен результатом и получил положительные отзывы от своих клиентов.', NULL, NULL, NULL, 'page_news', NULL, 'about', 'page', 1, 2, 2, NULL, 100, 1697629820, 1697629820),
	(12, 'Messenger', 'messenger', NULL, NULL, NULL, 'page_modules', NULL, 'messenger', 'messenger', 1, 7, 7, NULL, 100, NULL, NULL),
	(13, 'websocket', 'websocket', NULL, NULL, NULL, 'page_modules_console', NULL, 'websocket', 'websocket', 1, 7, 7, NULL, 100, NULL, NULL),
	(14, 'Скачать CMS DOLINAIS', '', NULL, NULL, NULL, 'page_news', NULL, 'download', 'page', 1, 2, 2, NULL, 100, 1699976354, 1699976354),
	(15, 'IPhone 15', 'Смартфон производства корпорации Apple, работающий на базе операционной системы iOS 17 и процессора Apple A16. Презентация смартфонов была проведена во вторник 12 сентября 2023 года, в этот же день стартовали предзаказы гаджетов, смартфоны поступили в продажу 22 сентября 2023 года. Стартовая цена смартфонов от 799$ и 899$ соответственно.', NULL, NULL, NULL, 'page_news', NULL, 'iphone-15', 'page', 1, 16, 16, NULL, 100, 1703965805, 1703965805),
	(16, 'Смартфоны', '', NULL, NULL, NULL, 'page_category', NULL, 'smartfony', 'page', 1, 2, 2, NULL, 100, 1703966106, 1703966106),
	(17, 'IPhone', '', NULL, NULL, NULL, 'page_category', NULL, 'iphone-14', 'page', 1, 16, 16, NULL, 100, 1703968273, 1703968273),
	(18, 'Ноутбуки', '', NULL, NULL, NULL, 'page_category', NULL, 'noutbuki', 'page', 1, 2, 2, NULL, 100, 1703968561, 1703968561),
	(19, 'Ноутбуки', '', NULL, NULL, NULL, 'page_category', NULL, 'noutbuki', 'page', 1, 2, 2, NULL, 100, 1703968604, 1703968604),
	(20, 'Ноутбуки', '', NULL, NULL, NULL, 'page_category', NULL, 'noutbuki', 'page', 1, 19, 19, NULL, 100, 1703968662, 1703968662),
	(21, 'Ноутбуки', '', NULL, NULL, NULL, 'page_category', NULL, 'noutbuki', 'page', 1, 19, 19, NULL, 100, 1703968790, 1703968790),
	(22, 'IPhone 12', '', NULL, NULL, NULL, 'page_category', NULL, 'iphone-12', 'page', 1, 16, 16, NULL, 100, 1703971822, 1703971822),
	(23, 'Telegram API', '', NULL, NULL, NULL, 'page_modules', NULL, 'tg', 'tg', 1, 7, 7, NULL, 100, 1713465532, 1713465532);

-- Дамп структуры для таблица dolinais.type_page
CREATE TABLE IF NOT EXISTS `type_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_title` varchar(50) DEFAULT NULL,
  `type_code` varchar(50) DEFAULT NULL,
  `type_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.type_page: ~11 rows (приблизительно)
INSERT INTO `type_page` (`id`, `type_title`, `type_code`, `type_sorting`, `created_at`, `updated_at`) VALUES
	(1, 'Раздел каталога', 'page_category', NULL, NULL, NULL),
	(2, 'Виджет', 'page_widgets', NULL, NULL, NULL),
	(3, 'Акции', 'page_akcii', NULL, NULL, NULL),
	(4, 'Атрибут', 'page_attribute', NULL, NULL, NULL),
	(5, 'Баннер', 'page_banner', NULL, NULL, NULL),
	(6, 'Новости\\Статья', 'page_news', NULL, NULL, NULL),
	(7, 'Промо страница', 'page_promo', NULL, NULL, NULL),
	(8, 'Страница Каталога', 'page_catalog', NULL, NULL, NULL),
	(9, 'Слайдер', 'page_slider', NULL, NULL, NULL),
	(10, 'Модуль', 'page_modules', NULL, NULL, NULL),
	(11, 'Модули для консоли', 'page_modules_console', NULL, NULL, NULL);

-- Дамп структуры для таблица dolinais.userchat
CREATE TABLE IF NOT EXISTS `userchat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chat_owner` int(10) unsigned NOT NULL,
  `chat_partner` int(10) unsigned NOT NULL,
  `last_action` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.userchat: ~2 rows (приблизительно)
INSERT INTO `userchat` (`id`, `chat_owner`, `chat_partner`, `last_action`, `created_at`) VALUES
	(1, 111, 222, 1699286520, 1699286157),
	(2, 222, 111, 1699286520, 1699286193);

-- Дамп структуры для таблица dolinais.userchat_msg
CREATE TABLE IF NOT EXISTS `userchat_msg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chat_id` int(10) unsigned NOT NULL,
  `msg_owner` int(10) unsigned NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `recipient` int(10) unsigned NOT NULL,
  `msg_date` int(10) unsigned NOT NULL,
  `msg_status` tinyint(3) unsigned NOT NULL,
  `msg_text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chat_id` (`chat_id`),
  CONSTRAINT `userchat_msg_ibfk_1` FOREIGN KEY (`chat_id`) REFERENCES `userchat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.userchat_msg: ~4 rows (приблизительно)
INSERT INTO `userchat_msg` (`id`, `chat_id`, `msg_owner`, `sender`, `recipient`, `msg_date`, `msg_status`, `msg_text`) VALUES
	(1, 1, 111, 111, 222, 1699286241, 1, 'Hello there'),
	(2, 2, 222, 111, 222, 1699286241, 0, 'Hello there'),
	(3, 1, 111, 111, 222, 1699286421, 1, 'How are you?'),
	(4, 2, 222, 111, 222, 1699286421, 0, 'How are you?');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
