-- --------------------------------------------------------
-- Хост:                         192.168.88.252
-- Версия сервера:               10.11.6-MariaDB-0+deb12u1 - Debian 12
-- Операционная система:         debian-linux-gnu
-- HeidiSQL Версия:              12.6.0.6765
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дамп структуры базы данных dolinais
CREATE DATABASE IF NOT EXISTS `dolinais` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `dolinais`;

-- Дамп структуры для таблица dolinais.account
CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `phone` bigint(12) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `patronymic` varchar(100) DEFAULT NULL,
  `birthday` int(11) DEFAULT 0,
  `role` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `password_hash` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT 0,
  `sonline` int(11) DEFAULT 0,
  `created_at` int(11) DEFAULT 0,
  `updated_at` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- Дамп данных таблицы dolinais.account: ~6 rows (приблизительно)
INSERT INTO `account` (`id`, `username`, `phone`, `email`, `first_name`, `last_name`, `patronymic`, `birthday`, `role`, `status`, `password_hash`, `auth_key`, `password_reset_token`, `accessToken`, `code`, `sonline`, `created_at`, `updated_at`) VALUES
	(1, '79200887766', 79200887766, NULL, 'Игорь', 'Меденцев', NULL, 0, NULL, 0, '$2y$10$lBqT0jwBvO7gsIRRm1X67e9pgmspLNND77OBFUe3Sqz0Z2VDU28cO', '405d27af11a43cb537560ed039877bf3', NULL, '405d27af11a43cb537560ed039877bf3', 0, 1, 1695388947, 1699701973),
	(2, '79258407070', 79258407070, NULL, 'Дарья', 'Меденцева', NULL, 0, NULL, 0, '$2y$10$fL33Kqf2CMtJ4JtPNRXcwOf9YSRcON6350sIkQVUq0T8mfdNjpug.', 'c3f00375d7652df2eb1b190b27f2038a', NULL, 'c3f00375d7652df2eb1b190b27f2038a', 0, 0, 1695388984, 1699690148),
	(3, '79155003020', 79155003020, NULL, 'Вадим', 'Меденцев', NULL, 0, NULL, 0, '$2y$10$aWsiH5hr22sNo.j1X0ruCOf4lx5Qc2xJkGK2NIsOAjmiPD.eHAKC.', '9a4dcef22cab707d3db2715846c59227', NULL, '9a4dcef22cab707d3db2715846c59227', 0, 0, 1695389079, 1695389079),
	(4, '12121', 12121, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$yZOFl5wRbGgGvVb3NxgaKOy8uyqwxYfGa.ftR3X6kL4cCcjJn9JkC', 'b83ce14726b999168b0cf97b8caa70f8', NULL, NULL, 0, 0, 1705250589, 1705250589),
	(5, '123123', 123123, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$Bo7Et2VeAMHAvRDA8lbdgeJRRU0vRRRWS8XX9M5rGFMjdXiBG9nNi', '88cc035cdae664eeb087cc938388e252', NULL, NULL, 0, 0, 1705250601, 1705250601),
	(6, '12345', 12345, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$xzWEEsLJtWG9LtYoFBfj0O3fUMORo3pludbUY54J0EuyfSk/7vRh.', '971a074aab1f106285b12dd3b36a129f', NULL, NULL, 0, 0, 1705250635, 1705250635),
	(7, '+79200887766', 79200887766, NULL, NULL, NULL, NULL, 0, NULL, 0, '$2y$10$4JxiDA58US0SBq.FvJUVouynG29MfwqSVQzRkQ74bo6YM3P0vqTFq', '5dddb3338a7aa8cff1bfd22bd81772cc', NULL, NULL, 0, 0, 1713340763, 1713340763);

-- Дамп структуры для таблица dolinais.articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL,
  `tree_id` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `files_post` varchar(255) DEFAULT NULL,
  `articles_status` int(11) DEFAULT NULL,
  `articles_like` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.articles: ~4 rows (приблизительно)
INSERT INTO `articles` (`id`, `title`, `description`, `categories_id`, `tree_id`, `slug`, `files_post`, `articles_status`, `articles_like`, `sorting`, `created_at`, `updated_at`) VALUES
	(1, 'How to reset ID in MySQL', 'При удаление всех данных из таблиц, иногда возникает необходимость сбросить счетчик AUTO_INCREMENT т.к. если этого не сделать то значения для новых данных будут присваиваться начиная с того, на котором стоял AUTO_INCREMENT до удаления данных из таблицы.\r\nALTER TABLE tablename AUTO_INCREMENT = 1', 1, 5, 'how-to-reset-auto-increment-in-mysql', 'scale_1200.png', 1, NULL, NULL, 1697480065, 1697481799),
	(2, 'Система управления содержимым', '<p>CMS это система управления контентом, на которой создают, запускают и контролируют работу сайта, меняют картинки, видео и другие файлы и собирают данные клиентов без знания программирования с минимальными финансовыми и временными вложениями.</p>\r\n<p>Чтобы пользоваться CMS не нужно быть программистом и полностью прописывать блоки с нуля &mdash; вместо этого используют готовые шаблоны.</p>', 1, 6, 'sistema-upravleniya-soderzhimym', '17bc7c9d4a7a863e5ad66ec8526a731c.png', 1, NULL, NULL, 1697481566, 1713446991),
	(3, 'О нас', '<p>Уважаемые коллеги!</p>\r\n<p>Рады представить вам новую версию нашей системы управления сайтом Dolina IS. Эта система уже много лет помогает нам создавать эффективные интернет-решения для наших клиентов.</p>\r\n<p>Dolina IS разработана специально для компаний, которые хотят иметь полный контроль над своим сайтом. Она позволяет легко управлять контентом, добавлять новые страницы, товары и услуги, а также настраивать дизайн сайта под свои нужды.</p>\r\n<p>Основные преимущества Dolina IS:</p>\r\n<p>&mdash; Простота использования. Система имеет интуитивно понятный интерфейс, который позволяет быстро освоить все её возможности даже новичкам.</p>\r\n<p>&mdash; Гибкость настройки. Вы можете выбрать готовый шаблон дизайна или создать свой собственный. Также есть возможность настроить функциональность сайта под конкретные задачи вашей компании.</p>\r\n<p>&mdash; Безопасность данных. Все данные хранятся на защищённом сервере, что гарантирует их сохранность и конфиденциальность.</p>\r\n<p>&mdash; Поддержка SEO-оптимизации. Система автоматически генерирует метатеги для каждой страницы, что повышает её видимость в поисковых системах.</p>\r\n<p>&mdash; Интеграция с другими сервисами. Вы можете интегрировать сайт с различными внешними сервисами (например, социальными сетями), чтобы расширить аудиторию вашего бизнеса.</p>\r\n<p>Мы уверены, что система Dolina IS станет незаменимым помощником для любой компании, которая хочет эффективно управлять своим сайтом и привлекать новых клиентов.</p>', 1, 11, 'about', '4d4b965543303cec8425b75a4a839242.png', 1, 500, NULL, 1697629820, 1713446925),
	(4, 'Скачать CMS DOLINA IS', '<p><a href="https://gitlab.com/dolinais/dolinais-cms" target="_blank">Скачать</a></p>\r\n<p>Рады представить вам новую версию нашей системы управления сайтом Dolina IS. \r\nЭта система уже много лет помогает нам создавать эффективные интернет-решения для наших клиентов.</p>\r\n<p>Dolina IS разработана специально для компаний, которые хотят иметь полный контроль над своим сайтом. Она позволяет легко управлять контентом, добавлять новые страницы, товары и услуги, а также настраивать дизайн сайта под свои нужды.</p>', 1, 14, 'download', 'be5a69fc6f1a879d700956468825e27c.png', 1, NULL, NULL, 1699976354, 1713463024);

-- Дамп структуры для таблица dolinais.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `created` int(11) NOT NULL DEFAULT 0,
  `modified` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Дамп данных таблицы dolinais.categories: ~6 rows (приблизительно)
INSERT INTO `categories` (`id`, `name`, `description`, `created`, `modified`) VALUES
	(1, 'Интернет', 'Category for anything related to fashion.', 0, 0),
	(2, 'В мире', 'Gadgets, drones and more.', 0, 0),
	(3, 'Авто', 'Motor sports and more', 0, 0),
	(5, 'Фильмы', 'Movie products.', 0, 0),
	(6, 'Книги', 'Kindle books, audio books and more.', 0, 0),
	(13, 'Спорт', 'Drop into new winter gear.', 0, 0);

-- Дамп структуры для таблица dolinais.files
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT '0',
  `file_title` varchar(255) DEFAULT '0',
  `file_description` varchar(255) DEFAULT '0',
  `file_page_id` int(11) DEFAULT NULL,
  `file_status` int(11) DEFAULT NULL,
  `file_default` int(11) DEFAULT NULL,
  `file_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.files: ~4 rows (приблизительно)
INSERT INTO `files` (`id`, `file_name`, `file_title`, `file_description`, `file_page_id`, `file_status`, `file_default`, `file_sorting`, `created_at`, `updated_at`) VALUES
	(2, 'scale_1200.png', 'scale_1200.png', 'scale_1200.png', 2, 1, NULL, NULL, 1697481722, 1697481722),
	(3, 'about_sql_5dcf267e9c.jpg', 'about_sql_5dcf267e9c.jpg', 'about_sql_5dcf267e9c.jpg', 1, 1, NULL, NULL, 1697481867, 1697481867),
	(4, 'web.jpg', 'web.jpg', 'web.jpg', 3, 1, NULL, NULL, 1697629947, 1697629947),
	(9, 'logo-dolina-is.jpg', 'logo-dolina-is.jpg', 'logo-dolina-is.jpg', 4, 1, NULL, NULL, 1713464206, 1713464206);

-- Дамп структуры для таблица dolinais.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `menu_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.menu: ~5 rows (приблизительно)
INSERT INTO `menu` (`id`, `title`, `page_id`, `slug`, `menu_sorting`, `created_at`, `updated_at`) VALUES
	(1, 'О нас', 1, 'about', NULL, NULL, NULL),
	(2, 'Статьи', NULL, 'page', NULL, NULL, NULL),
	(3, 'Скачать', 4, 'download', NULL, NULL, NULL),
	(4, 'Контакты', NULL, 'contacts', NULL, NULL, NULL),
	(5, 'API', NULL, 'api', NULL, NULL, NULL);

-- Дамп структуры для таблица dolinais.messenger
CREATE TABLE IF NOT EXISTS `messenger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.messenger: ~13 rows (приблизительно)
INSERT INTO `messenger` (`id`, `user_id`, `recipient_id`, `username`, `avatar`, `message`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, 'Игорь', 'https://rito56.ru/artimg/images/obsluzhivanie-ohrannyh-sistem-i-kamer-videonablyudeniya(1).jpg', 'Привет! Как дела? Такая сойдет камера?', 1697629947, 1697480065),
	(2, 2, 1, 'Дмитрий', 'https://irkutsk.meldana.com/upload/iblock/83b/83bd35e6840c24b7c108610dcecd86b6.jpg', 'Добрый день! Всё хорошо! Лучше такую!', 1697629947, 1697480065),
	(3, 1, 2, 'Игорь', NULL, 'Все эти камеры китайские, поэтому будут стоить не очень дорого)', 1697480065, 1697480065),
	(4, 2, 1, 'Дмитрий', NULL, 'Понял. То что выше фото,чем она хуже?', 1697480065, 1697480065),
	(5, 2, 1, NULL, NULL, 'Ну как тебе сказать.. Дерьмо(((', 1699633654, NULL),
	(6, 1, 2, NULL, NULL, 'Фигово...', 1699633718, NULL),
	(7, 2, 1, NULL, NULL, 'Еще вопрос. Как будем её подключать? Через роутер? Она сетевая или обычная?', 1699635024, NULL),
	(8, 1, 2, NULL, NULL, 'Сетевая! Поэтому она будет норм)', 1699635091, NULL),
	(9, 2, 1, NULL, NULL, 'Понял, отлично тогда!', 1699635118, NULL),
	(10, 1, 2, NULL, NULL, 'Как-то так)', 1699635294, NULL),
	(11, 2, 1, NULL, NULL, 'И ещё есть пару вопросов у меня)', 1699637226, NULL),
	(12, 1, 2, NULL, NULL, 'Каких?', 1699637249, NULL),
	(13, 2, 1, NULL, 'https://laborcolor.ru/wp-content/uploads/f/a/d/fadda6d67d084b4c92424b65d084d144.jpeg', 'как на счё поставить камеру не по сети, а потом подключить к роутеру?', 1699637336, NULL);

-- Дамп структуры для таблица dolinais.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `modules_version` varchar(255) DEFAULT NULL,
  `modules_size` int(11) DEFAULT NULL,
  `modules_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.modules: ~8 rows (приблизительно)
INSERT INTO `modules` (`id`, `module_id`, `type`, `description`, `slug`, `status`, `title`, `modules_version`, `modules_size`, `modules_sorting`, `created_at`, `updated_at`) VALUES
	(1, 'api', 'Module', 'Api сайта', 'api', 1, 'Api', '0.0.1', 100, NULL, NULL, NULL),
	(2, 'page', 'Module', 'Модуль страницы', 'page', 1, 'Страницы', '0.0.1', 100, NULL, NULL, NULL),
	(3, 'admin', 'Module', 'Admin modules', 'admin', 1, 'Панель администратора', '0.0.1', 100, NULL, NULL, NULL),
	(4, 'account', 'Module', 'User modules', 'account', 1, 'ЛК', '0.0.1', 100, NULL, NULL, NULL),
	(5, 'index', 'Module', 'Home', 'index', 1, 'Главная страница', '0.0.1', 100, NULL, NULL, NULL),
	(6, 'messenger', 'Module', 'Messenger', 'messenger', 1, 'Messenger', '0.0.1', 100, NULL, NULL, NULL),
	(7, 'websocket', 'Module', 'Websocket', 'websocket', 1, 'Websocket', '0.0.1', 100, NULL, NULL, NULL),
	(8, 'tg', 'Module', 'Telegram Api', 'tg', 1, 'Telegram', '0.0.1', 100, NULL, NULL, NULL);

-- Дамп структуры для таблица dolinais.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL,
  `tree_id` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `files_post` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `like` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- Дамп данных таблицы dolinais.product: ~4 rows (приблизительно)
INSERT INTO `product` (`id`, `title`, `description`, `categories_id`, `tree_id`, `slug`, `files_post`, `status`, `price`, `code`, `like`, `sorting`, `created_at`, `updated_at`) VALUES
	(1, 'IPhone 10', 'IPhone', 1, 5, 'how-to-reset-auto-increment-in-mysql', 'scale_1200.png', 1, 60000, 1234567, NULL, NULL, 1697480065, 1697481799),
	(2, 'IPhone 11', 'IPhone', 1, 6, 'sistema-upravleniya-soderzhimym', 'about_sql_5dcf267e9c.jpg', 1, 85000, 3211233, NULL, NULL, 1697481566, 1697481656),
	(3, 'IPhone 12', 'IPhone', NULL, 11, 'about', 'web.jpg', 1, 95000, 4666555, 500, NULL, 1697629820, 1697629820),
	(4, 'IPhone 13', 'IPhone', NULL, 14, 'zagruzka-izobrazheniy', 'diagramma.png', 1, 150000, 5555555, NULL, NULL, 1699976354, 1699976354);

-- Дамп структуры для таблица dolinais.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `reply_id` int(11) DEFAULT NULL,
  `reviews_like` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.reviews: ~17 rows (приблизительно)
INSERT INTO `reviews` (`id`, `user_id`, `message`, `page_id`, `reply_id`, `reviews_like`, `created_at`, `updated_at`) VALUES
	(1, '3', 'Я использую битрикс, работает отлично. Недавно провели статистику загрузки контента, результат поразил 10 сек', 11, NULL, 10, 1697798134, NULL),
	(2, '1', 'Самая лучшая на мой взгляд это Dolina IS CMS. Рекомендую!', 11, NULL, 555, 1697798171, NULL),
	(3, '2', 'Мое знакомство с сайтостроением началось с дипломной работы - для наглядности я решила проиллюстрировать ее сайтом. Мне посоветовали воспользоваться бесплатной системой управления содержимым (CMS) Joomla. И я, абсолютный гуманитарий не только сделала дипломный проект, но и множество других сайтов - для себя и на заказ.', 11, NULL, 150, NULL, NULL),
	(4, '4', 'Наверно все, кто ранее занимались сайтами или чем то подобным, сейчас боготворят людей создавших CMS, в частности WordPress (на самом деле их много, но данная самая удобная и дружелюбная). Захотел блог, обзорный сайт, галерею или сайт визитку, без проблем ВордПресс всегда поможет.', 11, NULL, 220, NULL, NULL),
	(5, '1', 'Интересные у вас статьи! Почаще пишите)', 6, NULL, 14, 1697918777, NULL),
	(6, '1', 'А как можно сделать определенный запрос, к примеру удалить все записи где user_id=5 ?', 5, NULL, 50, 1697919120, NULL),
	(7, '1', 'Есть решение, пишите кому интересно)', 5, 1, 56, 1697919370, NULL),
	(8, '1', 'Интересно...', 6, NULL, 66, 1697920042, NULL),
	(9, '1', 'Кто нибудь использовал самописные системы?', 11, NULL, 46, 1697920129, NULL),
	(10, '1', 'А на вот это? Будет коммент?', 5, 6, 72, 1697921651, NULL),
	(11, '1', 'Будет конечно))', 5, 6, 29, 1697921859, NULL),
	(12, '1', 'Заключительный этап!', 5, 7, 68, 1697921924, NULL),
	(13, '1', 'Новый комментарий!', 5, 12, 28, 1697922724, NULL),
	(14, '1', 'Ответ на новый комментарий!', 5, 13, 49, 1697922780, NULL),
	(15, '1', 'Есть решение, ответ!', 5, 6, 89, 1697922822, NULL),
	(16, '1', 'Ок', 6, NULL, 36, 1699303290, NULL),
	(17, '1', 'Всем привет! Я тут новенький! ))', 6, NULL, 32, 1705250410, NULL),
	(18, '1', 'Hello', 6, NULL, 17, 1713340530, NULL);

-- Дамп структуры для таблица dolinais.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `settings_name` varchar(255) NOT NULL,
  `settings_text` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.settings: ~0 rows (приблизительно)
INSERT INTO `settings` (`id`, `settings_name`, `settings_text`, `created_at`, `updated_at`) VALUES
	(1, 'title', 'CMS', NULL, NULL);

-- Дамп структуры для таблица dolinais.telegram
CREATE TABLE IF NOT EXISTS `telegram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.telegram: ~0 rows (приблизительно)

-- Дамп структуры для таблица dolinais.telegram_user
CREATE TABLE IF NOT EXISTS `telegram_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.telegram_user: ~2 rows (приблизительно)
INSERT INTO `telegram_user` (`id`, `user_id`) VALUES
	(1, 413896765),
	(2, 152256440);

-- Дамп структуры для таблица dolinais.tree_page
CREATE TABLE IF NOT EXISTS `tree_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) DEFAULT NULL,
  `page_text` text DEFAULT NULL,
  `page_meta_title` varchar(255) DEFAULT NULL,
  `page_meta_description` varchar(255) DEFAULT NULL,
  `page_meta_keywords` varchar(255) DEFAULT NULL,
  `page_type` varchar(255) DEFAULT NULL,
  `page_file` varchar(255) DEFAULT NULL,
  `page_alias` varchar(255) DEFAULT NULL,
  `module_id` varchar(255) DEFAULT NULL,
  `page_status` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lvl` int(11) DEFAULT NULL,
  `page_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.tree_page: ~23 rows (приблизительно)
INSERT INTO `tree_page` (`id`, `page_title`, `page_text`, `page_meta_title`, `page_meta_description`, `page_meta_keywords`, `page_type`, `page_file`, `page_alias`, `module_id`, `page_status`, `page_id`, `parent_id`, `lvl`, `page_sorting`, `created_at`, `updated_at`) VALUES
	(1, 'Главная', 'Главная', NULL, 'меню, каталог', 'меню, категории', 'page_category', NULL, 'categories', NULL, 1, NULL, NULL, NULL, 100, NULL, NULL),
	(2, 'Статьи', 'Статьи', NULL, NULL, NULL, 'page_category', NULL, 'page', 'page', 1, 1, NULL, NULL, 100, NULL, NULL),
	(3, 'Api', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'api', 'api', 1, 7, NULL, NULL, 100, NULL, NULL),
	(4, 'Личный кабинет', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'account', 'account', 1, 7, NULL, NULL, 100, NULL, NULL),
	(5, 'How to reset AUTO_INCREMENT in MySQL', NULL, NULL, NULL, NULL, 'page_news', NULL, 'how-to-reset-auto-increment-in-mysql', 'page', 1, 2, 2, NULL, 100, 1697480065, 1697480065),
	(6, 'Система управления содержимым', NULL, NULL, NULL, NULL, 'page_news', NULL, 'sistema-upravleniya-soderzhimym', 'page', 1, 2, 2, NULL, 100, 1697481566, 1697481566),
	(7, 'Модули', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'modules', NULL, 1, 1, 1, NULL, 100, 1697554265, 1697554265),
	(8, 'Обратная связь', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'callback', NULL, 1, 9, 9, NULL, 100, 1697555282, 1697555282),
	(9, 'Виджеты', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'widgets', NULL, 1, 1, 1, NULL, 100, 1697555349, 1697555349),
	(10, 'Admin', NULL, NULL, NULL, NULL, 'page_modules', NULL, 'admin', 'admin', 1, 7, 7, NULL, 100, 1697611207, 1697611207),
	(11, 'О нас', 'Дизайн и разработка веб-сайта для производственной компании может быть довольно сложной задачей. Однако, когда к нам обратилась компания по производству пружин, мы были рады взяться за этот проект. Наш клиент хотел веб-сайт, который продемонстрировал бы их продукты и услуги и помог бы им привлечь больше клиентов в Интернете. Вот как мы разработали сайт для компании по производству пружин.\r\n\r\nПонимание потребностей клиента\r\n\r\nПервым шагом в этом процессе было понимание потребностей клиента. У нас была серия встреч с клиентом, чтобы обсудить его бизнес-цели, целевую аудиторию и то, чего они хотели достичь с помощью своего веб-сайта. Мы также спросили их об их конкурентах и ​​о том, что им нравится и не нравится в веб-сайтах их конкурентов.\r\n\r\nСоздание концепции дизайна\r\n\r\nПосле того, как у нас было четкое понимание потребностей клиента, мы создали концепцию дизайна сайта. Мы хотели убедиться, что веб-сайт не только выглядит профессионально, но и удобен в навигации. Мы также хотели убедиться, что веб-сайт отражает ценности компании и ее фирменный стиль.\r\n\r\nРазработка веб-сайта\r\n\r\nПосле того, как концепция дизайна была одобрена клиентом, мы приступили к разработке сайта. Мы использовали WordPress в качестве системы управления контентом и разработали собственную тему для веб-сайта. Мы также позаботились о том, чтобы веб-сайт был оптимизирован для поисковых систем и был адаптивным, что означает, что он будет отлично выглядеть на любом устройстве.\r\n\r\nСоздание контента\r\n\r\nМы работали с клиентом над созданием контента для сайта. Мы позаботились о том, чтобы контент был информативным, привлекательным и легко читаемым. Мы также позаботились о том, чтобы контент был оптимизирован для поисковых систем и содержал правильные ключевые слова.\r\n\r\nТестирование и запуск\r\n\r\nПеред запуском сайта мы протестировали его на разных устройствах и в разных браузерах, чтобы убедиться, что он работает корректно. Мы также позаботились о безопасности сайта и установке всех необходимых плагинов и обновлений. Когда сайт нас устроил, мы его запустили.\r\n\r\nРезультат\r\n\r\nСайт, который мы разработали для компании по производству пружин, удался. Веб-сайт помог компании привлечь больше клиентов в Интернете и продемонстрировать свои продукты и услуги. На сайте было легко ориентироваться, а контент был информативным и привлекательным. Веб-сайт также отражал ценности компании и ее фирменный стиль. Заказчик остался доволен результатом и получил положительные отзывы от своих клиентов.', NULL, NULL, NULL, 'page_news', NULL, 'about', 'page', 1, 2, 2, NULL, 100, 1697629820, 1697629820),
	(12, 'Messenger', 'messenger', NULL, NULL, NULL, 'page_modules', NULL, 'messenger', 'messenger', 1, 7, 7, NULL, 100, NULL, NULL),
	(13, 'websocket', 'websocket', NULL, NULL, NULL, 'page_modules_console', NULL, 'websocket', 'websocket', 1, 7, 7, NULL, 100, NULL, NULL),
	(14, 'Скачать CMS DOLINAIS', '', NULL, NULL, NULL, 'page_news', NULL, 'download', 'page', 1, 2, 2, NULL, 100, 1699976354, 1699976354),
	(15, 'IPhone 15', 'Смартфон производства корпорации Apple, работающий на базе операционной системы iOS 17 и процессора Apple A16. Презентация смартфонов была проведена во вторник 12 сентября 2023 года, в этот же день стартовали предзаказы гаджетов, смартфоны поступили в продажу 22 сентября 2023 года. Стартовая цена смартфонов от 799$ и 899$ соответственно.', NULL, NULL, NULL, 'page_news', NULL, 'iphone-15', 'page', 1, 16, 16, NULL, 100, 1703965805, 1703965805),
	(16, 'Смартфоны', '', NULL, NULL, NULL, 'page_category', NULL, 'smartfony', 'page', 1, 2, 2, NULL, 100, 1703966106, 1703966106),
	(17, 'IPhone', '', NULL, NULL, NULL, 'page_category', NULL, 'iphone-14', 'page', 1, 16, 16, NULL, 100, 1703968273, 1703968273),
	(18, 'Ноутбуки', '', NULL, NULL, NULL, 'page_category', NULL, 'noutbuki', 'page', 1, 2, 2, NULL, 100, 1703968561, 1703968561),
	(19, 'Ноутбуки', '', NULL, NULL, NULL, 'page_category', NULL, 'noutbuki', 'page', 1, 2, 2, NULL, 100, 1703968604, 1703968604),
	(20, 'Ноутбуки', '', NULL, NULL, NULL, 'page_category', NULL, 'noutbuki', 'page', 1, 19, 19, NULL, 100, 1703968662, 1703968662),
	(21, 'Ноутбуки', '', NULL, NULL, NULL, 'page_category', NULL, 'noutbuki', 'page', 1, 19, 19, NULL, 100, 1703968790, 1703968790),
	(22, 'IPhone 12', '', NULL, NULL, NULL, 'page_category', NULL, 'iphone-12', 'page', 1, 16, 16, NULL, 100, 1703971822, 1703971822),
	(23, 'Telegram API', '', NULL, NULL, NULL, 'page_modules', NULL, 'tg', 'tg', 1, 7, 7, NULL, 100, 1713465532, 1713465532);

-- Дамп структуры для таблица dolinais.type_page
CREATE TABLE IF NOT EXISTS `type_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_title` varchar(50) DEFAULT NULL,
  `type_code` varchar(50) DEFAULT NULL,
  `type_sorting` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.type_page: ~11 rows (приблизительно)
INSERT INTO `type_page` (`id`, `type_title`, `type_code`, `type_sorting`, `created_at`, `updated_at`) VALUES
	(1, 'Раздел каталога', 'page_category', NULL, NULL, NULL),
	(2, 'Виджет', 'page_widgets', NULL, NULL, NULL),
	(3, 'Акции', 'page_akcii', NULL, NULL, NULL),
	(4, 'Атрибут', 'page_attribute', NULL, NULL, NULL),
	(5, 'Баннер', 'page_banner', NULL, NULL, NULL),
	(6, 'Новости\\Статья', 'page_news', NULL, NULL, NULL),
	(7, 'Промо страница', 'page_promo', NULL, NULL, NULL),
	(8, 'Страница Каталога', 'page_catalog', NULL, NULL, NULL),
	(9, 'Слайдер', 'page_slider', NULL, NULL, NULL),
	(10, 'Модуль', 'page_modules', NULL, NULL, NULL),
	(11, 'Модули для консоли', 'page_modules_console', NULL, NULL, NULL);

-- Дамп структуры для таблица dolinais.userchat
CREATE TABLE IF NOT EXISTS `userchat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chat_owner` int(10) unsigned NOT NULL,
  `chat_partner` int(10) unsigned NOT NULL,
  `last_action` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.userchat: ~2 rows (приблизительно)
INSERT INTO `userchat` (`id`, `chat_owner`, `chat_partner`, `last_action`, `created_at`) VALUES
	(1, 111, 222, 1699286520, 1699286157),
	(2, 222, 111, 1699286520, 1699286193);

-- Дамп структуры для таблица dolinais.userchat_msg
CREATE TABLE IF NOT EXISTS `userchat_msg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chat_id` int(10) unsigned NOT NULL,
  `msg_owner` int(10) unsigned NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `recipient` int(10) unsigned NOT NULL,
  `msg_date` int(10) unsigned NOT NULL,
  `msg_status` tinyint(3) unsigned NOT NULL,
  `msg_text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chat_id` (`chat_id`),
  CONSTRAINT `userchat_msg_ibfk_1` FOREIGN KEY (`chat_id`) REFERENCES `userchat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Дамп данных таблицы dolinais.userchat_msg: ~4 rows (приблизительно)
INSERT INTO `userchat_msg` (`id`, `chat_id`, `msg_owner`, `sender`, `recipient`, `msg_date`, `msg_status`, `msg_text`) VALUES
	(1, 1, 111, 111, 222, 1699286241, 1, 'Hello there'),
	(2, 2, 222, 111, 222, 1699286241, 0, 'Hello there'),
	(3, 1, 111, 111, 222, 1699286421, 1, 'How are you?'),
	(4, 2, 222, 111, 222, 1699286421, 0, 'How are you?');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
