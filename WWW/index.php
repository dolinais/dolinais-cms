<?php
/**
 * @link https://dolinais.ru/
 * @copyright Copyright (c) 2022 Dolina IS Software LLC
 * @license http://cms.dolinais.ru/license/
 */

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

define('SRC', dirname($_SERVER['DOCUMENT_ROOT']) . '/app');
define('APPLOG', dirname($_SERVER['DOCUMENT_ROOT']) . '/app/log');
define('APP', dirname($_SERVER['DOCUMENT_ROOT']) . '/app/src');
define('MODULES', dirname($_SERVER['DOCUMENT_ROOT']) . '/app/src/Modules');
define('TEMPLATES', dirname($_SERVER['DOCUMENT_ROOT']) . '/app/src/templates');
define('UPLOADS', dirname($_SERVER['DOCUMENT_ROOT']) . '/WWW/uploads');
define('RESOURCES', dirname($_SERVER['DOCUMENT_ROOT']) . '/WWW/resources');

require_once APP . '/ClassesAutoloader.php';
require_once dirname( __FILE__ ) . '/../app/loader.php';
