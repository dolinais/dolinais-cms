const sendBtn = document.querySelector('#send');
const messageBox = document.querySelector('#messageBox');
const notificationsApiController = document.querySelector('#notificationsApiController');
const notifications = document.querySelector('#notifications');
const notificationsdisplay = document.querySelector('#notificationsdisplay');
const newmessagerinfo = document.querySelector('#newmessagerinfo');
const useronline = document.querySelector('#useronline');
const transition = document.querySelector('#transition');
const messageInfoData = document.querySelector('#messageInfoData');

/**
 * Проверяем нажатие клавиши
 */

var i = 0;
var txt = '.....';
var speed = 1100;

function typeWriter() {
    if(document.getElementById("type")){
        if (i < txt.length) {
            document.getElementById("type").innerHTML += txt.charAt(i);
            i++;
            setTimeout(typeWriter, speed);
        }
        if (i == txt.length) {
            i = 0;
            document.getElementById("type").innerHTML = '';
            return;
        }
    }
}



document.getElementById("messageBox")
    .addEventListener("keyup", function(event) {
    event.preventDefault();

    if (event.keyCode === 13) {
        document.getElementById("send").click();
    }

    if (event.keyCode) {
        setTimeout(() => { messageInfoData.innerHTML = "<span id='type'></span> печатает"; typeWriter();}, 5000);
    }

    setTimeout(() => { messageInfoData.innerHTML = "Нет новых сообщений"; }, 15000);
});

function sayOpen() {
    window.open("http://192.168.0.158:3000/?sel=1", '_blank').focus();
}

function sayHi() {
    notificationsdisplay.style.display = "none";
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min)
}
document.getElementById('scrolling').scrollIntoView();
$(function() {
    $('#newmessager').scroll( function() {
        var $width = $('#newmessager').outerWidth()
        var $scrollWidth = $('#newmessager')[0].scrollWidth;
        var $scrollLeft = $('#newmessager').scrollLeft();

        if ($scrollWidth - $width === $scrollLeft){
            newmessagerinfo.style.display = "none";
            const rand = getRandomNumber(1, 100);
            if(document.getElementById('transition')){
                document.getElementById('transition').style.transition = "3s";
                document.getElementById('transition').style.background = "#d6f0e0";
                document.getElementById('transition').id = 'transition-'+rand;
                document.getElementById('transition-'+rand).style.background = "#d6f0e0";
            }

        }
        if ($scrollLeft===0){
            newmessagerinfo.style.display = "none";
            if(transition){
                // document.getElementById('transition').id = 'transition-0';
                // document.getElementById('transition').style.background = "#d6f0e0";
            }
        }
    });
});

(function() {

    let ws;

    function showMessage(message) {
        notificationsdisplay.style.display = "block";
        transition.remove();
        notifications.innerHTML += `
        <div class="d-flex flex-row p-3 transition" id="transition">
            <img src="https://img.icons8.com/color/48/000000/circled-user-female-skin-type-7.png" width="30" height="30">
            <div class="chat ml-2 p-3">${message.text}
                <i class="fas fa-pencil-alt ms-2" style="opacity: .3; cursor: pointer;"></i>
                <p id="div_${message.id}"></p>
                <span style="margin-top: 10px; float: right; margin-left: 50px;">
                    <i class="fas fa-heart ms-2"></i> ${message.date}
                </span>
            </div>
        </div>`;
        // setTimeout(sayHi, 10000);
        notificationsApiController.textContent = message.messagenew;
        messageBox.value = '';
        if(document.querySelector('#div_'+message.id)){
            newmessagerinfo.innerHTML = "Новые сообщения";
            newmessagerinfo.style.display = "block";
            if(transition){
                transition.style.background = "#d0eddb";
            }
            // document.getElementById('div_'+message.id).scrollIntoView();
        }
    }

    function init() {
        if (ws) {
            ws.onerror = ws.onopen = ws.onclose = null;
            ws.close();
        }

        ws = new WebSocket('ws://192.168.0.158:3000');
        ws.onopen = () => {
            console.log('Connection opened!');
        }
        ws.onmessage = (event) => {
            if(JSON.parse(event.data).userstatusonline === 1){
                console.log(JSON.parse(event.data).userstatusonline);
                useronline.innerHTML = "Онлайн";
                return;
            }
            else if(JSON.parse(event.data).userstatusonline === 0){
                console.log(JSON.parse(event.data).userstatusonline);
                useronline.innerHTML = "Не всети";
                return;
            }else{
                console.log(JSON.parse(event.data));
                showMessage(JSON.parse(event.data));
            }
        }
        ws.onclose = function() {
            ws = null;
        }
    }

    sendBtn.onclick = function() {
        if (!ws) {
            showMessage("No WebSocket connection :(");
            return ;
        }

        if(messageBox.value != ''){
            ws.send(JSON.stringify({
                url: 'http://192.168.0.158/api/node',
                user: document.cookie.match(/PHPSESSID=(.+?)(;|$)/)[1],
                userTo: JSON.parse('{"' + location.search.substring(1).replace(/&/g, '","').replace(/=/g,'":"') + '"}',
                    function(key, value) { return key===""?value:decodeURIComponent(value) }).sel,
                message: messageBox.value
            }));
        }else{
            ws.send(JSON.stringify({
                url: 'http://192.168.0.158/api/node',
                message: 777
            }));
        }
    }
init();
})();