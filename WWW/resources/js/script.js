function SendDataForm(id, url, data, json) {
    var count = 1;
    var interval = setInterval(function(){
    if(count != 61){
        document.getElementById('submit').disabled = true;
        document.getElementById('submit').innerHTML = count++ +' сек...';
    }
    }, 1000);
    try {
        var XHR = ("onload" in new XMLHttpRequest())?XMLHttpRequest:XDomainRequest;
        var xhr = new XHR();
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.setRequestHeader("Accept", "application/json");
        xhr.send(data);
        xhr.onload = function() {
            if (this.response) {
                if(json){
                    clearInterval(interval);
                    var intervalModal = setInterval(function(){
                        var myModalEl = document.getElementById('exampleModal');
                        var modal = bootstrap.Modal.getInstance(myModalEl).hide();
                        document.getElementById(id).innerHTML = '';
                        clearInterval(intervalModal);
                    }, 5000);

                    document.getElementById('submit').disabled = false;
                    document.getElementById('submit').innerHTML = 'Отправить ';
                    var jsondata = JSON.parse(this.response);

                    document.getElementById(id).innerHTML = jsondata['status'];

                    if(jsondata['data']){
                        let users = jsondata['data'];
                        var a = document.querySelectorAll('[data-id]');
                        for (var i in a) if (a.hasOwnProperty(i)) {
                            var data_id = a[i].getAttribute('data-id');
                            if(data_id){
                                document.querySelector('[data-id="'+data_id+'"]').innerHTML = JSON.parse(users)[''+data_id+''];
                            }
                        }
                    }
                    var form = document.getElementById('form-data');
                    var data = new FormData(form);
                    for (var [key, value] of data) {
                        let inputs = document.getElementsByName( key )[0].value = "";
                    }

                }else{
                    document.getElementById(id).innerHTML = 'Error';
                }
            }
        }
        xhr.onerror = function() { console.log('onerror '+this.status); }
        xhr.send();
    } catch(e) {}
}