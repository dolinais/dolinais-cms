// dragElement(document.getElementById("notificationsdisplay"));

function GetURL(elmurl) {
    var urlParams = new URLSearchParams(window.location.search);
    if (typeof urlParams.get(elmurl) !== 'undefined') {
        return urlParams.get(elmurl);
    }
}

function CloseEl(elm) {
    elm.style.display = "none";
}

function linksOpen(links) {
    window.open(links, '_blank').focus();
}

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }

    return decodeURI(dc.substring(begin + prefix.length, end));
}

function dragElement(elmnt) {

    if(getCookie("top") != null && getCookie("left") != null){
        elmnt.style.top = document.cookie.match(/top=(.+?)(;|$)/)[1];
        elmnt.style.left = document.cookie.match(/left=(.+?)(;|$)/)[1];
    }

    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        // если присутствует, заголовок - это место, откуда вы перемещаете DIV:
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
        // в противном случае переместите DIV из любого места внутри DIV:
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // получить положение курсора мыши при запуске:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // вызов функции при каждом перемещении курсора:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // вычислить новую позицию курсора:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // установите новое положение элемента:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        document.cookie = "top=" + elmnt.style.top;
        document.cookie = "left=" + elmnt.style.left;
        // остановка перемещения при отпускании кнопки мыши:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}