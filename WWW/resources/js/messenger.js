(function() {
    const sendBtn = document.querySelector('#send');
    const messages = document.querySelector('#messages');
    const messageBox = document.querySelector('#messageBox');
    let ws;

    document.getElementById("messageBox")
        .addEventListener("keyup", function(event) {
            event.preventDefault();

            if (event.keyCode === 13) {
                document.getElementById("send").click();
            }else{
                setTimeout(() => {
                    TypeUser();
                }, 1000);
            }
        }
    );

    function showMessage(message) {
        messages.innerHTML += `
            <hr>
            <div class="chat-date">Сегодня</div>
            <li class="clearfix" id="div_${message.messageID}">
                <div class="message-data">
                    <span class="message-data-time"><span><a href="">Дарья</a> </span> ${randomTime()}</span>
                </div>
                <div class="message my-message"><p>${message.text}</p>
                <img src="https://laborcolor.ru/wp-content/uploads/f/a/d/fadda6d67d084b4c92424b65d084d144.jpeg" alt="avatar" width="300" height="auto" class="chat-img">
                </div>
            </li>`;
        if(message.sessionID == document.cookie.match(/PHPSESSID=(.+?)(;|$)/)[1]){
            messageBox.value = '';
        }

        if(document.getElementById('div_'+ message.messageID)){
            document.getElementById('div_'+ message.messageID).scrollIntoView(false);
        }
    }

    function init() {
        if (ws) {
            ws.onerror = ws.onopen = ws.onclose = null;
            ws.close();
        }

        ws = new WebSocket('ws://192.168.0.158:3000');
        ws.onerror = function (evt) {
            if (ws.readyState == 3) {
                console.log('Connection closed');
            }
        }
        ws.onopen = () => {
            ws.send(JSON.stringify({
                url: 'http://192.168.0.158/api/node',
                method: 'onOpen',
                session: getCookie("PHPSESSID"),
                userTo: GetURL('sel'),
            }));

            console.log('Connection...');
        }

        ws.onmessage = (event) => {
            if (typeof event.data !== 'undefined') {
                console.log(JSON.parse(event.data));

                if(JSON.parse(event.data).method === 'TypeUser'){
                    if(JSON.parse(event.data).user_id == GetURL('sel')){
                        document.getElementById("im-activity").style.display = "block";
                        document.getElementById("im-activity-d").style.display = "block";
                        document.getElementById("div_status").style.display = "none";
                        document.getElementById("status-d").style.display = "none";
                        setTimeout(() => {
                            document.getElementById("im-activity").style.display = "none";
                            document.getElementById("im-activity-d").style.display = "none";
                            document.getElementById("div_status").style.display = "block";
                            document.getElementById("status-d").style.display = "block";
                        }, 3500);
                    }
                }

                if(JSON.parse(event.data).method === 'onStatus'){
                    if(JSON.parse(event.data).user_id === GetURL('sel')){
                        if(JSON.parse(event.data).onStatus === 1){
                            document.getElementById('div_status').innerHTML = '<i class="fa fa-circle online"></i>online ';
                            document.getElementById('status-d').innerHTML = '<i class="fa fa-circle online"></i>online ';
                        }
                    }
                }

                if(JSON.parse(event.data).method === 'onOpen'){
                    if(JSON.parse(event.data).user_id == GetURL('sel')){
                        document.getElementById('div_status').innerHTML = '<i class="fa fa-circle online"></i>online ';
                        document.getElementById('status-d').innerHTML = '<i class="fa fa-circle online"></i>online ';
                    }
                }

                if(JSON.parse(event.data).method === 'onClose'){
                    if(JSON.parse(event.data).user_id == GetURL('sel')){
                        document.getElementById('div_status').innerHTML = '<i class="fa fa-circle offline"></i>offline ';
                        document.getElementById('status-d').innerHTML = '<i class="fa fa-circle offline"></i>offline ';
                    }
                }

                if(JSON.parse(event.data).method === 'messages'){
                    showMessage(JSON.parse(event.data));
                }
            }
        }
        ws.onclose = function() {
            ws = null;
        }
    }

    sendBtn.onclick = function() {

        if(messageBox.value != '' && messageBox.value.length < 478){
            ws.send(JSON.stringify({
                url: 'http://192.168.0.158/api/node',
                method: 'messages',
                session: getCookie("PHPSESSID"),
                userTo: GetURL('sel'),
                message: messageBox.value
            }));
        }else{
            document.getElementById('length').innerHTML = 'Максимальная длинна текста 470 символов!';
            document.getElementById("length").style.display = "block";
            setTimeout(() => {
                document.getElementById("length").style.display = "none";
            }, 9000);
        }
    }

    function TypeUser(){
        ws.send(JSON.stringify({
            url: 'http://192.168.0.158/api/node',
            method: 'TypeUser',
            session: getCookie("PHPSESSID"),
            userTo: GetURL('sel')
        }));
    }

    function onStatus(){
        ws.send(JSON.stringify({
            url: 'http://192.168.0.158/api/node',
            method: 'onStatus',
            session: getCookie("PHPSESSID"),
            userTo: GetURL('sel')
        }));
    }

    // function test(){
    //     ws.send(JSON.stringify({
    //         url: 'http://192.168.0.158/api/node',
    //         method: 'messages',
    //         session: document.cookie.match(/PHPSESSID=(.+?)(;|$)/)[1],
    //         userTo: JSON.parse('{"' + location.search.substring(1).replace(/&/g, '","').replace(/=/g,'":"') + '"}',
    //             function(key, value) { return key===""?value:decodeURIComponent(value) }).sel,
    //         message: 'Hello' + randomInteger(1, 1000)
    //     }))
    // }

    function wcConnect() {
        if (!ws) {
            let randT = randomTime();
            showMessage(JSON.parse(JSON.stringify({
                text: "Восстанавливаем связь! Ожидайте... " + randT,
                messageID: randT
            })));

            if(document.getElementById('div_' + randT)){
                setTimeout(() => {
                    init();
                    document.getElementById('div_' + randT).remove();
                }, 3000);
            }
        }
    }

    setInterval(wcConnect, 60000);
    // setInterval(test,1000);

    function randomTime() {
        const now = new Date();
        return `${now.getHours()}:${now.getMinutes()}`;
        // return `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    }

    function randomInteger(min, max) {
        let rand = min + Math.random() * (max - min);
        return Math.round(rand);
    }
    init();
})();