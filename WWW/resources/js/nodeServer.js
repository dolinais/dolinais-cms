const sendBtn = document.querySelector('#send');
const messageBox = document.querySelector('#messageBox');
const notificationsApiController = document.querySelector('#notificationsApiController');
const notifications = document.querySelector('#notifications');
const notificationsdisplay = document.querySelector('#notificationsdisplay');
const newmessagerinfo = document.querySelector('#newmessagerinfo');
const useronline = document.querySelector('#useronline');
const transition = document.querySelector('#transition');
const messageInfoData = document.querySelector('#messageInfoData');

dragElement(document.getElementById("notificationsdisplay"));

$(function() {
    const rand = getRandomNumber(1, 100);
    $('#newmessager').scroll( function() {
        var $width = $('#newmessager').outerWidth()
        var $scrollWidth = $('#newmessager')[0].scrollWidth;
        var $scrollLeft = $('#newmessager').scrollLeft();

        if ($scrollWidth - $width === $scrollLeft){
            newmessagerinfo.style.display = "none";

            if(document.getElementById('transition')){
                document.getElementById('transition').style.transition = "3s";
                document.getElementById('transition').style.background = "#d6f0e0";
                document.getElementById('transition').id = 'transition-'+rand;
                document.getElementById('transition-'+rand).style.background = "#d6f0e0";
            }
        }

        if ($scrollLeft === 0){
            newmessagerinfo.style.display = "none";

            if(document.getElementById('transition')){
                document.getElementById('transition').style.transition = "3s";
                document.getElementById('transition').style.background = "#d6f0e0";
                document.getElementById('transition').id = 'transition-'+rand;
                document.getElementById('transition-'+rand).style.background = "#d6f0e0";
            }
        }
    });
});

(function() {
    /**
     * Проверяем нажатие клавиши
     */
    document.getElementById("messageBox")
        .addEventListener("keyup", function(event) {
            event.preventDefault();

            if (event.keyCode === 13) {
                document.getElementById("send").click();
            }else{
                setTimeout(() => {
                    TypeUser();
                }, 1000);
            }
        }
    );

    let ws;

    function showMessage(message) {
        notificationsdisplay.style.display = "block";
        transition.remove();
        notifications.innerHTML += `
        <div class="d-flex flex-row p-3 transition" id="transition">
            <img src="https://img.icons8.com/color/48/000000/circled-user-female-skin-type-7.png" width="30" height="30">
            <div class="chat ml-2 p-3">${message.text}
                <i class="fas fa-pencil-alt ms-2" style="opacity: .3; cursor: pointer;"></i>
                <p id="div_${message.messageID}"></p>
                <span style="margin-top: 10px; float: right; margin-left: 50px;">
                    <i class="fas fa-heart ms-2"></i> ${message.date}
                </span>
            </div>
        </div>`;
        notificationsApiController.textContent = message.messagenew;
        messageBox.value = '';
        if(document.querySelector('#div_'+message.messageID)){
            newmessagerinfo.innerHTML = "Новые сообщения";
            newmessagerinfo.style.display = "block";
            if(transition){
                transition.style.background = "#d0eddb";
            }
            // document.getElementById('div_'+message.messageID).scrollIntoView();
        }
    }

    function init() {
        if (ws) {
            ws.onerror = ws.onopen = ws.onclose = null;
            ws.close();
        }

        ws = new WebSocket('ws://192.168.0.158:3000');

        ws.onerror = function (evt) {
            if (ws.readyState == 3) {
                console.log('Connection closed');
            }
        }

        ws.onopen = () => {
            ws.send(JSON.stringify({
                url: 'http://192.168.0.158/api/node',
                method: 'onOpen',
                session: getCookie("PHPSESSID"),
                userTo: GetURL('sel')
            }));

            console.log('Connection...');
        }

        ws.onmessage = (event) => {
            if (typeof event.data !== 'undefined') {
                if(JSON.parse(event.data).method === 'TypeUser'){
                    if(true){
                        document.getElementById("typeTuserName").innerHTML = 'Максимальная';
                        document.getElementById("im-activity").style.display = "block";
                        document.getElementById("messageInfoData").style.display = "none";
                        setTimeout(() => {
                            document.getElementById("im-activity").style.display = "none";
                            document.getElementById("messageInfoData").style.display = "block";
                        }, 3000);
                        return;
                    }else{
                        return;
                    }
                }
                if(JSON.parse(event.data).method === 'onOpen'){
                    if(JSON.parse(event.data).user_id == GetURL('sel')){
                        useronline.innerHTML = '<i class="fa fa-circle online" style="font-size:6px;"></i> в сети';
                    }
                }
                if(JSON.parse(event.data).method === 'onClose'){
                    if(JSON.parse(event.data).user_id == GetURL('sel')){
                        useronline.innerHTML = '<i class="fa fa-circle" style="color:#8c8c8c; font-size:6px;"></i> не в сети';
                    }
                }
                if(JSON.parse(event.data).method === 'messages'){
                    showMessage(JSON.parse(event.data));
                }
            }
        }
        ws.onclose = function() {
            ws = null;
        }
    }

    sendBtn.onclick = function() {

        if(messageBox.value != '' && messageBox.value.length < 478){
            ws.send(JSON.stringify({
                url: 'http://192.168.0.158/api/node',
                method: 'messages',
                session: getCookie("PHPSESSID"),
                userTo: GetURL('sel'),
                message: messageBox.value
            }));
        }else{
            document.getElementById('length').innerHTML = 'Максимальная длинна текста 470 символов!';
            document.getElementById("length").style.display = "block";
            setTimeout(() => {
                document.getElementById("length").style.display = "none";
            }, 9000);
        }
    }

    function TypeUser(){
        ws.send(JSON.stringify({
            url: 'http://192.168.0.158/api/node',
            method: 'TypeUser',
            session: getCookie("PHPSESSID"),
            userTo: GetURL('sel')
        }));
    }

    function wcConnect() {
        if (!ws) {
            let randT = randomTime();
            showMessage(JSON.parse(JSON.stringify({
                text: "Восстанавливаем связь! Ожидайте... " + randT,
                messageID: randT
            })));

            if(document.getElementById('div_' + randT)){
                setTimeout(() => {
                    init();
                    document.getElementById('div_' + randT).remove();
                }, 3000);
            }
        }
    }

    setInterval(wcConnect, 60000);
init();
})();